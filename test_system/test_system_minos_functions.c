/** @cond license
 * MinOS
 * Copyright (C) 2020 Nicu Gavrila (https://www.linkedin.com/in/nicu-gavrila/)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 * @endcond
 */

#include <signal.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "../minos_vm/minos_vm.h"


#include "../minos/minos_defs.h"

#include "../minos/minos.h"

#include "test_system.h"




int nested_irq_disable_count = 0;


void disable_interrupts(void)
{
	if (nested_irq_disable_count == 0)
	{
		DI();
	}

	nested_irq_disable_count++;
}


void enable_interrupts(void)
{
	if (nested_irq_disable_count > 0)
	{
		nested_irq_disable_count--;

		if (nested_irq_disable_count == 0)
		{
			EI();
		}
	}
	else
	{
		printf("Error: enable_interrupts called too many times");
		log_data(LOG_ERR, RES_NOK, "enable_interrupts called too many times @ %llu!", (minos_u64)get_minos_time());
	}
}


