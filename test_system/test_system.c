/** @cond license
 * MinOS
 * Copyright (C) 2020 Nicu Gavrila (https://www.linkedin.com/in/nicu-gavrila/)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 * @endcond
 */

#include <signal.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "../minos_vm/minos_vm.h"


#include "../minos/minos_defs.h"

#include "../minos/minos.h"

#include "test_system.h"



#define ALLOWED_TASK_JITTER 0


void task_0(void);
void task_1(void);
void task_2(void);
void task_3(void);
void task_4(void);
void task_5(void);
void task_6(void);
void task_7(void);
void task_8(void);
void task_9(void);



int next_task_to_run;


int create_task_ok_test = 0;
int create_task_nok_null_test = 0;
int create_task_nok_full_test = 0;

int delete_task_ok = 0;
int delete_task_nok_inexistent_test = 0;

int set_period_ok_test = 0;
int set_period_nok_inexistent_test = 0;

int set_delay_ok_test = 0;
int set_delay_nok_inexistent_test = 0;

int activate_task_ok_test = 0;
int activate_task_nok_inexistent_test = 0;

int stop_task_ok_test = 0;
int stop_task_nok_inexistent_test = 0;

int suspend_task_ok_test = 0;
int suspend_task_nok_inexistent_test = 0;
int suspend_task_nok_os_test = 0;
int suspend_task_nok_stopped_test = 0;

int prio_tests = 0;


int substate_transition_ok_test = 0;
int substate_transition_nok_test = 0;

int run_2_standby_test = 0;
int run_2_sleep_test = 0;

int standby_2_run_test = 0;
int standby_2_sleep_test = 0;

int sleep_2_standby_test = 0;
int sleep_2_run_test = 0;



struct task_data_t tasks[MAX_MINOS_TASKS];

struct task_data_t dummy_task;

minos_handle_t dmy_handle;
int dmy_type;
minos_tick_t dmy_period;
minos_tick_t dmy_runpoint;


int janus_callback_expected = 0;
enum minos_state_t next_expected_state;
minos_run_substate_t next_expected_substate;

int run_state_requests = 0;
int standby_state_requests = 0;

void determine_next_task_to_run(void);
void print_test_execution_statistics(void);
void log_test_execution_statistics(void);
void test_janus_callback(struct minos_state_transition_t transition);


void int_handler(int sig)
{
	if (sig == SIGINT)
	{
		printf("Exiting...\n");
		print_test_execution_statistics();
		log_test_execution_statistics();
		close_logfile();
		exit(0);
	}

}


void dummy_start_timer_signal(void)
{
}


void dummy_halt_instruction(void)
{
	chronos_time_interrupt(1);
}


void dummy_sleep_function(void)
{
}


void test_system(void)
{
	int i;
	struct sigaction act;



	act.sa_handler = int_handler;

	sigaction(SIGINT, &act, NULL);


	srandom(0x5EED);


	if (MAX_JANUS_CALLBACKS != 10)
	{
		printf("ERROR: MAX_JANUS_CALLBACKS differs from what the test system expects\n");

		log_data(LOG_ERR, RES_NONE,
		         "MAX_JANUS_CALLBACKS differs from what the test system expects");
	}


	for (i = 0; i < MAX_JANUS_CALLBACKS; i++)
	{
		if (set_janus_callback(test_janus_callback) < MINOS_SUCCESS)
		{
			log_data(LOG_INF, RES_NOK, "Janus callback number %d registration unsuccessful @ %llu!", i, (minos_u64)get_minos_time());
		}
		else
		{
			log_data(LOG_INF, RES_OK, "Janus callback number %d registered successfully @ %llu!", i, (minos_u64)get_minos_time());
		}
	}

	log_data(LOG_NONE, RES_NONE, "");

	if (set_janus_callback(test_janus_callback) < MINOS_SUCCESS)
	{
		log_data(LOG_INF, RES_OK, "Janus callback number 10 registration unsuccessful @ %llu!\n", i, (minos_u64)get_minos_time());
	}
	else
	{
		log_data(LOG_INF, RES_NOK, "Janus callback number 10 registered successfully @ %llu!\n", i, (minos_u64)get_minos_time());
	}

	janus_callback_expected = 1;
	next_expected_state = MINOS_RUN;
	next_expected_substate = 0;


	log_data(LOG_NONE, RES_NONE, "");
	log_data(LOG_NONE, RES_NONE, "");
	log_data(LOG_NONE, RES_NONE, "Attempting to add NULL task to Karajan");
	log_data(LOG_NONE, RES_NONE, "");

	dummy_task.func = NULL;
	create_n_activate_random_task(&dummy_task, RES_NOK);
	create_task_nok_null_test++;

	log_data(LOG_NONE, RES_NONE, "");
	log_data(LOG_NONE, RES_NONE, "");
	log_data(LOG_NONE, RES_NONE, "Adding 10 tasks to Karajan");
	log_data(LOG_NONE, RES_NONE, "");


	tasks[0].func = task_0;
	tasks[1].func = task_1;
	tasks[2].func = task_2;
	tasks[3].func = task_3;
	tasks[4].func = task_4;
	tasks[5].func = task_5;
	tasks[6].func = task_6;
	tasks[7].func = task_7;
	tasks[8].func = task_8;
	tasks[9].func = task_9;

	if (MAX_MINOS_TASKS != 10)
	{
		printf("ERROR: MAX_MINOS_TASKS differs from what the test system expects\n");

		log_data(LOG_ERR, RES_NONE,
		         "MAX_MINOS_TASKS differs from what the test system expects");
	}


	for (i = 0; i < MAX_MINOS_TASKS; i++)
	{
		tasks[i].prio = i * 10;

		log_data(LOG_NONE, RES_NONE, "%d", i);
		create_n_activate_random_task(&tasks[i], RES_OK);
		tasks[i].valid = 1;

		create_task_ok_test++;
	}

	log_data(LOG_NONE, RES_NONE, "");
	log_data(LOG_NONE, RES_NONE, "");

	log_data(LOG_NONE, RES_NONE, "");
	log_data(LOG_NONE, RES_NONE, "");
	log_data(LOG_NONE, RES_NONE, "Attempting to add an extra task to Karajan");
	log_data(LOG_NONE, RES_NONE, "");

	dummy_task.func = task_0;
	create_n_activate_random_task(&dummy_task, RES_NOK);
	create_task_nok_full_test++;

	dummy_task.func = NULL;
	create_n_activate_random_task(&dummy_task, RES_NOK);
	create_task_nok_null_test++;

	determine_next_task_to_run();
}


void recalculate_task_runpoints(void)
{
	int i;

	minos_tick_t current_time;

	current_time = get_minos_time();

	for (i = 0; i < MAX_MINOS_TASKS; i++)
	{
		if ((tasks[i].valid != 0) &&
		    (tasks[i].activations != 0))
		{

			tasks[i].runpoint = current_time + tasks[i].delay;
		}
	}

	determine_next_task_to_run();
}


void prob_request_run_state(int probability)
{
	if (special_action(probability) == 1)
	{
		request_minos_state(MINOS_RUN);
		if (run_state_requests < 0xFF)
			run_state_requests++;

		printf("MINOS_RUN request, counter = %d\n", run_state_requests);

		log_data(LOG_INF, RES_NONE, "MINOS_RUN request, counter = %d",
			 run_state_requests);
	}
}


void prob_release_run_state(int probability)
{
	if (special_action(probability) == 1)
	{
		release_minos_state(MINOS_RUN);
		if (run_state_requests > 0)
			run_state_requests--;

		printf("MINOS_RUN release, counter = %d\n", run_state_requests);

		log_data(LOG_INF, RES_NONE, "MINOS_RUN release, counter = %d",
			 run_state_requests);
	}
}


void prob_request_standby_state(int probability)
{
	if (special_action(probability) == 1)
	{
		request_minos_state(MINOS_STANDBY);
		if (standby_state_requests < 0xFF)
			standby_state_requests++;

		printf("MINOS_STANDBY request, counter = %d\n", standby_state_requests);

		log_data(LOG_INF, RES_NONE, "MINOS_STANDBY request, counter = %d",
			 standby_state_requests);
	}
}


void prob_release_standby_state(int probability)
{
	if (special_action(probability) == 1)
	{
		release_minos_state(MINOS_STANDBY);
		if (standby_state_requests > 0)
			standby_state_requests--;

		printf("MINOS_STANDBY release, counter = %d\n", standby_state_requests);

		log_data(LOG_INF, RES_NONE, "MINOS_STANDBY release, counter = %d",
			 standby_state_requests);
	}
}


void prob_request_substate(int probability, enum minos_state_t minos_state,
                           minos_run_substate_t minos_substate)
{
	minos_return_t val_ret;

	minos_run_substate_t requested_substate;

	if (special_action(probability) != 0) // request a substate
	{
		requested_substate = get_random(255);

		printf("requesting substate %d in state %d\n",
		       requested_substate, minos_state);

		log_data(LOG_INF, RES_NONE,
			 "requesting substate %d in state %d",
			 requested_substate, minos_state);


		val_ret = request_minos_run_substate(requested_substate);

		if (minos_state == MINOS_RUN)
		{
			if (val_ret < MINOS_SUCCESS)
			{
				log_data(LOG_INF, RES_NOK,
					 "Substate %d request unsuccessful",
					 requested_substate);
			}
			else
			{
				log_data(LOG_WAR, RES_OK,
					 "Substate %d request successful",
					 requested_substate);
			}

			if (requested_substate != minos_substate)
			{
				janus_callback_expected = 1;
				next_expected_substate = requested_substate;

				substate_transition_ok_test++;
			}

		}
		else
		{
			if (val_ret < MINOS_SUCCESS)
			{
				log_data(LOG_INF, RES_OK,
					 "Substate %d request unsuccessful",
					 requested_substate);
			}
			else
			{
				log_data(LOG_WAR, RES_NOK,
					 "Substate %d request successful",
					 requested_substate);
			}

			substate_transition_nok_test++;
		}
	}
}


void handle_run_state(void)
{
	if (run_state_requests == 0)
	{
		janus_callback_expected = 1;
		next_expected_state = MINOS_STANDBY;
		next_expected_substate = 0;

		if (standby_state_requests != 0)
		{
			run_2_standby_test++;
		}
		else
		{
			run_2_sleep_test++;
		}
	}
}


void handle_standby_state(void)
{
	if (run_state_requests != 0)
	{
		janus_callback_expected = 1;
		next_expected_state = MINOS_RUN;
		next_expected_substate = 0;

		standby_2_run_test++;
	}
	else if (standby_state_requests == 0)
	{
		janus_callback_expected = 1;
		next_expected_state = MINOS_SLEEP;
		next_expected_substate = 0;

		standby_2_sleep_test++;
	}
}


void handle_sleep_state(void)
{
	if ((run_state_requests + standby_state_requests) == 0)
	{
		printf("transition to SLEEP ok\n");

		log_data(LOG_INF, RES_OK, "transition to SLEEP ok");
	}
	else
	{
		printf("transition to SLEEP nok!\n");

		printf("run_state_requests: %d\n",
		       run_state_requests);

		printf("standby_state_requests: %d\n",
		       standby_state_requests);


		log_data(LOG_WAR, RES_NOK, "transition to SLEEP nok!");

		log_data(LOG_NONE, RES_NONE, "run_state_requests: %d",
			 run_state_requests);

		log_data(LOG_NONE, RES_NONE, "standby_state_requests: %d",
			 standby_state_requests);
	}

	while ((run_state_requests + standby_state_requests) == 0)
	{
		prob_request_run_state(25);
		prob_request_standby_state(25);
		prob_request_run_state(25);
		prob_request_standby_state(25);
	}

	if (standby_state_requests != 0)
	{
		sleep_2_standby_test++;
	}
	else
	{
		sleep_2_run_test++;
	}

	janus_callback_expected = 1;
	next_expected_state = MINOS_STANDBY;
	next_expected_substate = 0;
}

void test_janus_callback(struct minos_state_transition_t transition)
{
	static int calls_counter = 0;

	minos_tick_t current_time;

	calls_counter++;

	current_time = get_minos_time();

	if (janus_callback_expected == 0)
	{
		printf("\n\n");

		printf("Unexpected call to Janus callback @ %llu\n", current_time);

		printf("old state: %d     ->  new state: %d\n",
		       transition.current_state, transition.next_state);

		printf("old substate: %d  ->  new substate: %d\n",
		       transition.current_substate, transition.next_substate);

		printf("\n\n");


		log_data(LOG_NONE, RES_NONE, "");
		log_data(LOG_NONE, RES_NONE, "");

		log_data(LOG_WAR, RES_NOK, "Unexpected call to Janus callback @ %llu",
		         current_time);

		log_data(LOG_NONE, RES_NONE, "old state: %d     ->  new state: %d",
		         transition.current_state, transition.next_state);

		log_data(LOG_NONE, RES_NONE, "old substate: %d  ->  new substate: %d",
		         transition.current_substate, transition.next_substate);

		log_data(LOG_NONE, RES_NONE, "");
		log_data(LOG_NONE, RES_NONE, "");
	}

	if (calls_counter >= 10)
	{
		calls_counter = 0;

		janus_callback_expected = 0;

		printf("\n\n");
		log_data(LOG_NONE, RES_NONE, "");
		log_data(LOG_NONE, RES_NONE, "");

		printf("Expected call to Janus callback @ %llu\n", current_time);

		log_data(LOG_INF, RES_OK, "Expected call to Janus callback @ %llu",
		         current_time);

		if (transition.next_state == next_expected_state)
		{
			log_data(LOG_INF, RES_OK, "old state: %d     ->  new state: %d",
				 transition.current_state, transition.next_state);

			printf("OK transition from state %d to state %d\n",
			       transition.current_state, transition.next_state);
		}
		else
		{
			log_data(LOG_WAR, RES_NOK, "old state: %d     ->  new state: %d",
				 transition.current_state, transition.next_state);

			log_data(LOG_NONE, RES_NONE, "Expected transition to %d",
				 next_expected_state);


			printf("NOK transition from state %d to state %d\n",
			       transition.current_state, transition.next_state);

			printf("Expected transition to %d", next_expected_state);
		}

		if (transition.next_substate == next_expected_substate)
		{
			log_data(LOG_INF, RES_OK, "old substate: %d  ->  new substate: %d",
				 transition.current_substate, transition.next_substate);

			printf("OK transition from substate %d to substate %d\n",
			       transition.current_substate, transition.next_substate);
		}
		else
		{
			log_data(LOG_WAR, RES_NOK, "old substate: %d  ->  new substate: %d",
				 transition.current_substate, transition.next_substate);

			log_data(LOG_NONE, RES_NONE, "Expected transition to %d",
				 next_expected_substate);


			printf("NOK transition from substate %d to substate %d\n",
			       transition.current_substate, transition.next_substate);

			printf("Expected transition to %d", next_expected_substate);
		}


		prob_request_substate(20, transition.next_state, transition.next_substate);

		switch (transition.next_state)
		{
			case MINOS_SLEEP:
				chronos_time_interrupt(1000);

				handle_sleep_state();

				break;

			case MINOS_STANDBY:
				handle_standby_state();

				if (transition.current_state == MINOS_SLEEP)
				{
					recalculate_task_runpoints();
				}

				break;

			case MINOS_RUN:
				handle_run_state();

				break;

			default:
				printf("Transitioning to unrecognized state: %d\n",
				       transition.next_state);

				log_data(LOG_ERR, RES_NOK,
				         "Transitioning to unrecognized state: %d",
					 transition.next_state);
				break;
		}


		printf("\n\n");
		log_data(LOG_NONE, RES_NONE, "");
		log_data(LOG_NONE, RES_NONE, "");
	}
}


void determine_next_task_to_run(void)
{
	int i;

	next_task_to_run = 0;

	for (i = 1; i < MAX_MINOS_TASKS; i++)
	{
		if ((tasks[i].valid != 0) &&
		    (tasks[i].suspended == 0) &&
		    (tasks[i].activations != 0))
		{
			if ((tasks[next_task_to_run].valid != 0) &&
			    (tasks[next_task_to_run].suspended == 0) &&
			    (tasks[next_task_to_run].activations != 0))
			{
				if (tasks[i].runpoint < tasks[next_task_to_run].runpoint)
				{
					next_task_to_run = i;
				}
			}
			else
			{
				next_task_to_run = i;
			}
		}
	}

	if ((tasks[next_task_to_run].valid == 0) ||
	    (tasks[next_task_to_run].suspended != 0) ||
	    (tasks[next_task_to_run].activations == 0))
	{
		next_task_to_run = MAX_MINOS_TASKS;
	}
}


void print_execution_statistics(void)
{
	int i;
	int tmp;

	printf("\n\nStatistics:\n");

	printf("Current time: %llu\n", get_minos_time());


	printf("Valid tasks: ");
	tmp = 0;
	for (i = 0; i < MAX_MINOS_TASKS; i++)
		if (tasks[i].valid != 0)
		{
			tmp++;
			printf("%d ", tasks[i].handle);
		}
	printf(", total: %d\n", tmp);

	printf("One-shot tasks: ");
	tmp = 0;
	for (i = 0; i < MAX_MINOS_TASKS; i++)
		if (tasks[i].valid != 0)
			if (tasks[i].period == 0)
			{
				tmp++;
				printf("%d ", tasks[i].handle);
			}
	printf(", total: %d\n", tmp);

	printf("Periodic tasks: ");
	tmp = 0;
	for (i = 0; i < MAX_MINOS_TASKS; i++)
		if (tasks[i].valid != 0)
			if (tasks[i].period != 0)
			{
				tmp++;
				printf("%d ", tasks[i].handle);
			}
	printf(", total: %d\n", tmp);

	printf("Stopped tasks: ");
	tmp = 0;
	for (i = 0; i < MAX_MINOS_TASKS; i++)
		if (tasks[i].valid != 0)
			if (tasks[i].activations == 0)
			{
				tmp++;
				printf("%d ", tasks[i].handle);
			}
	printf(", total: %d\n", tmp);

	printf("Suspended tasks: ");
	tmp = 0;
	for (i = 0; i < MAX_MINOS_TASKS; i++)
		if (tasks[i].valid != 0)
			if (tasks[i].suspended != 0)
			{
				tmp++;
				printf("%d ", tasks[i].handle);
			}
	printf(", total: %d\n", tmp);

	tmp = 0;
	for (i = 0; i < MAX_MINOS_TASKS; i++)
	{
		if ((tasks[i].valid != 0) &&
		    (tasks[i].activations != 0) &&
		    (tasks[i].suspended == 0) &&
		    ((tasks[i].runpoint + ALLOWED_TASK_JITTER) < get_minos_time()))
		 {
			if (tmp == 0)
			{
				tmp = 1;
				log_data(LOG_NONE, RES_NONE, "");
				log_data(LOG_NONE, RES_NONE, "");
				log_data(LOG_NONE, RES_NONE, "");
			}
			log_data(LOG_WAR, RES_NOK,
		                 "task_%d was supposed to run @ %llu, not yet executed @ %llu!",
		                 tasks[i].handle, tasks[i].runpoint, get_minos_time());
			printf("\n\n\n");
			printf("task_%d was supposed to run @ %llu, not yet executed @ %llu!\n",
		               tasks[i].handle, tasks[i].runpoint, get_minos_time());
		 }
	}

	print_test_execution_statistics();

	if (tmp != 0)
	{
		log_data(LOG_NONE, RES_NONE, "");
		log_data(LOG_NONE, RES_NONE, "");
		log_data(LOG_NONE, RES_NONE, "");
		printf("\n\n\n");
	}


	printf("\n\n");
}


void print_test_execution_statistics(void)
{
	printf("\n\n");

	printf("create_task_ok_test:                %d \n",
	       create_task_ok_test);
	printf("create_task_nok_null_test:          %d \n",
	       create_task_nok_null_test);
	printf("create_task_nok_full_test:          %d \n",
	       create_task_nok_full_test);

	printf("delete_task_ok:                     %d \n",
	       delete_task_ok);
	printf("delete_task_nok_inexistent_test:    %d \n",
	       delete_task_nok_inexistent_test);

	printf("set_period_ok_test:                 %d \n",
	       set_period_ok_test);
	printf("set_period_nok_inexistent_test:     %d \n",
	       set_period_nok_inexistent_test);

	printf("set_delay_ok_test:                  %d \n",
	       set_delay_ok_test);
	printf("set_delay_nok_inexistent_test:      %d \n",
	       set_delay_nok_inexistent_test);

	printf("activate_task_ok_test:              %d \n",
	       activate_task_ok_test);
	printf("activate_task_nok_inexistent_test:  %d \n",
	       activate_task_nok_inexistent_test);

	printf("stop_task_ok_test:                  %d \n",
	       stop_task_ok_test);
	printf("stop_task_nok_inexistent_test:      %d \n",
	       stop_task_nok_inexistent_test);

	printf("suspend_task_ok_test:               %d \n",
	       suspend_task_ok_test);
	printf("suspend_task_nok_inexistent_test:   %d \n",
	       suspend_task_nok_inexistent_test);
	printf("suspend_task_nok_os_test:           %d \n",
	       suspend_task_nok_os_test);
	printf("suspend_task_nok_stopped_test:      %d \n",
	       suspend_task_nok_stopped_test);

	printf("prio_tests:                         %d \n",
	       prio_tests);

	printf("\n");

	printf("substate_transition_ok_test:        %d \n",
	       substate_transition_ok_test);
	printf("substate_transition_nok_test:       %d \n",
	       substate_transition_nok_test);

	printf("run_2_standby_test:                 %d \n",
	       run_2_standby_test);
	printf("run_2_sleep_test:                   %d \n",
	       run_2_sleep_test);

	printf("standby_2_run_test:                 %d \n",
	       standby_2_run_test);
	printf("standby_2_sleep_test:               %d \n",
	       standby_2_sleep_test);

	printf("sleep_2_standby_test:               %d \n",
	       sleep_2_standby_test);
	printf("sleep_2_run_test:                   %d \n",
	       sleep_2_run_test);

	printf("\n\n");
}


void log_test_execution_statistics(void)
{
	log_data(LOG_NONE, RES_NONE, "");
	log_data(LOG_NONE, RES_NONE, "");

	log_data(LOG_NONE, RES_NONE, "create_task_ok_test:                %d",
	         create_task_ok_test);
	log_data(LOG_NONE, RES_NONE, "create_task_nok_null_test:          %d",
	         create_task_nok_null_test);
	log_data(LOG_NONE, RES_NONE, "create_task_nok_full_test:          %d",
	         create_task_nok_full_test);

	log_data(LOG_NONE, RES_NONE, "delete_task_ok:                     %d",
	         delete_task_ok);
	log_data(LOG_NONE, RES_NONE, "delete_task_nok_inexistent_test:    %d",
	         delete_task_nok_inexistent_test);

	log_data(LOG_NONE, RES_NONE, "set_period_ok_test:                 %d",
	         set_period_ok_test);
	log_data(LOG_NONE, RES_NONE, "set_period_nok_inexistent_test:     %d",
	         set_period_nok_inexistent_test);

	log_data(LOG_NONE, RES_NONE, "set_delay_ok_test:                  %d",
	         set_delay_ok_test);
	log_data(LOG_NONE, RES_NONE, "set_delay_nok_inexistent_test:      %d",
	         set_delay_nok_inexistent_test);

	log_data(LOG_NONE, RES_NONE, "activate_task_ok_test:              %d",
	         activate_task_ok_test);
	log_data(LOG_NONE, RES_NONE, "activate_task_nok_inexistent_test:  %d",
	         activate_task_nok_inexistent_test);

	log_data(LOG_NONE, RES_NONE, "stop_task_ok_test:                  %d",
	         stop_task_ok_test);
	log_data(LOG_NONE, RES_NONE, "stop_task_nok_inexistent_test:      %d",
	         stop_task_nok_inexistent_test);

	log_data(LOG_NONE, RES_NONE, "suspend_task_ok_test:               %d",
	         suspend_task_ok_test);
	log_data(LOG_NONE, RES_NONE, "suspend_task_nok_inexistent_test:   %d",
	         suspend_task_nok_inexistent_test);
	log_data(LOG_NONE, RES_NONE, "suspend_task_nok_os_test:           %d",
	         suspend_task_nok_os_test);
	log_data(LOG_NONE, RES_NONE, "suspend_task_nok_stopped_test:      %d",
	         suspend_task_nok_stopped_test);

	log_data(LOG_NONE, RES_NONE, "prio_tests:                         %d",
	         prio_tests);

	log_data(LOG_NONE, RES_NONE, "");

	log_data(LOG_NONE, RES_NONE, "substate_transition_ok_test:        %d",
	         substate_transition_ok_test);
	log_data(LOG_NONE, RES_NONE, "substate_transition_nok_test:       %d",
	         substate_transition_nok_test);

	log_data(LOG_NONE, RES_NONE, "run_2_standby_test:                 %d",
	         run_2_standby_test);
	log_data(LOG_NONE, RES_NONE, "run_2_sleep_test:                   %d",
	         run_2_sleep_test);

	log_data(LOG_NONE, RES_NONE, "standby_2_run_test:                 %d",
	         standby_2_run_test);
	log_data(LOG_NONE, RES_NONE, "standby_2_sleep_test:               %d",
	         standby_2_sleep_test);

	log_data(LOG_NONE, RES_NONE, "sleep_2_standby_test:               %d",
	         sleep_2_standby_test);
	log_data(LOG_NONE, RES_NONE, "sleep_2_run_test:                   %d",
	         sleep_2_run_test);

}


void activate_all_tasks(void)
{
	int i;

	for (i = 0; i < MAX_MINOS_TASKS; i++)
	{
		if ((tasks[i].valid != 0) && (tasks[i].suspended != 0))
		{
			while (tasks[i].runpoint < get_minos_time())
			{
				tasks[i].runpoint += tasks[i].period;
			}

			activate_task(&tasks[i], RES_OK);
			activate_task_ok_test++;
		}

		if ((tasks[i].valid != 0) && (tasks[i].activations == 0))
		{
			tasks[i].runpoint = tasks[i].delay + get_minos_time();

			activate_task(&tasks[i], RES_OK);
			activate_task_ok_test++;
		}
	}
}


void replenish_task_complement(void)
{
	int i;

	for (i = 0; i < MAX_MINOS_TASKS; i++)
	{
		if (tasks[i].valid == 0)
		{
			create_n_activate_random_task(&tasks[i], RES_OK);
			create_task_ok_test++;
		}
	}
}


void state_actions(void)
{
	printf("\n\n");
	log_data(LOG_NONE, RES_NONE, "");
	log_data(LOG_NONE, RES_NONE, "");

	printf("current state:                  %d\n", get_minos_state());
	printf("current substate:               %d\n", get_minos_run_substate());
	printf("MINOS_RUN request counter:      %d\n", run_state_requests);
	printf("MINOS_STANDBY request counter:  %d\n", standby_state_requests);
	printf("\n");

	if (janus_callback_expected != 0)
	{
		printf("Janus callback not called\n");

		log_data(LOG_WAR, RES_NOK, "Janus callback not called");
	}
	else
	{
		if (special_action(25) == 1) // request first, release second
		{
			printf("request first, release second\n");

			prob_request_run_state(50);
			prob_release_run_state(50);

			prob_request_standby_state(50);
			prob_release_standby_state(50);
		}

		if (special_action(25) == 1) // release first, request second
		{
			printf("release first, request second\n");

			prob_release_run_state(50);
			prob_request_run_state(50);

			prob_release_standby_state(50);
			prob_request_standby_state(50);
		}

		prob_request_substate(50, get_minos_state(), get_minos_run_substate());

		switch (get_minos_state())
		{
			case MINOS_STANDBY:
				handle_standby_state();

				break;

			case MINOS_RUN:
				handle_run_state();

				break;

			default:
				printf("Unrecognized state: %d\n",
				       get_minos_state());

				log_data(LOG_ERR, RES_NOK, "Unrecognized state: %d",
					 get_minos_state());
				break;
		}

	}


	printf("\n\n");
	log_data(LOG_NONE, RES_NONE, "");
	log_data(LOG_NONE, RES_NONE, "");
}


void log_task_execution(int index)
{
	static int run_number = 0;
	minos_tick_t current_time;

	enum test_result_t exp_res;

	int i;

	run_number++;

	current_time = get_minos_time();

	if (tasks[index].period == 0)
	{
		tasks[index].activations--;
	}

	if (next_task_to_run != index)
	{
		log_data(LOG_WAR, RES_NOK,
		         "task_%d executed, but task_%d was expected!",
		         tasks[index].handle, tasks[next_task_to_run].handle);
		log_data(LOG_NONE, RES_NONE,
		         "actual index: %d, expected index: %d",
		         index, next_task_to_run);


		printf("\n\ntask_%d executed, but task_%d was expected!\n\n",
		       tasks[index].handle, tasks[next_task_to_run].handle);
		printf("actual index: %d, expected index: %d\n",
		       index, next_task_to_run);
	}

	if ((tasks[index].runpoint - ALLOWED_TASK_JITTER <= current_time) &&
	    (current_time <= (tasks[index].runpoint + ALLOWED_TASK_JITTER)))
	{
		log_data(LOG_INF, RES_OK,
		         "task_%d index_%d executed @ %llu (%llu)!",
		         tasks[index].handle, index, current_time, tasks[index].runpoint);
	}
	else
	{
		log_data(LOG_WAR, RES_NOK,
		         "task_%d index_%d executed @ %llu, was supposed to run @ %llu!",
		         tasks[index].handle, index, current_time, tasks[index].runpoint);

		printf("\n\ntask_%d index_%d executed @ %llu, was supposed to run @ %llu!\n\n",
		       tasks[index].handle, index, current_time, tasks[index].runpoint);
	}

//	printf("%d", tasks[index].handle);
//	fflush(stdout);

	if (special_action(5) != 0)   /// delete this task
	{
		log_data(LOG_INF, RES_NONE,
		         "Special action: delete this task");

		delete_task(&tasks[index], RES_OK);
		delete_task_ok++;
	}

	if (special_action(5) != 0)   /// delete another task (if one can be found)
	{
		log_data(LOG_INF, RES_NONE,
		         "Special action: delete another task");

		for (i = 0; i < MAX_MINOS_TASKS; i++)
		{
                        if ((tasks[i].valid != 0) && (i != index))
			{
                        	break;
			}
		}

		if (i < MAX_MINOS_TASKS)
		{
			log_data(LOG_NONE, RES_NONE,
				 "Chosen: task_%d index_%d",
				 tasks[i].handle, i);

			delete_task(&tasks[i], RES_OK);
			delete_task_ok++;
		}
		else
		{
			for (i = 0; i < MAX_MINOS_TASKS; i++)
			{
				if ((tasks[i].valid == 0) &&
				    (tasks[i].handle != tasks[index].handle))
				{
					break;
				}
			}

			log_data(LOG_NONE, RES_NONE,
				 "Chosen: task_%d index_%d",
				 tasks[i].handle, i);

			delete_task(&tasks[i], RES_NOK);
			delete_task_nok_inexistent_test++;
		}
	}

	if (special_action(5) != 0)   /// stop this task
	{
		log_data(LOG_INF, RES_NONE,
		         "Special action: stop this task");

		if (tasks[index].valid != 0)
		{
			exp_res = RES_OK;
			stop_task_ok_test++;
		}
		else
		{
			exp_res = RES_NOK;
			stop_task_nok_inexistent_test++;
		}

		stop_task(&tasks[index], exp_res);
	}

	if (special_action(5) != 0)   /// stop another task (if one can be found)
	{
		log_data(LOG_INF, RES_NONE,
		         "Special action: stop another task");

		for (i = 0; i < MAX_MINOS_TASKS; i++)
		{
                        if ((tasks[i].valid != 0) && (i != index))
			{
                        	break;
			}
		}

		if (i < MAX_MINOS_TASKS)
		{
			log_data(LOG_NONE, RES_NONE,
				 "Chosen: task_%d index_%d",
				 tasks[i].handle, i);

			stop_task(&tasks[i], RES_OK);
			stop_task_ok_test++;
		}
	}

	if (special_action(5) != 0)   /// suspend this task (if periodic)
	{
		log_data(LOG_INF, RES_NONE,
		         "Special action: suspend this task");

		if ((tasks[index].valid != 0) &&
		    (tasks[index].period != 0) &&
		    (tasks[index].activations != 0))
		{
			exp_res = RES_OK;
			suspend_task_ok_test++;
		}
		else
		{
			exp_res = RES_NOK;
			if (tasks[index].valid == 0)
				suspend_task_nok_inexistent_test++;

			if (tasks[index].period == 0)
				suspend_task_nok_os_test++;

			if (tasks[index].activations == 0)
				suspend_task_nok_stopped_test++;
		}

		suspend_task(&tasks[index], exp_res);
	}

	if (special_action(5) != 0)   /// suspends another task (if one can be found)
	{
		log_data(LOG_INF, RES_NONE,
		         "Special action: suspend another task");

		for (i = 0; i < MAX_MINOS_TASKS; i++)
		{
                        if ((tasks[i].valid != 0) &&
                            (i != index) &&
                            (tasks[i].period != 0))
			{
                        	break;
			}
		}

		if (i < MAX_MINOS_TASKS)
		{
			log_data(LOG_NONE, RES_NONE,
				 "Chosen: task_%d index_%d",
				 tasks[i].handle, i);

			if (tasks[i].activations == 0)
			{
				exp_res = RES_NOK;
				suspend_task_nok_stopped_test++;
			}
			else
			{
				exp_res = RES_OK;
				suspend_task_ok_test++;
			}
			suspend_task(&tasks[i], exp_res);
		}
	}

	if (special_action(5) != 0)   /// activate a suspended task (if one can be found)
	{
		log_data(LOG_INF, RES_NONE,
		         "Special action: activate a suspended task");

		for (i = 0; i < MAX_MINOS_TASKS; i++)
		{
                        if ((tasks[i].valid != 0)      &&
                            (i != index)              &&
                            (tasks[i].period != 0)       &&
                            (tasks[i].suspended != 0)  &&
                            (tasks[i].activations != 0))
			{
                        	break;
			}
		}

		if (i < MAX_MINOS_TASKS)
		{
			log_data(LOG_NONE, RES_NONE,
				 "Chosen: task_%d index_%d",
				 tasks[i].handle, i);

			while (tasks[i].runpoint < current_time)
			{
				tasks[i].runpoint += tasks[i].period;
			}

			activate_task(&tasks[i], RES_OK);
			activate_task_ok_test++;
		}
	}

	if (special_action(5) != 0)   /// create a task (if possible)
	{
		log_data(LOG_INF, RES_NONE,
		         "Special action: create a task");

		for (i = 0; i < MAX_MINOS_TASKS; i++)
		{
                        if (tasks[i].valid == 0)
			{
                        	break;
			}
		}

		log_data(LOG_NONE, RES_NONE,
		         "Chosen: index_%d",
		         i);

		if (i < MAX_MINOS_TASKS)
		{
			if (i != index)
			{
				create_n_activate_random_task(&tasks[i], RES_OK);
				create_task_ok_test++;
				set_delay_ok_test++;
				set_period_ok_test++;
				activate_task_ok_test++;
			}
			else
			{
				create_task(&tasks[i], RES_OK);
				create_task_ok_test++;
			}
		}
		else
		{
			dummy_task.func = task_0;
			create_n_activate_random_task(&dummy_task, RES_NOK);
			create_task_nok_full_test++;
			set_delay_nok_inexistent_test++;
			set_period_nok_inexistent_test++;
			activate_task_nok_inexistent_test++;
		}
	}


	if (special_action(5) != 0)   /// create a copy of this task (if possible)
	{
		log_data(LOG_INF, RES_NONE,
		         "Special action: create a copy of this task");

		for (i = 0; i < MAX_MINOS_TASKS; i++)
		{
                        if ((tasks[i].valid == 0) && (i != index))
			{
                        	break;
			}
		}

		log_data(LOG_NONE, RES_NONE,
		         "Chosen: index_%d",
		         i);

		if (i < MAX_MINOS_TASKS)
		{
			create_task(&tasks[i], RES_OK);

			update_task_delay(&tasks[i], tasks[index].delay, RES_OK);
			update_task_period(&tasks[i], tasks[index].period, RES_OK);

			activate_task(&tasks[i], RES_OK);

			tasks[i].runpoint = current_time + tasks[i].delay;

			log_next_runpoint(&tasks[i]);

			create_task_ok_test++;
			set_delay_ok_test++;
			set_period_ok_test++;
			activate_task_ok_test++;
		}
	}


	if (tasks[index].valid != 0)
	{
		set_random_task_delay_n_period(&tasks[index], RES_OK);

		set_delay_ok_test++;
		set_period_ok_test++;
		activate_task_ok_test++;
	}

	log_data(LOG_NONE, RES_NONE, "");


	if (run_number > 20)
	{
		run_number = 0;

		state_actions();

		print_execution_statistics();
	}


	for (i = 0; i < MAX_MINOS_TASKS; i++)
	{
		if ((tasks[i].valid != 0) &&
		    (tasks[i].suspended == 0) &&
		    (tasks[i].activations != 0))
		{
			break;
		}
	}

	if (i >= MAX_MINOS_TASKS)  // nothing left to run
	{
		run_number = 0;
		print_execution_statistics();
		printf("Nothing left to run, reactivating and recreating tasks...\n\n");
		print_execution_statistics();

		activate_all_tasks();
		replenish_task_complement();
	}

	determine_next_task_to_run();

	if (next_task_to_run < MAX_MINOS_TASKS)
	{
		for (i = 0; i < MAX_MINOS_TASKS; i++)
		{
			if ((i != next_task_to_run) &&
			    (tasks[i].valid != 0) &&
			    (tasks[i].suspended == 0) &&
			    (tasks[i].activations != 0) &&
			    (tasks[i].runpoint == tasks[next_task_to_run].runpoint))
			{
				prio_tests++;
				break;
			}
		}
	}


///	state_actions();
}


void task_0(void)
{
	log_task_execution(0);
}

void task_1(void)
{
	log_task_execution(1);
}

void task_2(void)
{
	log_task_execution(2);
}

void task_3(void)
{
	log_task_execution(3);
}

void task_4(void)
{
	log_task_execution(4);
}

void task_5(void)
{
	log_task_execution(5);
}

void task_6(void)
{
	log_task_execution(6);
}

void task_7(void)
{
	log_task_execution(7);
}

void task_8(void)
{
	log_task_execution(8);
}

void task_9(void)
{
	log_task_execution(9);
}


