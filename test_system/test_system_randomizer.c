/** @cond license
 * MinOS
 * Copyright (C) 2020 Nicu Gavrila (https://www.linkedin.com/in/nicu-gavrila/)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 * @endcond
 */

#include <signal.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "../minos_vm/minos_vm.h"


#include "../minos/minos_defs.h"

#include "../minos/minos.h"

#include "test_system.h"


unsigned int get_random(unsigned int max_val)
{
	return random() % (max_val + 1);
}


enum test_result_t get_nok_res(enum test_result_t exp_res)
{
	switch (exp_res)
	{
		case RES_NOK:
			return RES_OK;
		case RES_OK:
			return RES_NOK;
		default:
			return exp_res;
	}
}


enum test_result_t get_ok_res(enum test_result_t exp_res)
{
	switch (exp_res)
	{
		case RES_NOK:
			return RES_NOK;
		case RES_OK:
			return RES_OK;
		default:
			return exp_res;
	}
}


int special_action(int percentage)
{
	unsigned int action;

	action = get_random(10000);

	if (action < (percentage * 100))
		return 1;
	else
		return 0;
}


void create_task(struct task_data_t *task, enum test_result_t exp_res)
{
	minos_return_t tmp;

	if ((tmp = create_minos_task(task->func, task->prio)) < MINOS_SUCCESS)
	{
		log_data(LOG_INF, get_nok_res(exp_res),
		         "Creation of task_%d unsuccessful @ %llu!",
		         tmp, (minos_u64)get_minos_time());
	}
	else
	{
		task->valid = 1;
		task->suspended = 0;
		task->activations = 0;
		task->period = 0;
		task->delay = 0;
		log_data(LOG_INF, get_ok_res(exp_res),
		         "Creation of task_%d successful @ %llu!",
		         tmp, (minos_u64)get_minos_time());
	}

	task->handle = tmp;
}


void delete_task(struct task_data_t *task, enum test_result_t exp_res)
{
	if (delete_minos_task(task->handle) < MINOS_SUCCESS)
	{
		log_data(LOG_INF, get_nok_res(exp_res),
		         "Deletion of task_%d unsuccessful @ %llu!",
		         task->handle, (minos_u64)get_minos_time());
	}
	else
	{
		task->valid = 0;
		task->suspended = 0;
		task->activations = 0;

		log_data(LOG_INF, get_ok_res(exp_res),
		         "Deletion of task_%d successful @ %llu!",
		         task->handle, (minos_u64)get_minos_time());
	}
}


void stop_task(struct task_data_t *task, enum test_result_t exp_res)
{
	if (stop_minos_task(task->handle) < MINOS_SUCCESS)
	{
		log_data(LOG_INF, get_nok_res(exp_res),
		         "Stopping task_%d unsuccessful @ %llu!",
		         task->handle, (minos_u64)get_minos_time());
	}
	else
	{
		task->activations = 0;
		task->suspended = 0;

		log_data(LOG_INF, get_ok_res(exp_res),
		         "Stopping task_%d successful @ %llu!",
		         task->handle, (minos_u64)get_minos_time());
	}
}


void suspend_task(struct task_data_t *task, enum test_result_t exp_res)
{
	if (suspend_minos_task(task->handle) < MINOS_SUCCESS)
	{
		log_data(LOG_INF, get_nok_res(exp_res),
		         "Suspending task_%d unsuccessful @ %llu!",
		         task->handle, (minos_u64)get_minos_time());
	}
	else
	{
		task->suspended = 1;

		log_data(LOG_INF, get_ok_res(exp_res),
		         "Suspending task_%d successful @ %llu!",
		         task->handle, (minos_u64)get_minos_time());
	}
}


void update_task_delay(struct task_data_t *task,
		       minos_tick_t delay,
		       enum test_result_t exp_res)
{
	if (change_minos_task_delay(task->handle, delay) < MINOS_SUCCESS)
	{
		log_data(LOG_INF, get_nok_res(exp_res),
		         "Changing delay of task_%d to %d unsuccessful @ %llu!",
		         task->handle, delay, (minos_u64)get_minos_time());
	}
	else
	{
		task->delay = delay;

		log_data(LOG_INF, get_ok_res(exp_res),
		         "Changing delay of task_%d to %d successful @ %llu!",
		         task->handle, delay, (minos_u64)get_minos_time());
	}
}


void update_task_period(struct task_data_t *task,
		       minos_tick_t period,
		       enum test_result_t exp_res)
{
	if (change_minos_task_period(task->handle, period) < MINOS_SUCCESS)
	{
		log_data(LOG_INF, get_nok_res(exp_res),
		         "Changing period of task_%d to %d unsuccessful @ %llu!",
		         task->handle, period, (minos_u64)get_minos_time());
	}
	else
	{
		if (task->period == 0)
		{
			if (period != 0)
			{
				task->activations = 0;
			}
		}
		else
		{
			if (period == 0)
			{
				task->activations = 0;
			}
		}


		task->period = period;

		log_data(LOG_INF, get_ok_res(exp_res),
		         "Changing period of task_%d to %d successful @ %llu!",
		         task->handle, period, (minos_u64)get_minos_time());
	}
}


void activate_task(struct task_data_t *task, enum test_result_t exp_res)
{
	if (activate_minos_task(task->handle) < MINOS_SUCCESS)
	{
		log_data(LOG_INF, get_nok_res(exp_res),
		         "Activation of task_%d unsuccessful @ %llu!",
		         task->handle, (minos_u64)get_minos_time());
	}
	else
	{
		task->suspended = 0;

		if (task->period == 0)
		{
			task->activations++;
		}
		else
		{
			task->activations = 1;
		}

		log_data(LOG_INF, get_ok_res(exp_res),
		         "Activation of task_%d successful @ %llu!",
		         task->handle, (minos_u64)get_minos_time());
	}
}


void log_next_runpoint(struct task_data_t *task)
{
	log_data(LOG_INF, RES_NONE,
		 "Next runpoint of task_%d: %llu",
		 task->handle, task->runpoint);
}


void set_random_period(struct task_data_t *task, enum test_result_t exp_res)
{
	minos_tick_t period;
	period = get_random(10000);

	if (period < 5000) // one-shot task
	{
		period = 0;
	}
	else
	{
		period -= 4999;
	}

	update_task_period(task, period, exp_res);
}


void set_random_delay(struct task_data_t *task, enum test_result_t exp_res)
{
	minos_tick_t delay;
	delay = get_random(10000);

	update_task_delay(task, delay, exp_res);
}


void create_n_activate_random_task(struct task_data_t *task, enum test_result_t exp_res)
{
	create_task(task, exp_res);

	set_random_delay(task, exp_res);
	set_random_period(task, exp_res);


	activate_task(task, exp_res);

	task->runpoint = get_minos_time() + task->delay;

	log_next_runpoint(task);
}


void set_random_task_delay_n_period(struct task_data_t *task, enum test_result_t exp_res)
{
	set_random_delay(task, exp_res);

	if (task->delay > 5000) // change period
	{
		set_random_period(task, exp_res);
	}

	if ((task->period == 0) || (task->activations == 0))
	{
		task->runpoint = get_minos_time() + task->delay;
	}
	else
	{
		task->runpoint = get_minos_time() + task->period;
	}

	activate_task(task, exp_res);

	log_next_runpoint(task);
}


