/** @cond license
 * MinOS
 * Copyright (C) 2020 Nicu Gavrila (https://www.linkedin.com/in/nicu-gavrila/)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 * @endcond
 */

#include "../minos/minos.h"

typedef void (*task_type_t)(void);


enum log_type_t
{
	LOG_INF = 0,
	LOG_WAR,
	LOG_ERR,
	LOG_NONE,
};


enum test_result_t
{
	RES_OK = 0,
	RES_NOK,
	RES_NONE,
};


struct task_data_t
{
	task_type_t     func;
	minos_handle_t  handle;
	int             valid;

	int             prio;
	minos_tick_t    period;
	minos_tick_t    delay;
	minos_tick_t    runpoint;

	int             activations;
	int             suspended;
};


void log_data(enum log_type_t type, enum test_result_t result, char *fmt, ...);
void close_logfile(void);


unsigned int get_random(unsigned int max_val);

void create_task(struct task_data_t *task, enum test_result_t exp_res);
void delete_task(struct task_data_t *task, enum test_result_t exp_res);
void stop_task(struct task_data_t *task, enum test_result_t exp_res);
void suspend_task(struct task_data_t *task, enum test_result_t exp_res);
void update_task_delay(struct task_data_t *task,
		       minos_tick_t delay,
		       enum test_result_t exp_res);
void update_task_period(struct task_data_t *task,
		       minos_tick_t period,
		       enum test_result_t exp_res);
void activate_task(struct task_data_t *task, enum test_result_t exp_res);
void log_next_runpoint(struct task_data_t *task);
void set_random_period(struct task_data_t *task, enum test_result_t exp_res);
void set_random_delay(struct task_data_t *task, enum test_result_t exp_res);
void create_n_activate_random_task(struct task_data_t *task, enum test_result_t exp_res);
void set_random_task_delay_n_period(struct task_data_t *task, enum test_result_t exp_res);

int special_action(int percentage);
