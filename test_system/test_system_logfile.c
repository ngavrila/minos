/** @cond license
 * MinOS
 * Copyright (C) 2020 Nicu Gavrila (https://www.linkedin.com/in/nicu-gavrila/)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 * @endcond
 */

#include <signal.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "../minos_vm/minos_vm.h"


#include "../minos/minos_defs.h"

#include "../minos/minos.h"

#include "test_system.h"


FILE *logfile = NULL;

int no_log_inf = 0;
int no_log_war = 0;
int no_log_err = 0;
int no_log_none = 0;

int no_res_ok = 0;
int no_res_nok = 0;
int no_res_none = 0;

void log_data(enum log_type_t type, enum test_result_t result, char *fmt, ...)
{
	va_list arg_list;

	if (logfile == NULL)
	{
		printf("Creating log file... ");

		logfile = fopen("../test_system/log.txt", "w");

		if (logfile == NULL)
		{
			printf("FAILED\n");
			printf("WARNING:  Log file creation failed!\n");
			return;
		}

		printf("OK\n");
	}

	switch (type)
	{
		case LOG_INF:
			no_log_inf++;
			fprintf(logfile, "INFO:     ");
			break;

		case LOG_WAR:
			no_log_war++;
			fprintf(logfile, "WARNING:  ");
			break;

		case LOG_ERR:
			no_log_err++;
			fprintf(logfile, "ERROR:    ");
			break;

		default:
			no_log_none++;
			fprintf(logfile, "          ");
			break;
	}

	switch (result)
	{
		case RES_OK:
			no_res_ok++;
			fprintf(logfile, "OK     ");
			break;

		case RES_NOK:
			no_res_nok++;
			fprintf(logfile, "NOK    ");
			break;

		default:
			no_res_none++;
			fprintf(logfile, "       ");
			break;
	}

	va_start(arg_list, fmt);
	vfprintf(logfile, fmt, arg_list);
	va_end(arg_list);

	fprintf(logfile, "\n");
}


void close_logfile(void)
{
	if (logfile != NULL)
	{
		fprintf(logfile, "\n");
		fprintf(logfile, "\n");
		fprintf(logfile, "= Summary ===========================================");
		fprintf(logfile, "\n");
		fprintf(logfile, "\n");

		fprintf(logfile, "INFO     entries: %d\n", no_log_inf);
		fprintf(logfile, "WARNING  entries: %d\n", no_log_war);
		fprintf(logfile, "ERROR    entries: %d\n", no_log_err);
		fprintf(logfile, "none     entries: %d\n", no_log_none);
		fprintf(logfile, "\n");
		fprintf(logfile, "Test OK    entries: %d\n", no_res_ok);
		fprintf(logfile, "Test NOK   entries: %d\n", no_res_nok);
		fprintf(logfile, "Test none  entries: %d\n", no_res_none);

		fclose(logfile);


		printf("\n");
		printf("\n");
		printf("= Summary ===========================================");
		printf("\n");
		printf("\n");

		printf("INFO     entries: %d\n", no_log_inf);
		printf("WARNING  entries: %d\n", no_log_war);
		printf("ERROR    entries: %d\n", no_log_err);
		printf("none     entries: %d\n", no_log_none);
		printf("\n");
		printf("Test OK    entries: %'d\n", no_res_ok);
		printf("Test NOK   entries: %'d\n", no_res_nok);
		printf("Test none  entries: %'d\n", no_res_none);

	}
}
