# This is MinOS

A [Real-Time Operating System](https://en.wikipedia.org/wiki/Real-time_operating_system)
designed with speed and ease of use in mind. \
It offers the following features:

*  a [non-preemptive](https://en.wikipedia.org/wiki/Cooperative_multitasking) priority-based scheduler
*  a simple mechanism to handle system states
*  a system wide time counter.


MinOS includes only the most common features used in an embedded system and I
did my best to keep fancy stuff out. \
This is meant to be simple, efficient and easy to use (short list of API
functions and short documentation).



If you clone this repo, you will have access to its entire documentation,
which can be found in path: \
**./minos/minos/documentation/documentation.html**

MinOS is a work in progress. \
Bugs may be found and fixed, concepts may change and other components may be added.

I created this RTOS for use in my other hobby projects, so as those projects
evolve, MinOS will evolve, too.

If you find it useful, feel free to use it!


# The Name

The name stands for Minimalist Operating System.

It also happens to be the name of the first King of Crete from Greek mythology. \
[Minos](https://en.wikipedia.org/wiki/Minos) was the son of Zeus and Europa and,
after his death, he became the judge of the dead in the underworld.


# License
All files in this repo are licensed under LGPL V2.1. \
If you want to use MinOS, please use it under the terms of this license.

**_MinOS_** \
**_Copyright (C) 2020 Nicu Gavrilă (https://www.linkedin.com/in/nicu-gavrila/)_**

**_This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version._**

**_This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details._**

**_You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
USA_**

