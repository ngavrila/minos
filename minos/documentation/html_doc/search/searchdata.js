var indexSectionsWithContent =
{
  0: "acdefghjklmnprstu",
  1: "mt",
  2: "cjkm",
  3: "acdeghjkmprst",
  4: "acdflmnprstu",
  5: "jm",
  6: "m",
  7: "m",
  8: "lmu"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "defines"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Enumerator",
  8: "Macros"
};

