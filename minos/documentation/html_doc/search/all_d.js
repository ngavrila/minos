var searchData=
[
  ['release_5fminos_5fstate',['release_minos_state',['../janus_8c.html#a361b953f09323c08a423ecd9bf7df3e4',1,'release_minos_state(enum minos_state_t rq_state):&#160;janus.c'],['../minos_8h.html#a361b953f09323c08a423ecd9bf7df3e4',1,'release_minos_state(enum minos_state_t rq_state):&#160;janus.c']]],
  ['request_5fminos_5frun_5fsubstate',['request_minos_run_substate',['../janus_8c.html#aa6a5e3753cdf66f13d865751ce8a6d63',1,'request_minos_run_substate(minos_run_substate_t rq_substate):&#160;janus.c'],['../minos_8h.html#aa6a5e3753cdf66f13d865751ce8a6d63',1,'request_minos_run_substate(minos_run_substate_t rq_substate):&#160;janus.c']]],
  ['request_5fminos_5fstate',['request_minos_state',['../janus_8c.html#a4b29d07dc3ce884e1e1c34bb5721bbfb',1,'request_minos_state(enum minos_state_t rq_state):&#160;janus.c'],['../minos_8h.html#a4b29d07dc3ce884e1e1c34bb5721bbfb',1,'request_minos_state(enum minos_state_t rq_state):&#160;janus.c']]],
  ['runpoint',['runpoint',['../structtask__entry.html#a4f65df4b910ddcedac1bc4fa1e398949',1,'task_entry']]],
  ['runpoint_5foverflow',['runpoint_overflow',['../structtask__entry.html#a9961cf02200231ff3c3f63d137191e3a',1,'task_entry']]]
];
