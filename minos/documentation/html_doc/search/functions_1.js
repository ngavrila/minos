var searchData=
[
  ['change_5fminos_5ftask_5fdelay',['change_minos_task_delay',['../karajan_8c.html#af1d7264f66da6925bb1a2a25a152cad5',1,'change_minos_task_delay(minos_handle_t handle, minos_tick_t delay):&#160;karajan.c'],['../minos_8h.html#af1d7264f66da6925bb1a2a25a152cad5',1,'change_minos_task_delay(minos_handle_t handle, minos_tick_t delay):&#160;karajan.c']]],
  ['change_5fminos_5ftask_5fperiod',['change_minos_task_period',['../karajan_8c.html#ac055aa1b47221a1d6c59e7fadc33e18b',1,'change_minos_task_period(minos_handle_t handle, minos_tick_t period):&#160;karajan.c'],['../minos_8h.html#ac055aa1b47221a1d6c59e7fadc33e18b',1,'change_minos_task_period(minos_handle_t handle, minos_tick_t period):&#160;karajan.c']]],
  ['chronos',['chronos',['../chronos_8c.html#a8cf5f177ffe72a109bc7f169db75f1d9',1,'chronos(void):&#160;chronos.c'],['../minos__internal_8h.html#a8cf5f177ffe72a109bc7f169db75f1d9',1,'chronos(void):&#160;chronos.c']]],
  ['chronos_5ftime_5finterrupt',['chronos_time_interrupt',['../chronos_8c.html#a89319c1ecaa6adb42bb4c7e7407a6846',1,'chronos_time_interrupt(minos_tick_t ticks):&#160;chronos.c'],['../minos_8h.html#a89319c1ecaa6adb42bb4c7e7407a6846',1,'chronos_time_interrupt(minos_tick_t ticks):&#160;chronos.c']]],
  ['create_5fminos_5ftask',['create_minos_task',['../karajan_8c.html#ac357cf6e73b9b45465b583475197db4f',1,'create_minos_task(void(*task)(void), minos_defdata_t prio):&#160;karajan.c'],['../minos_8h.html#ac357cf6e73b9b45465b583475197db4f',1,'create_minos_task(void(*task)(void), minos_defdata_t prio):&#160;karajan.c']]]
];
