var searchData=
[
  ['next_5frunpoint_5fcalculated',['next_runpoint_calculated',['../karajan_8c.html#ae43c4b39cccc40a0f39dac4279aff1b3',1,'karajan.c']]],
  ['next_5fstate',['next_state',['../structminos__state__transition__t.html#a3d3fc6efd831a4737b69f448354abfd4',1,'minos_state_transition_t::next_state()'],['../janus_8c.html#a3d3fc6efd831a4737b69f448354abfd4',1,'next_state():&#160;janus.c']]],
  ['next_5fsubstate',['next_substate',['../structminos__state__transition__t.html#aa839f2d220ea8784bf737deec9060f77',1,'minos_state_transition_t::next_substate()'],['../janus_8c.html#aa839f2d220ea8784bf737deec9060f77',1,'next_substate():&#160;janus.c']]],
  ['no_5fof_5fjanus_5fcallbacks',['no_of_janus_callbacks',['../janus_8c.html#a2f3e9bdb25500b9ed71db0ae88bd781a',1,'janus.c']]],
  ['number_5fof_5ftasks',['number_of_tasks',['../karajan_8c.html#a6dd860de7104459a98d3cef8ac8d472a',1,'karajan.c']]]
];
