var searchData=
[
  ['set_5fjanus_5fcallback',['set_janus_callback',['../janus_8c.html#a463d4d317de3f67e4eab7d492dc090e2',1,'set_janus_callback(janus_callback_t cb):&#160;janus.c'],['../minos_8h.html#a463d4d317de3f67e4eab7d492dc090e2',1,'set_janus_callback(janus_callback_t cb):&#160;janus.c']]],
  ['setup_5fnext_5ftask_5frunpoint',['setup_next_task_runpoint',['../karajan_8c.html#a998524500996b3c9ced69b9c1424e013',1,'karajan.c']]],
  ['start_5ftimer_5fsignal',['start_timer_signal',['../minos__integration_8h.html#aaf668909920595f587e20103820b3039',1,'minos_integration.h']]],
  ['state_5fchange_5fcallbacks',['state_change_callbacks',['../janus_8c.html#a47a03b720499615a90d25e55b862f951',1,'janus.c']]],
  ['state_5frequests',['state_requests',['../janus_8c.html#a5bc3c0e1ea3da1dfa4840dd3424a70a0',1,'janus.c']]],
  ['stop_5fminos_5ftask',['stop_minos_task',['../karajan_8c.html#aed8da26387122b8591f1d8d64622fdb4',1,'stop_minos_task(minos_handle_t handle):&#160;karajan.c'],['../minos_8h.html#aed8da26387122b8591f1d8d64622fdb4',1,'stop_minos_task(minos_handle_t handle):&#160;karajan.c']]],
  ['substate_5frequest',['substate_request',['../janus_8c.html#a4328cec464dc52d292f213265c7a5882',1,'janus.c']]],
  ['suspend_5fminos_5ftask',['suspend_minos_task',['../karajan_8c.html#a04136799b89ad7526176712a68717f69',1,'suspend_minos_task(minos_handle_t handle):&#160;karajan.c'],['../minos_8h.html#a04136799b89ad7526176712a68717f69',1,'suspend_minos_task(minos_handle_t handle):&#160;karajan.c']]],
  ['suspended',['suspended',['../structtask__entry.html#a67e1ecc5f9d1280b43ab0f1b57653fcf',1,'task_entry']]]
];
