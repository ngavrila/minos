/** @cond license
 * MinOS
 * Copyright (C) 2020 Nicu Gavrila (https://www.linkedin.com/in/nicu-gavrila/)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 * @endcond
 */

#include <stdio.h>

#include "minos_internal.h"

/**
 * @brief Variable that stores the current @minos time.
 *
 * @details All timings in all components of @minos are governed by the value of this
 *   variable. \n
 *
 * It's type, @ref minos_tick_t, can be configured in file @ref minos_cfg.h. \n
 *
 * It is your responsibility to make sure that the largest delays and periods of your
 *   tasks fit in this variable. Otherwise you would get very strange behaviour from
 *   @karajan. As long as you respect this constraint, you should be fine, even if this
 *   variable overflows. \n
 *
 * This variable is updated by the function @ref chronos_time_interrupt and it's value
 *   can be read calling the function @ref get_minos_time.
 */
volatile minos_tick_t chronos_official_time;


/**
 * @brief This is the function that initializes @chronos, the time counter of @minos.
 */
void chronos(void)
{
	chronos_official_time = 0;

	MINOS_START_TIME();
}


void chronos_time_interrupt(minos_tick_t ticks)
{
	chronos_official_time += ticks;
}


minos_tick_t get_minos_time(void)
{
	minos_tick_t ret_val;

	MINOS_INTERRUPT_DISABLE();
	ret_val = chronos_official_time;
	MINOS_INTERRUPT_RESTORE();

	return ret_val;
}




/**
 * @file
 *
 * @chronos is the @minos component that implements time keeping. \n \n
 *
 *
 * It's the simplest component of @ref minos.c "MinOS" and it only consists of a variable
 *   that counts the time as it passes, @ref chronos_official_time. \n \n
 *
 *
 * Its initialization function, @ref chronos(), will call the @ref MINOS_START_TIME
 *   macro. You should assign a function to this macro in @ref minos_integration.h. \n
 *
 * If you use a timer, this function that you assign there should start this timer which
 *   should call @ref chronos_time_interrupt on a regular basis. \n
 *
 * You can find a coding example in section @ref mainpage_code_example_timer of the
 *   @ref index "Main Page". \n \n
 *
 *
 * Once @ref chronos.c "Chronos" calls @ref MINOS_START_TIME, it is ready to receive
 *   time information. This is done by an external SW component calling function
 *   @ref chronos_time_interrupt from @ref chronos.c "Chronos", therefore informing it
 *   about how many <c><b>ticks</b></c> have passed since the last
 *   @ref chronos_time_interrupt call. \n
 *
 * You must ensure that the @ref chronos_time_interrupt function will be called every
 *   <c><b>tick</b></c>, otherwise some timings may not be accurate. \n \n
 *
 *
 * A <c><b>tick</b></c> is the time unit used in every @ref minos.c "MinOS" component.
 *   You define this unit. If you want it to be 1 millisecond, you just call
 *   @ref chronos_time_interrupt with parameter <c><b>1</b></c> every time a millisecond
 *   elapses. The same is valid if you want to use a <c><b>tick</b></c> of 17
 *   milliseconds, you just call @ref chronos_time_interrupt with parameter
 *   <c><b>1</b></c> every time 17 milliseconds elapse. \n
 *
 * Just keep in mind that the data type used to store <c><b>ticks</b></c> is an integer
 *   and one <c><b>tick</b></c> is the smallest unit of time. You won't be able to use
 *   values like <c><b>1.7 ticks</b></c>. It's either <c><b>1 tick</b></c> or
 *   <c><b>2 ticks</b></c>. \n \n
 *
 * The current time is stored in variable @ref chronos_official_time and its data type,
 *   @ref minos_tick_t, is configurable in file @ref minos_cfg.h. \n \n \n
 *
 *
 *
 * And that's about it with the documentation of @ref chronos.c "Chronos". Quick and
 *   simple. \n \n
 *
 *
 * Regarding its name, it comes from the personification of time in pre-Socratic
 *   philosophy and later literature. It may or may not have connections with the
 *   Titan Cronus from Greek mythology. \n
 *
 * You can read more about it on its
 * <a href="https://en.wikipedia.org/wiki/Chronos">Wikipedia page</a>.
 */
