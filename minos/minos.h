/** @cond license
 * MinOS
 * Copyright (C) 2020 Nicu Gavrila (https://www.linkedin.com/in/nicu-gavrila/)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 * @endcond
 */

#ifndef MINOS_H
#define MINOS_H

#include "minos_defs.h"


/**
 * @brief @minos start function.
 *
 * @details Once you call it, @minos boots. This function will not return because
 *   It is implemented as an infinite loop. The code it executes on each loop is
 *   dependant on the system state. \n
 *
 * For more details, see the @ref minos_normal_operation_loops section in @minos
 *   documentation. \n
 *
 * For more details about system states, see @janus.
 */
void minos(void);


/**
 * @brief Function to be called to keed @minos informed about the passage of time.
 *
 * @details This function should be called by the system timer ISR, but it's safe to call
 *   it from any context as long as you adhere to the following: The <c><b>tick</b></c>
 *   is a time unit that you define and @minos will exclusively use it to measure time.
 *   Call this function with the number of passed <c><b>ticks</b></c> since the last
 *   call as a parameter. \n
 *
 * If you don't assign a function to @ref MINOS_START_TIME in @ref minos_integration.h,
 *   this function will be called from @ref minos() on every loop with parameter
 *   <c><b>1</b></c>. Therefore the timings will be heavily influenced by what is being
 *   executed. \n
 *
 * For a code example, see the @ref mainpage_code_example_timer section on the
 *   @ref index "Main Page".
 *
 * @param ticks    The number of ticks elapsed since the last call
 */
void chronos_time_interrupt(minos_tick_t ticks);


/**
 * @brief Use this function to create a new task.
 *
 * @details See the @ref karajan_task_creation section of the documentation for @karajan
 *   for more information about the task creation.
 *
 * @param task The pointer to the function that will become a task.
 * @param prio The priority number of the newly created task. The lower the priority
 *               number is, the higher the priority.
 *
 * @return A positive handle to the newly created task if successful,
 *         otherwise @ref MINOS_ERROR
 */
minos_return_t  create_minos_task(void (*task)(void), minos_defdata_t prio);


/**
 * @brief This function deletes a task from @minos.
 *
 * @details This operation is not frequently used in an embedded system, but it may
 *   be handy. \n
 *
 * See the @ref karajan_task_creation section of the documentation for @karajan
 *   for more information about the task deletion.
 *
 * @param handle The handle of the task to be deleted
 *
 * @return @ref MINOS_SUCCESS if task successfuly deleted, otherwise @ref MINOS_ERROR
 */
minos_return_t  delete_minos_task(minos_handle_t handle);


/**
 * @brief This function changes the period of a task.
 *
 * @details By default, the period of a task is <c><b>0</b></c>, therefore the task is
 *   one-shot. If you change it to nonzero, the task will become periodic, with the set
 *   period. \n
 *
 * If a task is already set as periodic, if you change the period back to <c><b>0</b></c>
 *   it will become one-shot. \n
 *
 * Changing between task types will automatically stop a task. \n \n
 *
 *
 * For more information, check the following sections of the documentaiton for @karajan:
 *   * @ref karajan_task_parameters (or the @ref task_entry::period "period" parameter)
 *   * @ref karajan_task_types
 *     * @ref karajan_one_shot_tasks
 *     * @ref karajan_periodic_tasks
 *
 * @param handle The handle of the task
 * @param period The new period for the task. If <c><b>period</b></c> is 0, the
 *               task will become one-shot.
 *
 * @return @ref MINOS_SUCCESS if the period was successfuly updated,
 *         otherwise @ref MINOS_ERROR
 */
minos_return_t  change_minos_task_period(minos_handle_t handle, minos_tick_t period);


/**
 * @brief This function changes the delay of a task.
 *
 * @details For more information, check the following sections of the documentaiton for
 *   @karajan:
 *   * @ref karajan_task_parameters (or the @ref task_entry::delay "delay" parameter)
 *   * @ref karajan_task_types
 *     * @ref karajan_one_shot_tasks
 *     * @ref karajan_periodic_tasks
 *
 * @param handle The handle of the task
 * @param delay  The new delay of the task.
 *
 * @return @ref MINOS_SUCCESS if the delay was successfuly updated,
 *         otherwise @ref MINOS_ERROR
 */
minos_return_t  change_minos_task_delay(minos_handle_t handle, minos_tick_t delay);


/**
 * @brief Activate a task.
 *
 * @details That means @minos can now schedule the referenced task for execution. If the
 *   task is @ref karajan_one_shot_tasks "one-shot", it will be executed once per
 *   activation. If the task is  @ref karajan_periodic_tasks "periodic", it will start
 *   executing periodically once activated until
 *   @ref karajan_task_activate_stop_suspend "stopped" with @ref stop_minos_task or
 *   @ref karajan_task_activate_stop_suspend "suspended" with @ref suspend_minos_task. \n
 *
 * Check the @ref karajan_task_activate_stop_suspend section in @karajan documentation
 *   for more details.
 *
 * @param handle The handle of the task
 *
 * @return @ref MINOS_SUCCESS if the task was successfuly activated,
 *         otherwise @ref MINOS_ERROR
 */
minos_return_t  activate_minos_task(minos_handle_t handle);


/**
 * @brief @ref karajan_task_activate_stop_suspend "Suspend" a currently active
 *   @ref karajan_periodic_tasks "periodic" task.
 *
 * @details The time counters for a @ref karajan_task_activate_stop_suspend "suspended"
 *   task continue to run normally, just the way they would if the task was active. The
 *   only difference is that the function of the task is never called. \n
 *
 * The function fails if the handle is invalid or the task is
 *   @ref karajan_one_shot_tasks "one-shot" or not activated. \n
 *
 * Check the @ref karajan_task_activate_stop_suspend section in @karajan documentation
 *   for more details.
 *
 * @param handle The handle of the task
 *
 * @return @ref MINOS_SUCCESS if the task was successfuly suspended,
 *         otherwise @ref MINOS_ERROR
 */
minos_return_t suspend_minos_task(minos_handle_t handle);


/**
 * @brief @ref karajan_task_activate_stop_suspend "Stop" a task.
 *
 * @details That means @minos will now stop executing the referenced task. @karajan
 *   will ignore the task as if it doesn't exist. \n
 *
 * Check the @ref karajan_task_activate_stop_suspend section in @karajan documentation
 *   for more details.
 *
 * @param handle The handle of the task
 *
 * @return @ref MINOS_SUCCESS if the task was successfuly stopped,
 *         otherwise @ref MINOS_ERROR
 */
minos_return_t  stop_minos_task(minos_handle_t handle);


/**
 * @brief Returns the current @minos state.
 *
 * @details Check the @ref janus_states section in @janus documentation for more
 *   information on states.
 *
 * @return Current @minos state
 */
enum minos_state_t get_minos_state(void);


/**
 * @brief Register a callback with @janus.
 *
 * @details This callback will be called every time there is a state or substate
 *   transition. \n
 *
 * For more details about callbacks, states, substates and transitions, check the
 *   following sections from the documentation for @janus:
 *
 *   * @ref janus_callbacks
 *   * @ref janus_states
 *   * @ref janus_run_substates
 *   * @ref janus_state_transitions
 *
 * The callback will receive as parameter a structure of type
 *   @ref minos_state_transition_t. Check its documentation for more details.
 *
 *
 * @param cb Pointer to the callback function
 *
 * @return @ref MINOS_SUCCESS if callback successfuly registered,
 *         otherwise @ref MINOS_ERROR
 */
minos_return_t set_janus_callback(janus_callback_t cb);


/**
 * @brief Request a @minos state.
 *
 * @details @minos will go to the highest requested state. The following sections from
 *   @janus documentation offer more details about states, state transitions and state
 *   requests:
 *
 *   * @ref janus_states
 *   * @ref janus_state_transitions
 *   * @ref janus_lowest_state
 *   * @ref janus_state_request_number
 *   * @ref janus_critical_error_state
 *
 * @param rq_state Requested state
 */
void request_minos_state(enum minos_state_t rq_state);


/**
 * @brief Releases a @minos state.
 *
 * @details @minos will go to the highest requested state. The following sections from
 *   @janus documentation offer more details about states, state transitions and state
 *   requests:
 *
 *   * @ref janus_states
 *   * @ref janus_state_transitions
 *   * @ref janus_lowest_state
 *   * @ref janus_state_request_number
 *   * @ref janus_critical_error_state
 *
 * @param rq_state Released state
 */
void release_minos_state(enum minos_state_t rq_state);


/**
 * @brief Returns the current @minos substate.
 *
 * @details Check the @ref janus_run_substates section in @janus documentation for more
 *   information on substates.
 *
 * @return Current @minos substate
 */
minos_run_substate_t get_minos_run_substate(void);


/**
 * @brief Request a @minos substate. Substates are only available in @ref MINOS_RUN state.
 *
 * @details @minos will go to the latest requested substate. \n
 *
 * Check the @ref janus_run_substates section in @janus documentation for more
 *   information on substates.
 *
 * @param rq_substate Requested substate
 *
 * @return @ref MINOS_SUCCESS if request acknowledged, otherwise @ref MINOS_ERROR
 */
minos_return_t request_minos_run_substate(minos_run_substate_t rq_substate);


/**
 * @brief The @ref MINOS_CRITICAL_ERROR state is set in this function and the system is
 *   stopped.
 *
 * @details It doesn't matter in which state the system is currently, the error state is
 *   set and the execution stops in an infinite loop awaiting reset. \n
 *
 * @minos doesn't set this state in it's current implementation. \n
 *
 * See the @ref janus_critical_error_state section in @janus documentation for more
 *   details on the handling of @ref MINOS_CRITICAL_ERROR state.
 */
void enter_os_critical_error_state(void);


/**
 * @brief Returns the current @minos time which is stored in variable
 *   @ref chronos_official_time from @chronos.
 *
 * @details Its return type, @ref minos_tick_t, can be configured in file
 *   @ref minos_cfg.h. There are some restrictions regarding its size and if those are
 *   met, @minos will correctly handle overflows. Check the documentation for
 *   @ref minos_tick_t for more details. \n
 *
 * If you plan to use this function to get the current time, all you get is the value of
 *   @ref chronos_official_time, with no way of finding out from @chronos if it has ever
 *   overflowed or not. You must implement a mechanism of your own if you wish to detect
 *   that.
 *
 * @return Current @minos time
 */
minos_tick_t get_minos_time(void);




#endif // MINOS_H


/**
 * @mainpage
 *
 * @tableofcontents
 *
 *
 * \n \n @section mainpage_introduction Introduction
 *
 * @minos is a <a href="https://en.wikipedia.org/wiki/Real-time_operating_system">
 *   Real-Time Operating System</a> designed with speed and ease of
 *   use in mind. It offers the following features:
 *    * a <a href="https://en.wikipedia.org/wiki/Cooperative_multitasking">
 *      non-preemptive</a> priority-based scheduler
 *    * a simple mechanism to handle system states
 *    * a system wide time counter.
 *
 * If you don't need a <a href="https://en.wikipedia.org/wiki/Preemption_(computing)">
 *   preemptive</a> system, you just need a fast RTOS that can execute tasks and provide
 *   you with a simple way of handling system states, this is the one. \n
 *
 * @minos includes only the most common features used in an embedded
 *   system and I did my best to keep fancy stuff out. This 
 *   <a href="https://en.wikipedia.org/wiki/Real-time_operating_system">RTOS</a> is meant
 *   to be simple, efficient and easy to use (short list of @ref minos.h "API" functions
 *   and short documentation). \n \n
 *
 * 
 * This documentation assumes that you are a bit familiar with embedded software. You
 *   should know what a microcontroller is and how it works and you should know how to
 *   use its peripheral units, especially a timer. The documentation also talks about
 *   tasks from the point of view of an operating system, so you may want to be familiar
 *   with that concept, too. Here are some links that can get you up to speed:
 * 
 *   * <a href="https://en.wikipedia.org/wiki/Embedded_system">Embedded system</a>
 * 
 *   * <a href="https://en.wikipedia.org/wiki/Embedded_software">Embedded software</a>
 * 
 *   * <a href="https://en.wikipedia.org/wiki/Microcontroller">Microcontroller</a>
 * 
 *   * <a href="https://en.wikipedia.org/wiki/Programmable_interval_timer">
 *     Timer</a>
 * 
 *   * <a href="https://en.wikipedia.org/wiki/Real-time_operating_system">
 *     Real-Time Operating System</a>
 * 
 *   * <a href="https://en.wikipedia.org/wiki/Task_(computing)">Task</a>
 * 
 *   * <a href="https://en.wikipedia.org/wiki/Scheduling_(computing)">Scheduler</a>
 * 
 *   * <a href="https://en.wikipedia.org/wiki/Cooperative_multitasking">
 *      Non-preemptive (Cooperative) task scheduler</a>
 * 
 *
 * Integrating @minos into your existing projects, or designing a new
 *   system around it is as straight forward as possible. The only header file you need
 *   to include in your files is @ref minos.h. @ref minos.h also documents the
 *   @ref minos.h "API" of @minos. Here is what you need to do to create a software
 *   system around @minos:
 *
 *   * First of all, add a call to @ref minos() in your <c><b>main()</b></c> function
 * 
 *   * Create the source code of your software component(s)
 *
 *   * Update file @ref minos_integration.h and add the functions @minos should know
 *     about and call. 
 * 
 *   * Optionally, you can enable a timer, so that @minos can keep track of time.
 *
 *   * The third and final step is to check the @ref minos_cfg.h if there are any
 *     configurations you need to make in @minos to correctly map to your system. For
 *     most simple applications the default configuration should be enough, but you may
 *     want to tweak some options here and there.
 *
 *
 *
 * \n \n @section mainpage_code_example Code Examples and Templates
 *
 * Let's take a code example (more like stubs) which shows you how to create your
 *   own component and system and how to integrate them with @minos.
 *
 * 
 * \n @subsection mainpage_code_example_main Calling minos() From Your main() Function
 * 
 * The very first thing you should do is to create a <c><b>main.c</b></c> file to host
 *   your <c><b>main()</b></c> function. This function should call @ref minos():
 *
 * @code
 *
 * void main(void)
 * {
 * 	minos(); // This function will never return.
 * }
 *
 * @endcode
 *
 *
 * \n @subsection mainpage_code_example_user_cmp Creating Your Software Component(s)
 *
 * Now you should create a new software component. The source file would be the name of
 *   your component dot c, for example <c><b>component.c</b></c>. Let's have a look at a
 *   template of this file: \n
 *
 * @code
 *
 * #include "minos.h"
 *
 *
 * minos_handle_t one_shot_task_handle;
 * minos_handle_t periodic_task_handle;
 *
 *
 * void one_shot_task(void);
 * void periodic_task(void);
 *
 *
 * void component(void)
 * {
 *         // This function initializes your component.
 *         // Do whatever initializations are required here.
 *         // This function will be called once by MinOS at the system startup.
 *
 *         one_shot_task_handle = create_minos_task(one_shot_task, 10);
 *         periodic_task_handle = create_minos_task(periodic_task, 20);
 *
 *         if (periodic_task_handle != MINOS_ERROR)
 *         {
 *                 change_minos_task_period(periodic_task_handle, 10);
 *                 activate_minos_task(periodic_task_handle);
 *         }
 *
 * }
 *
 *
 * void one_shot_task(void)
 * {
 *         // Do some important things here.
 *         // I plan to run this function every once in a while, when required
 * }
 *
 *
 * void periodic_task(void)
 * {
 *         // Do some more important stuff here.
 *         // I plan to run this function regularly.
 *
 *         // Whenever the times come, activate the one_shot_task...
 *                 activate_minos_task(one_shot_task_handle);
 * }
 *
 * @endcode
 *
 * There is a bit of detail there, but it's not that complicated! \n
 *
 * A <c><b>one-shot</b></c> task is a task that is called as many times as you call
 *   function @ref activate_minos_task to activate it. \n
 *
 * A <c><b>periodic</b></c> task is a task that is called periodically once you call
 *   function @ref activate_minos_task to activate it. \n \n
 *
 * 
 * The first parameter of function @ref create_minos_task is the task itself, the
 *   function you created and you need it to become a task. \n
 *
 * The second parameter is the priority number of the task. The lower this number, the
 *   higher the priority of that task. \n \n
 *
 *
 * The first parameter in function @ref change_minos_task_period is the handle of the
 *   task that we want to change. This handle has been returned at creation time by
 *   function @ref create_minos_task. \n
 * 
 * The second parameter in function @ref change_minos_task_period tells @minos how often
 *   is that task to be called. The unit of that time period is the <c><b>tick</b></c>.
 *
 * 
 * If you want to know more, you can read the documentation on
 *   @ref karajan_task_types "Types of Tasks",
 *   @ref karajan_one_shot_tasks "One-Shot Tasks",
 *   @ref karajan_periodic_tasks "Periodic Tasks" and
 *   @ref karajan_task_activate_stop_suspend "How to Activate, Suspend and Stop Tasks" \n
 *
 * 
 * \n @subsection mainpage_code_example_integration_h Modifying minos_integration.h
 *
 * In order for the @minos to work properly, you need to supply some code
 *   that @minos can execute from your system. Here we see how to do that
 *   for the two most important ones: \n \n
 *
 *
 * In file @ref minos_integration.h you will find a list defined like this:
 *
 * @code
 *
 * void test_system(void);
 * // documentation stuff on multiple lines...
 * static void (*user_init_vector[])(void) =
 * {
 * 	test_system,
 * 	MINOS_NULL
 * };
 * @endcode
 *
 * If <c><b>test_system</b></c> is there, remove it. \n \n
 *
 * Add your component in the list, like this:
 *
 * @code
 *
 * void component(void);
 * // documentation stuff on multiple lines...
 * static void (*user_init_vector[])(void) =
 * {
 * 	component,
 * 	MINOS_NULL
 * };
 * @endcode
 *
 * If you have more than one, list them all here before the @ref MINOS_NULL entry. \n \n
 *
 *
 * \n @subsection mainpage_code_example_timer Enabling a Timer
 *
 * If you find this section too complex, or you just don't want to set a timer, that's ok.
 *   Just make sure that the @ref MINOS_START_TIME macro in @ref minos_integration.h
 *   looks like this:
 *
 * @code
 *
 * void minos_empty_function(void);
 * // documentation stuff on multiple lines...
 * #define MINOS_START_TIME      minos_empty_function
 *
 * @endcode
 *
 * All timings will now be relative (the system won't be 
 *   <a href="https://en.wikipedia.org/wiki/Real-time_computing">real-time</a> at all),
 *   but you don't have to bother with anything else, you can skip ahead to
 *   @ref configurations_in_minos_cfg_h. \n \n
 *
 *
 * But, as a <a href="https://en.wikipedia.org/wiki/Real-time_operating_system">
 *   Real Time Operating System</a>, @minos should be aware of the time. That being said,
 *   you should start a timer that will request an interrupt periodically. You would have
 *   to implement this feature, because it's dependant on what microcontroller or
 *   processor you are using. \n
 *
 * You will have to supply @minos with a function that it can call to start
 *   the timer and the interrupt service routine of that timer should call function
 *   @ref chronos_time_interrupt from @minos, usually with parameter
 *   <c><b>1</b></c>. Here is a template: \n \n
 *
 *
 * <c><b>system_timer.c</b></c>
 *
 * @code
 *
 * #include "minos.h"
 *
 *
 * void start_system_timer(void)
 * {
 *         // Initialize here all the registers needed for the timer.
 *         // Configure the timer to generate an interrupt and suplly an ISR.
 *         // Enable the timer.
 * }
 *
 *
 * // add here any pragmas required by the compiler
 * void system_timer_isr(void)
 * {
 *         chronos_time_interrupt(1);
 *         // clear interrupt flag
 * }
 *
 * @endcode
 *
 * Now you will have to tell @minos about the function
 *   <c><b>start_system_timer</b></c> you implemented. And you do that by adding it
 *   in file @ref minos_integration.h where the @ref MINOS_START_TIME macro is defined:
 *
 * @code
 *
 * void start_timer_signal(void);
 * // documentation stuff on multiple lines...
 * #define MINOS_START_TIME      start_timer_signal
 *
 * @endcode
 *
 * Replace the <c><b>start_timer_signal</b></c> (or whatever else is there) with your
 *   function:
 *
 * @code
 *
 * void start_system_timer(void);
 * // documentation stuff on multiple lines...
 * #define MINOS_START_TIME      start_system_timer
 *
 * @endcode
 *
 * When it's ready, @minos will call your function to start the timer. In
 *   turn, this will start the periodic interrupt that will call function
 *   @ref chronos_time_interrupt from @minos. \n
 *
 * However, because you are now calling a @minos API function from an
 *   interrupt, you must supply functions to disable and enable interrupts to macros
 *   @ref MINOS_INTERRUPT_DISABLE and @ref MINOS_INTERRUPT_RESTORE. Check their
 *   documentation for more details. \n \n
 * 
 *
 * One more thing. Remember the <c><b>tick</b></c>? If you are using the template above,
 *   the period between the timer interrupts is your <c><b>tick</b></c>. And the periodic
 *   task we configured in the example component has a period of 10 <c><b>ticks</b></c>.
 *
 *
 * \n @subsection configurations_in_minos_cfg_h Configuring MinOS
 *
 * There's not much to configure using the above example. One item would be the number of
 *   tasks supported by @minos. You can configure @minos by
 *   modifying definitions and macros in file @ref minos_cfg.h. There you can find the
 *   following macro:
 *
 * @code
 * #define MAX_MINOS_TASKS         10
 * @endcode
 *
 * Since we are only using two tasks, we can downsize the definition above to avoid
 *   wasting memory:
 *
 * @code
 * #define MAX_MINOS_TASKS         2
 * @endcode
 *
 * Just be aware, @minos will support only the number of tasks you
 *   configure here! \n \n
 *
 *
 * And just make sure that @minos won't go to sleep, that is the macro
 *   @ref LOWEST_JANUS_STATE is set to @ref MINOS_RUN :
 *
 * @code
 * #define LOWEST_JANUS_STATE   MINOS_RUN
 * @endcode
 *
 * If you want to know more about the system states, check the documentation for
 *   @ref janus.c "Janus". \n \n
 *
 * That's about it. You have made a quick system using @minos. \n \n
 *
 *
 *
 * \n \n @section more_api_doc More Documentation on How to Use MinOS
 *
 * Check the other tabs for more documentation on @minos. They explain in
 *   more detail how the system can be used, what features it has and it offers complete
 *   lists of APIs, configurations and definitions provided by @minos:
 *
 *   * @ref minos_integration.h "Integration"
 *
 *     * Shows you what else you have to do to integrate @minos into a
 *       software system, besides populating the @ref user_init_vector list.
 *
 *   * @ref minos_cfg.h "Configuration"
 *
 *     * Contains documentation about various configurations that can be applied to
 *       @minos to tailor it to your specific needs.
 *
 *   * @ref minos.h "API"
 *
 *     * Contains a list of all functions that @minos provides.
 *
 *   * @ref minos_defs.h "Definitions"
 *
 *     * Documents all macros and data types that @minos defines and uses.
 *
 *
 *
 * \n \n @section more_internal_doc Documentation about the Internal Design of MinOS
 *
 *
 * Just in case you think the name sounds familiar, it has been borrowed from Greek
 *   mythology. <a href="https://en.wikipedia.org/wiki/Minos">Minos</a> was the first
 *   King of Crete, son of Zeus and Europa. After his death, Minos became a judge of the
 *   dead in the underworld. \n
 *
 * So, Minos was the King of the Underworld in Greek mythology. Now MinOS can be the King
 *   of the Underworld in your software system! \n \n
 *
 *
 * I used the same approach for its component, too. Instead of choosing technical names,
 *   the components of @minos are named after some human or mythical being. \n
 *
 * It may be easier to remember them and their functionality this way rather than
 *   memorizing some strange acronym that may clash with some other name you already
 *   know. \n \n
 *
 *
 * If you want to know more about @minos and how it works, check the
 *   @ref minos.c "Internal Design" dropdown tab:
 *
 *   * @minos
 *
 *     * Offers an overview of the design on @minos and its components.
 *       This page also shows how @minos behaves in different system states.
 *
 *   * @chronos
 *
 *     * @minos component that keeps track of time.
 *       <a href="https://en.wikipedia.org/wiki/Chronos">Chronos</a> is also the
 *       personification of time in pre-Socratic philosophy and later literature.
 *
 *   * @janus
 *
 *     * @minos component that handles the system states. It's named after
 *       <a href="https://en.wikipedia.org/wiki/Janus">Janus</a>, Roman god of beginnings,
 *       transitions and endings, among others.
 *
 *   * @karajan
 *
 *     * This is the @minos component that handles the tasks. It decides
 *       what task is executed and at what time. It's named after a famous
 *       20<sup>th</sup> century Austrian conductor,
 *       <a href="https://en.wikipedia.org/wiki/Herbert_von_Karajan">Herbert von Karajan</a>.
 *
 * \n
 *
 *
 *
 */
