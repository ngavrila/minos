/** @cond license
 * MinOS
 * Copyright (C) 2020 Nicu Gavrila (https://www.linkedin.com/in/nicu-gavrila/)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 * @endcond
 */

#ifndef MINOS_CFG_H
#define MINOS_CFG_H

/**
 * @brief This defines the size of the variables @minos uses to count system time.
 *
 * @details It should be of unsigned type, because a negative value for time doesn't
 *   sound right... \n
 *
 * The time unit used in @minos is the <c><b>tick</b></c>. The duration of one
 *   <c><b>tick</b></c> is defined by you by the way you configure the system timer and
 *   how you call the @ref chronos_time_interrupt function. \n
 *
 * A variable of this type must be able to store a larger value than the maximum task
 *   period or delay. \n
 *
 * @ref chronos_official_time is a variable of this type and, as the name suggests,
 *   it stores the current system time from @minos point of view. \n
 *
 * @minos won't be affected if it overflows as long as you make sure the above
 *   constraint is respected. \n
 *
 * If the @ref chronos_official_time variable is likely to overflow, you must take that
 *   into consideration if you plan to call @ref get_minos_time(), since this function
 *   will simply return the value of the @ref chronos_official_time variable. \n
 *
 * Check the documentation of @chronos for more details. \n
 *
 * A short coding example is shown in the @ref mainpage_code_example_timer section
 *   of the @ref index "Main Page".
 */
typedef minos_u64    minos_tick_t;


/**
 * @brief This data type is returned by all @minos @ref minos.h "APIs" that return
 *   someting.
 *
 * @details It should be of the signed type, because the returned values defined by
 *   @ref MINOS_RETURN_VALUES are 0 or negative. It should also be large enough to store
 *   the values of @ref MAX_JANUS_CALLBACKS and @ref MAX_MINOS_TASKS in their
 *   positive form! \n
 *
 * It is configurable because the speed with which a CPU accesses variables of different
 *   data types is not equal. For example, an 8-bit CPU accesses an 8-bit variable
 *   a bit faster than a 32-bit variable. On the other hand, a 32-bit CPU may access a
 *   32-bit variable a bit faster than an 8-bit variable. \n
 *
 * Configuring this data type correctly may result in a slight increase in speed.
 */
typedef minos_s32    minos_return_t;


/**
 * @brief This data type is used by @minos when it doesn't care about the data type.
 *
 * @details It's used mostly for flags and other things like that. It should be an
 *   unsigned type. \n
 *
 * It is configurable because the speed with which a CPU accesses variables of different
 *   data types is not equal. For example, an 8-bit CPU accesses an 8-bit variable
 *   a bit faster than a 32-bit variable. On the other hand, a 32-bit CPU may access a
 *   32-bit variable a bit faster than an 8-bit variable. \n
 *
 * Configuring this data type correctly may result in a slight increase in speed.
 */
typedef minos_u8     minos_defdata_t;


/**
 * @brief @ref minos_handle_t is used by @minos to store handles (e.g. task handles).
 *
 * @details It is of type @ref minos_defdata_t by default. You must guarantee that this
 *   data type can store the maximum number of tasks as defined by @ref MAX_MINOS_TASKS
 *   and the maximum number of of Janus callbacks as defined by @ref MAX_JANUS_CALLBACKS.
 */
typedef minos_defdata_t      minos_handle_t;


/**
 * @brief This data type is used to store the @minos substate, which is available when
 *   the system is in @ref MINOS_RUN state.
 *
 * @details Check the @ref janus_run_substates section in the documentation for @janus
 *   for more information on substates.
 */
typedef minos_defdata_t      minos_run_substate_t;


/**
 * @brief This configures the maximum number of @janus callbacks that you can have
 *   registered at the same time.
 *
 * @details @minos doesn't use dynamic memory allocation. Having a much higher number than
 *   required wastes memory. Having a lower one causes @ref set_janus_callback to report
 *   an error when all slots are in use and you try to add another callback.
 *
 *
 * For more details about @janus callbacks check the following sections from the
 *   documentation:
 *
 *   * @ref janus_callbacks
 *   * @ref janus_states
 *   * @ref janus_run_substates
 *   * @ref janus_state_transitions
 *   * @ref minos_state_transition_t
 *   * @ref janus_callback_t
 */
#define MAX_JANUS_CALLBACKS     10


/**
 * @brief This configures the maximum number of tasks that you can have registered at
 *   the same time.
 *
 * @details @minos doesn't use dynamic memory allocation. Having a much higher number than
 *   required wastes memory. Having a lower one causes @ref set_janus_callback to report
 *   an error when all slots are in use and you try to add another callback. \n
 *
 * You can find more details on the @ref karajan_task_creation section in @karajan
 *   documentation.
 */
#define MAX_MINOS_TASKS         10

/**
 * @brief This configuration sets the lowest state @janus can go to.
 *
 * @details Obviously, after the system is initialized and leaves the
 *   @ref MINOS_USER_INIT state for @ref MINOS_RUN. Again, obviously, @janus won't go
 *   lower than @ref MINOS_SLEEP, even if you configure a lower state here. A visual
 *   representation can be found @ref janus_lowest_state "here".
 */
#define LOWEST_JANUS_STATE   MINOS_SLEEP





#endif // MINOS_CFG_H


/**
 * @file
 *
 * On this page you can see the various configurations you can apply to @minos. Take a
 *   look and see what you can do to make it better suited for your application. \n
 *
 * Some data type definitions can be found here because, well, they are configurable...
 *   By changing them you can lower the RAM/ROM footprint for @minos, slightly
 *   increase/decrease speed etc.
 */
