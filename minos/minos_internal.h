/** @cond license
 * MinOS
 * Copyright (C) 2020 Nicu Gavrila (https://www.linkedin.com/in/nicu-gavrila/)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 * @endcond
 */

#ifndef MINOS_INTERNAL_H
#define MINOS_INTERNAL_H

#define MINOS_INTERNAL_DEFS
#include "minos.h"
#include "minos_integration.h"
#undef MINOS_INTERNAL_DEFS

/****************************************************************************************
 ** MinOS component init functions ******************************************************
 ****************************************************************************************/


void chronos(void);
void janus(void);
void karajan(void);


void karajan_leaves_janus_state(enum minos_state_t old_state);


minos_return_t process_minos_states(void);


minos_return_t  karajan_schedule(void);


void minos_empty_function(void);


#endif // MINOS_INTERNAL_H


/**
 * @file
 *
 * This header file is used internally by all @minos components. It includes other
 *   headers and also provides definitions and prototypes used between @minos
 *   components.
 *
 */
