/** @cond license
 * MinOS
 * Copyright (C) 2020 Nicu Gavrila (https://www.linkedin.com/in/nicu-gavrila/)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 * @endcond
 */

#include "minos_internal.h"


/**
 * @brief A placeholder function that does nothing.
 *
 * @details It's main purpose is to serve as a placeholder for functions that you don't
 *   implement in your system, but they are required in the
 *   @ref minos_integration.h "Integration" file. \n
 *
 * It's a function that does absolutely nothing, but it does it very well!
 */
void minos_empty_function(void)
{
}



/**
 * @file
 *
 * This file implements functions that are common to a few @minos components. \n
 *
 * For the moment it's only one, but who knows what will happen in the future?
 *
 */
