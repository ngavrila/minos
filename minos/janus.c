/** @cond license
 * MinOS
 * Copyright (C) 2020 Nicu Gavrila (https://www.linkedin.com/in/nicu-gavrila/)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 * @endcond
 */

#include "minos_internal.h"

/**
 * @brief The maximum number of times a state can be requested
 */
#define MAX_STATE_REQUESTS    0xFF


/**
 * @brief Current @minos state
 *
 * @details See the @ref janus_states section for more information on states. \n
 *
 * See the @ref janus_state_transitions section for more information on state
 *   transitions.
 **/
static enum minos_state_t current_state = MINOS_HW_INIT;


/**
 * @brief Next @minos state as determined by the @ref process_minos_states function based
 *   on @ref state_requests
 *
 * @details See the @ref janus_states section for more information on states. \n
 *
 * See the @ref janus_state_transitions section for more information on state
 *   transitions.
 */
static enum minos_state_t next_state    = MINOS_HW_INIT;


/**
 * @brief The number of current requests for each @minos state
 *
 * @details See the @ref janus_states section for more information on states. \n
 *
 * See the @ref janus_state_transitions section for more information on state
 *   transitions.
 */
static minos_defdata_t    state_requests[MINOS_CRITICAL_ERROR];

/**
 * @brief Current @minos substate
 *
 * @details See the @ref janus_run_substates section for more information about
 *   substates.
 */
minos_run_substate_t current_substate = 0;



/**
 * @brief Next @minos substate as determined by the @ref process_minos_states function
 *   based on @ref substate_request
 *
 * @details See the @ref janus_run_substates section for more information about
 *   substates.
 */
minos_run_substate_t next_substate = 0;


/**
 * @brief Currently requested @minos substate
 *
 * @details See the @ref janus_run_substates section for more information about
 *   substates.
 */
minos_run_substate_t substate_request = 0;


/**
 * @brief This variable stores the registered @janus callbacks.
 *
 * @details It's size can be configured using the @ref MAX_JANUS_CALLBACKS macro in
 *   @ref minos_cfg.h
 *
 * For more details about callbacks check the following sections from the documentation:
 *
 *   * @ref janus_callbacks
 *   * @ref janus_states
 *   * @ref janus_run_substates
 *   * @ref janus_state_transitions
 *   * @ref minos_state_transition_t
 *   * @ref janus_callback_t
 */
static janus_callback_t state_change_callbacks[MAX_JANUS_CALLBACKS + 1];


/**
 * @brief The number of currently registered Janus callbacks
 */
minos_handle_t no_of_janus_callbacks;


/**
 * @brief This function executes the state change.
 *
 * @details It calls all @janus callbacks first and informs @karajan about the state
 *   change by calling it's @ref karajan_leaves_janus_state function with the current
 *   state (that's about to be transitioned away from) as a parameter. \n
 *
 * After all callbacks are called, the current state and substate are set to the new
 *   ones as determined by @ref process_minos_states.
 */
void process_minos_state_change(void);


/**
 * @brief This function initializes @janus, the system state handler.
 *
 * @details It is called as soon as @ref minos() is executed, before entering the
 *   infinite loop, because @janus needs to be initialized as quickly as possible.
 *   Everything else that runs is dependent on the system states.
 */
void janus(void)
{
	minos_handle_t i;
	current_state = MINOS_HW_INIT;
	next_state    = MINOS_HW_INIT;

	for (i = 0; i < MINOS_CRITICAL_ERROR; i++)
		state_requests[i] = 0;
	state_requests[MINOS_SLEEP] = 1;

	for (i = 0; i <= MAX_JANUS_CALLBACKS; i++)
		state_change_callbacks[i] = MINOS_NULL;
	no_of_janus_callbacks = 0;
}


enum minos_state_t get_minos_state(void)
{
	return current_state;
}

minos_run_substate_t get_minos_run_substate(void)
{
	return current_substate;
}

void request_minos_state(enum minos_state_t rq_state)
{
	if ((rq_state < MINOS_CRITICAL_ERROR) && (state_requests[rq_state] < MAX_STATE_REQUESTS))
		state_requests[rq_state]++;
}

void release_minos_state(enum minos_state_t rq_state)
{
	if ((rq_state < MINOS_CRITICAL_ERROR) && (state_requests[rq_state] > 0))
		state_requests[rq_state]--;
}

minos_return_t request_minos_run_substate(minos_run_substate_t rq_substate)
{
        if (next_state == MINOS_RUN)
        {
        	substate_request = rq_substate;

        	return MINOS_SUCCESS;
        }

	return MINOS_ERROR;
}


void enter_os_critical_error_state(void)
{
	current_state = MINOS_CRITICAL_ERROR;
	while (5 < 7);
}


/**
 * @brief This function processes the system states and substates and it's execution is
 *   not contingent on anything.
 *
 * @details It is called on every loop from @ref minos(). \n
 *
 * Based on state and substate requests and allowed transitions, this function
 *   determines if a state and/or substate change is required and if yes, what is the
 *   next state and/or substate. \n \n
 *
 *
 * It uses the state requests stored in @ref state_requests to determine the next state.
 *   The next state of the system is stored in variable @ref next_state. \n
 *
 * See the @ref janus_startup section for more details about the state transitions
 *   that take place at startup. \n
 *
 * See the @ref janus_states section for more details about states. \n
 *
 * See the @ref janus_state_transitions section for more information on state
 *   transitions during normal operation. \n \n
 *
 * It uses the last substate request stored in @ref substate_request to determine the
 *   next substate. The next substate is stored in variable @ref next_substate. \n
 *
 * See the @ref janus_run_substates section for more information about substates. \n \n
 *
 *
 * This function also reads the @ref LOWEST_JANUS_STATE configuration to determine what
 *   is the lowest state @minos can go to. \n
 *
 * See the @ref janus_lowest_state section for more information about the
 *   @ref LOWEST_JANUS_STATE macro. \n \n
 *
 *
 * If a transition is to be made (@ref current_state != @ref next_state and/or
 *   @ref current_substate != @ref next_substate), it calls
 *   @ref process_minos_state_change, which in turn calls the Janus callbacks.
 *
 * @return @ref MINOS_PENDING if a state transition has been executed,
 *           otherwise @ref MINOS_SUCCESS.
 */
minos_return_t process_minos_states(void)
{
	minos_handle_t i;
	minos_defdata_t go_higher = 0;

	if (current_state < MINOS_USER_INIT)
	{
		go_higher = 1;
	}
	else if (current_state == MINOS_USER_INIT)
	{
		next_state = MINOS_RUN;
	}
	else
	{
		for (i = current_state + 1; i < MINOS_CRITICAL_ERROR; i++)
			if (state_requests[i] != 0)
				go_higher = 1;
	}

	if (go_higher != 0)
	{
		next_state = current_state + 1;
	}
	else if ((state_requests[current_state] == 0)    &&
	         (current_state > LOWEST_JANUS_STATE)    &&
	         (current_state > MINOS_SLEEP))
	{
		next_state = current_state - 1;
	}

	if (next_state == MINOS_RUN)
	{
		next_substate = substate_request;
	}
	else
	{
		next_substate = 0;
		substate_request   = 0;
	}

	if ((current_state != next_state) || (current_substate != next_substate))
	{
		process_minos_state_change();

		if (current_state > MINOS_USER_INIT)
			return MINOS_PENDING;
	}

	return MINOS_SUCCESS;
}

void process_minos_state_change(void)
{
	minos_handle_t i;
	struct minos_state_transition_t transition;

	transition.current_state = current_state;
	transition.next_state = next_state;
	transition.current_substate = current_substate;
	transition.next_substate = next_substate;

	for (i = 0; state_change_callbacks[i] != MINOS_NULL; i++)
		state_change_callbacks[i](transition);

	karajan_leaves_janus_state(current_state);

	current_state = next_state;
	current_substate = next_substate;
}

minos_return_t set_janus_callback(janus_callback_t cb)
{
	if (no_of_janus_callbacks < MAX_JANUS_CALLBACKS)
	{
		state_change_callbacks[no_of_janus_callbacks] = cb;
		no_of_janus_callbacks++;
		return MINOS_SUCCESS;
	}

	return MINOS_ERROR;
}




/**
 * @file
 *
 * @janus is the @minos component that implements the system state machine. It implements
 *   the state machine that determines the current system state. It handles all
 *   system state transitions and it offers a feature that allows software components to
 *   register callbacks with it. @janus will call the callbacks when a state transition
 *   is about to occur.
 *
 *
 *
 * \n \n @section janus_states State Diagram and Description of States
 *
 * @dot
 *   digraph system_state_machine
 *   {
 *     compound=true;
 *     node [shape=Mrecord style="filled, bold" fontname="times bold"];
 *     rankdir="LR";
 *
 *     init [shape=point height=0.2 width=0.2];
 *
 *     subgraph cluster_init_states {
 *       fontsize=25;
 *       fontname="times bold";
 *       label = "Initialization States";
 *       color=lightblue;
 *       node [fillcolor=lightblue];
 *
 *       MINOS_HW_INIT  -> MINOS_OS_INIT;
 *       MINOS_OS_INIT  -> MINOS_USER_INIT;
 *     }
 *     init   -> MINOS_HW_INIT;
 *
 *     subgraph cluster_normal_operation_states {
 *       fontsize=25;
 *       fontname="times bold";
 *       label = "Normal Operation States"; labeljust="l";
 *       color=green;
 *       node [fillcolor=green];
 *
 *       MINOS_SLEEP    -> MINOS_STANDBY;
 *       MINOS_STANDBY  -> MINOS_SLEEP;
 *       MINOS_STANDBY  -> MINOS_RUN;
 *       MINOS_RUN      -> MINOS_STANDBY;
 *     }
 *
 *     MINOS_USER_INIT  -> MINOS_RUN;
 *
 *   }
 * @enddot
 *
 * @dot
 *   digraph system_state_machine_fatal_error
 *   {
 *     compound=true;
 *     node [shape=Mrecord style="filled, bold" fontname="times bold"];
 *     rankdir="LR";
 *
 *     subgraph cluster_fatal_error_state {
 *       fontsize=25;
 *       fontname="times bold";
 *       label = "Fatal Error State";
 *       color=red;
 *
 *       MINOS_CRITICAL_ERROR [style=filled fillcolor=red];
 *       node[style="filled"];
 *       "Any MinOS State"  -> MINOS_CRITICAL_ERROR [label="Fatal Error"];
 *     }
 *   }
 * @enddot
 *
 * And below you have a description for each state:
 *
 * @anchor states_table
 * <table>
 *   <caption id="state_table">States Table</caption>
 *
 *   <tr><th>State                   <th>Description
 *   <tr><td style="color:cornflowerblue">   <b> (0) MINOS_HW_INIT </b>
 *                                           <td> @copydoc MINOS_HW_INIT
 *
 *   <tr><td style="color:cornflowerblue">   <b> (1) MINOS_OS_INIT </b>
 *                                           <td> @copydoc MINOS_OS_INIT
 *
 *   <tr><td style="color:cornflowerblue">   <b> (2) MINOS_USER_INIT </b>
 *                                           <td> @copydoc MINOS_USER_INIT
 *
 *   <tr><td style="color:green">            <b> (3) MINOS_SLEEP </b>
 *                                           <td> @copydoc MINOS_SLEEP
 *
 *   <tr><td style="color:green">            <b> (4) MINOS_STANDBY </b>
 *                                           <td> @copydoc MINOS_STANDBY
 *
 *   <tr><td style="color:green">            <b> (5) MINOS_RUN </b>
 *                                           <td> @copydoc MINOS_RUN
 *
 *   <tr><td style="color:red">              <b> (6) MINOS_CRITICAL_ERROR </b>
 *                                         <td> @copydoc MINOS_CRITICAL_ERROR
 * </table>
 *
 *
 * The function that determines the next state transition is @ref process_minos_states,
 *   which is called every loop from @ref minos(). \n
 *
 *
 *
 * \n \n @section janus_startup Startup
 *
 * At startup @minos is in state @ref MINOS_HW_INIT. When I say "startup", I mean as soon
 *   as function @ref minos() is called. There are three predetermined state transitions
 *   that happen immediately after startup: \n \n
 *
 *
 * @ref MINOS_HW_INIT <c><b>-></b></c>
 * @ref MINOS_OS_INIT <c><b>-></b></c>
 * @ref MINOS_USER_INIT <c><b>-></b></c>
 * @ref MINOS_RUN. \n \n
 *
 *
 * This happens independently of the system configuration. Function
 *   @ref process_minos_states is called on every loop of the infinite loop implemented
 *   in @ref minos() and swithces to the next state in the chain above. \n
 *
 * Here is an illustration:
 *
 * @msc "State transitions at startup"
 *
 *   width = "1050";
 *
 *   power [label=""],
 *   main [label=""],
 *   minos [label=""],
 *   janus [label=""],
 *   minos_cmp [label=""],
 *   usr_cmp [label=""];
 *
 *   power box power [label="Power"],
 *   main  box main  [label="main.c"],
 *   minos box minos [label="minos.c", url="@ref minos.c"],
 *   janus box janus [label="janus.c", url="@ref janus.c"],
 *   minos_cmp box minos_cmp [label="\nOther MinOS components\n"],
 *   usr_cmp box usr_cmp [label="user.c"];
 *
 *   power => main [label="main()"];
 *
 *   main => minos [label="minos()",
 *                     url           = "@ref minos()"];
 *
 *   minos => janus [label           = "janus()",
 *                     url           = "@ref janus()"];
 *
 *   janus note janus [label         = "\nSet state MINOS_HW_INIT\n",
 *                     url           = "@ref MINOS_HW_INIT",
 *                     textbgcolour  = "#c0c0c0"];
 *
 *   janus >> minos;
 *
 *
 *   minos note minos [label         = "Infinite loop starts",
 *                     textbgcolour  = "#c0c0c0"];
 *
 *
 *   minos => usr_cmp [label         = "MINOS_HW_INIT_FUNCTION",
 *                     url           = "@ref MINOS_HW_INIT_FUNCTION"];
 *   usr_cmp >> minos;
 *
 *
 *   minos => janus [label = "process_minos_states()",
 *                   url   = "@ref process_minos_states"];
 *
 *   janus note janus [label         = "\nSet state MINOS_OS_INIT\n",
 *                     url           = "@ref MINOS_OS_INIT",
 *                     textbgcolour  = "#c0c0c0"];
 *
 *   janus >> minos;
 *
 *
 *   minos => minos_cmp [label = "Call init functions",
 *                       url   = "@ref minos_init_vector"];
 *
 *   minos_cmp >> minos;
 *
 *
 *   minos => janus [label = "process_minos_states()",
 *                   url   = "@ref process_minos_states"];
 *
 *   janus note janus [label         = "\nSet state MINOS_USER_INIT\n",
 *                     url           = "@ref MINOS_USER_INIT",
 *                     textbgcolour  = "#c0c0c0"];
 *
 *   janus >> minos;
 *
 *
 *   minos => usr_cmp [label = "Call init functions",
 *                     url   = "@ref user_init_vector"];
 *
 *   usr_cmp >> minos;
 *
 *
 *   minos => janus [label = "process_minos_states()",
 *                   url   = "@ref process_minos_states"];
 *
 *   janus note janus [label         = "\nSet state MINOS_RUN\n",
 *                     url           = "@ref MINOS_RUN",
 *                     textbgcolour  = "#c0c0c0"];
 *
 *   janus >> minos;
 *
 *
 *   |||;
 *
 * @endmsc
 *
 *
 *
 * \n \n @section janus_state_transitions State Transitions
 *
 * In the previous section we had a description of the initialization phase of @minos.
 *   From this point on the state transitions are determined by two factors:
 *   * The configuration set in @ref LOWEST_JANUS_STATE
 *   * The state requests the user components make during the @ref MINOS_USER_INIT
 *     state, when their respective initialization functions are called.
 *
 * From this moment on, @minos can be in one of the following three states:
 *   * @ref MINOS_RUN (highest state)
 *   * @ref MINOS_STANDBY
 *   * @ref MINOS_SLEEP (lowest state)
 *
 * If your task needs to know the state of the system, you can call @ref get_minos_state
 *   at any point. \n \n
 *
 *
 * It is worth emphasizing that @janus will not transition to a state lower than the
 *   configured @ref LOWEST_JANUS_STATE, regardless of the current state requests. If
 *   you don't plan to use any system states and/or you are just interested in writing
 *   some code on a microcontroller and run that code from the moment you connect the
 *   power supply to it until you disconnect it, just assign @ref MINOS_RUN to
 *   @ref LOWEST_JANUS_STATE. That will ensure that @minos will be kept in the
 *   @ref MINOS_RUN state after the pre-programmed transitions above. That will keep the
 *   scheduler (@karajan) running and you don't have to worry about states. Your system
 *   will be always on. \n \n
 *
 *
 * Talking about state requests, @janus offers a pair of functions to request and release
 *   states: @ref request_minos_state and @ref release_minos_state. Following your
 *   request, as soon as function @ref process_minos_states is called from @ref minos(),
 *   @janus will evaluate if a transition is required or not. \n
 *
 * The mechanism for requests and releases is simple. @janus will go to the highest
 *   requested state. For example, <c><b>Component_A</b></c> requests state
 *   \ref MINOS_RUN and <c><b>Component_B</b></c> requests state @ref MINOS_STANDBY.
 *   @janus will see as a target state @ref MINOS_RUN, because it's the higher state
 *   (@ref MINOS_RUN <c><b>= 5</b></c>, @ref MINOS_STANDBY <c><b>= 4</b></c>). If
 *   <c><b>Component_A</b></c> releases the @ref MINOS_RUN state, the system will go
 *   in the @ref MINOS_STANDBY state. \n \n
 *
 *
 * For the next diagram, let's assume that @ref LOWEST_JANUS_STATE is set to
 *   @ref MINOS_SLEEP :
 *
 * @anchor normal_operation_transitions
 * @msc "State transitions during normal operation"
 *
 *   width = "1050";
 *
 *   component_a [label=""],
 *   component_b [label=""],
 *   janus [label=""];
 *
 *   component_a  box component_a  [label="Component_A"],
 *   component_b  box component_b  [label="Component_B"],
 *   janus box janus [label="Janus", url="@ref janus.c"];
 *
 *   |||;
 *
 *   component_a => janus [label = "request_minos_state(MINOS_RUN)",
 *                         url   = "@ref request_minos_state"];
 *
 *   janus note janus [label         = "requests for MINOS_RUN = 1 \n
 *                                      requests for MINOS_STANDBY = 0 \n
 *                                      requests for MINOS_SLEEP = 0",
 *                     textbgcolour  = "#c0c0c0"];
 *
 *   janus >> component_a;
 *
 *   ...;
 *
 *   janus note janus [label         = "Set state MINOS_RUN",
 *                     textbgcolour  = "#c0c0c0"];
 *
 *   ...;
 *
 *   component_b => janus [label = "request_minos_state(MINOS_STANDBY)",
 *                         url   = "@ref request_minos_state"];
 *
 *   janus note janus [label         = "\n
 *                                      requests for MINOS_RUN = 1 \n
 *                                      requests for MINOS_STANDBY = 1 \n
 *                                      requests for MINOS_SLEEP = 0\n",
 *                     textbgcolour  = "#c0c0c0"];
 *
 *   janus >> component_b;
 *
 *   ...;
 *
 *   janus note janus [label         = "Stay in state MINOS_RUN",
 *                     textbgcolour  = "#c0c0c0"];
 *
 *   ...;
 *
 *   component_a => janus [label = "release_minos_state(MINOS_RUN)",
 *                         url   = "@ref release_minos_state"];
 *
 *   janus note janus [label         = "\n
 *                                      requests for MINOS_RUN = 0 \n
 *                                      requests for MINOS_STANDBY = 1 \n
 *                                      requests for MINOS_SLEEP = 0\n",
 *                     textbgcolour  = "#c0c0c0"];
 *
 *   janus >> component_a;
 *
 *   ...;
 *
 *   janus note janus [label         = "Set state MINOS_STANDBY",
 *                     textbgcolour  = "#c0c0c0"];
 *
 *   ...;
 *
 *   component_b => janus [label = "release_minos_state(MINOS_STANDBY)",
 *                         url   = "@ref release_minos_state"];
 *
 *   janus note janus [label         = "\n
 *                                      requests for MINOS_RUN = 0 \n
 *                                      requests for MINOS_STANDBY = 0 \n
 *                                      requests for MINOS_SLEEP = 0\n",
 *                     textbgcolour  = "#c0c0c0"];
 *
 *   janus >> component_b;
 *
 *   ...;
 *
 *   janus note janus [label         = "Set state MINOS_SLEEP",
 *                     textbgcolour  = "#c0c0c0"];
 *
 *
 *   |||;
 *
 * @endmsc
 *
 * It's important to know that state transitions don't happen immediately. The
 *   @ref process_minos_states function is called from the loop in @ref minos() for the
 *   state requests to be checked and, if necessary, a state transition to be made. \n
 *
 * It's also important to note that the legal state transitions are the ones illustrated
 *   in @ref states_table "States table". For example, @janus will never transition from
 *   @ref MINOS_RUN to @ref MINOS_SLEEP without going through
 *   @ref MINOS_STANDBY first:
 *
 * @anchor run_to_sleep_transition
 * @msc "State transitions from MINOS_RUN to MINOS_SLEEP"
 *
 *   width = "1050";
 *
 *   component_a [label=""],
 *   janus [label=""];
 *
 *   component_a  box component_a  [label="Component_A"],
 *   janus box janus [label="Janus", url="@ref janus.c"];
 *
 *   |||;
 *
 *   component_a => janus [label = "request_minos_state(MINOS_RUN)",
 *                         url   = "@ref request_minos_state"];
 *
 *   janus note janus [label         = "\n
 *                                      requests for MINOS_RUN = 1 \n
 *                                      requests for MINOS_STANDBY = 0 \n
 *                                      requests for MINOS_SLEEP = 0\n",
 *                     textbgcolour  = "#c0c0c0"];
 *
 *   janus >> component_a;
 *
 *   ...;
 *
 *   janus note janus [label         = "Set state MINOS_RUN",
 *                     textbgcolour  = "#c0c0c0"];
 *
 *   ...;
 *
 *   component_a => janus [label = "release_minos_state(MINOS_RUN)",
 *                         url   = "@ref release_minos_state"];
 *
 *   janus note janus [label         = "\n
 *                                      requests for MINOS_RUN = 0 \n
 *                                      requests for MINOS_STANDBY = 0 \n
 *                                      requests for MINOS_SLEEP = 0\n",
 *                     textbgcolour  = "#c0c0c0"];
 *
 *   janus >> component_a;
 *
 *   ...;
 *
 *   janus note janus [label         = "Set state MINOS_STANDBY",
 *                     textbgcolour  = "#c0c0c0"];
 *
 *   ...;
 *
 *   janus note janus [label         = "Set state MINOS_SLEEP",
 *                     textbgcolour  = "#c0c0c0"];
 *
 *
 *   |||;
 *
 * @endmsc
 *
 *
 *
 * \n \n @section janus_lowest_state LOWEST_JANUS_STATE Example
 *
 * Now let's see what happens to the above diagram if @ref LOWEST_JANUS_STATE is set to
 *   @ref MINOS_STANDBY (spoiler: the lowest state will be @ref MINOS_STANDBY, the
 *   system will never transition to @ref MINOS_SLEEP):
 *
 * @anchor run_to_standby_transition_lowest_state
 * @msc "State transitions from MINOS_RUN to LOWEST_JANUS_STATE = MINOS_STANDBY"
 *
 *   width = "1050";
 *
 *   component_a [label=""],
 *   janus [label=""];
 *
 *   component_a  box component_a  [label="Component_A"],
 *   janus box janus [label="Janus", url="@ref janus.c"];
 *
 *   |||;
 *
 *   component_a => janus [label = "request_minos_state(MINOS_RUN)",
 *                         url   = "@ref request_minos_state"];
 *
 *   janus note janus [label         = "\n
 *                                      requests for MINOS_RUN = 1 \n
 *                                      requests for MINOS_STANDBY = 0 \n
 *                                      requests for MINOS_SLEEP = 0\n",
 *                     textbgcolour  = "#c0c0c0"];
 *
 *   janus >> component_a;
 *
 *   ...;
 *
 *   janus note janus [label         = "Set state MINOS_RUN",
 *                     textbgcolour  = "#c0c0c0"];
 *
 *   ...;
 *
 *   component_a => janus [label = "release_minos_state(MINOS_RUN)",
 *                         url   = "@ref release_minos_state"];
 *
 *   janus note janus [label         = "\n
 *                                      requests for MINOS_RUN = 0 \n
 *                                      requests for MINOS_STANDBY = 0 \n
 *                                      requests for MINOS_SLEEP = 0\n",
 *                     textbgcolour  = "#c0c0c0"];
 *
 *   janus >> component_a;
 *
 *   ...;
 *
 *   janus note janus [label         = "Set state MINOS_STANDBY",
 *                     textbgcolour  = "#c0c0c0"];
 *
 *
 *   |||;
 *
 * @endmsc
 *
 * The hardcoded lowest state @janus can go to after the initialization phase is
 *   @ref MINOS_SLEEP. Even if you set @ref LOWEST_JANUS_STATE to @ref MINOS_USER_INIT,
 *   @minos will never reach that state. It will remain in @ref MINOS_SLEEP, even if
 *   there is no request for it.
 *
 *
 *
 * \n \n @section janus_state_request_number The Number of State Requests
 *
 * One more thing about requests and releases. As long as there is at least one active
 *   request, @janus will consider that state to be requested and it will transition to
 *   it (if it's the highest requested state). That means there is a counter of how many
 *   requests there are for a state and as long as that counter is greater than 0, the
 *   state is considered to be requested. For example, let's say that both
 *   <c><b>Component_A</b></c> and <c><b>Component_B</b></c> request both
 *   @ref MINOS_STANDBY and @ref MINOS_RUN states. As a result @ref MINOS_STANDBY will
 *   have <c><b>2</b></c> requests and @ref MINOS_RUN will also have <c><b>2</b></c>
 *   requests. @janus would go to the @ref MINOS_RUN state. if
 *   <c><b>Component_B</b></c> will release it's request for state @ref MINOS_RUN,
 *   the new number of requests would be <c><b>1</b></c> for @ref MINOS_RUN and
 *   <c><b>2</b></c> for @ref MINOS_STANDBY. But the system won't go to
 *   @ref MINOS_STANDBY just because it has a higher number of requests. It will stay
 *   in @ref MINOS_RUN,  because at least one component requested this state and it's
 *   the highest requested state:
 *
 * @anchor normal_operation_transitions_number_of_requests
 * @msc "State transitions during normal operation, number of requests"
 *
 *   width = "1050";
 *
 *   component_a [label=""],
 *   component_b [label=""],
 *   janus [label=""];
 *
 *   component_a  box component_a  [label="Component_A"],
 *   component_b  box component_b  [label="Component_B"],
 *   janus box janus [label="Janus", url="@ref janus.c"];
 *
 *   |||;
 *
 *   component_a => janus [label = "request_minos_state(MINOS_RUN)",
 *                         url   = "@ref request_minos_state"];
 *
 *   ...;
 *
 *   component_a => janus [label = "request_minos_state(MINOS_STANDBY)",
 *                         url   = "@ref request_minos_state"];
 *
 *   ...;
 *
 *   component_b => janus [label = "request_minos_state(MINOS_RUN)",
 *                         url   = "@ref request_minos_state"];
 *
 *   ...;
 *
 *   component_b => janus [label = "request_minos_state(MINOS_STANDBY)",
 *                         url   = "@ref request_minos_state"];
 *
 *   ...;
 *
 *   janus note janus [label         = "\n
 *                                      requests for MINOS_RUN = 2 \n
 *                                      requests for MINOS_STANDBY = 2 \n
 *                                      requests for MINOS_SLEEP = 0\n",
 *                     textbgcolour  = "#c0c0c0"];
 *
 *   ...;
 *
 *   janus note janus [label         = "Set state MINOS_RUN",
 *                     textbgcolour  = "#c0c0c0"];
 *
 *   ...;
 *
 *   component_a => janus [label = "release_minos_state(MINOS_RUN)",
 *                         url   = "@ref release_minos_state"];
 *
 *   janus note janus [label         = "\n
 *                                      requests for MINOS_RUN = 1 \n
 *                                      requests for MINOS_STANDBY = 2 \n
 *                                      requests for MINOS_SLEEP = 0\n",
 *                     textbgcolour  = "#c0c0c0"];
 *
 *   ...;
 *   janus note janus [label         = "Stay in state MINOS_RUN",
 *                     textbgcolour  = "#c0c0c0"];
 *
 *   |||;
 *
 * @endmsc
 *
 * The maximum number of times you can request a state is hard coded by the
 *   @ref MAX_STATE_REQUESTS macro.
 *
 *
 *
 * \n \n @section janus_critical_error_state MINOS_CRITICAL_ERROR State
 *
 * Nothing has been said about the @ref MINOS_CRITICAL_ERROR state so far. That's
 *   because it is a very special state. It can't be set using @ref request_minos_state,
 *   it has a dedicated API function: @ref enter_os_critical_error_state which is
 *   implemented as an infinite loop that will never return. It is up to you to activate
 *   a watchdog timer that would reset the system in case this state is entered or to use
 *   a manual reset. @minos will not transition to this state in the current
 *   implementation, it is up to you to use it if it is required by your application.
 *   Please note that no operations like cleanup or deinitialization can be done after
 *   you call @ref enter_os_critical_error_state. A call to this function will simply
 *   lock the system and wait for reset. \n \n
 *
 *
 * For example, it's not a critical error if one of your components can't set a
 *   @janus callback. This error must be signaled somehow, but the system should continue
 *   its execution, maybe in a reduced functionality mode.
 *
 * But it is a critical error is a stack overflow, or buffer overflow. In this case the
 *   system can't pinpoint what data has been corrupted, therefore any component may be
 *   affected. \n
 *
 * A critical error is any error the system can't recover from. \n
 *
 *
 *
 * \n \n @section janus_run_substates Substates
 *
 * @minos is supposed to be a simple RTOS, so the number of states it implements is
 *   kept at an absolute minimum and I would rather remove states rather than add new
 *   ones. \n
 *
 * However, adding a limitation of the number of the states may be annoying at some
 *   point. This is why @janus supports substates. The substates are available only
 *   when the system is in state @ref MINOS_RUN and implementing the substates is
 *   entirely at your discretion, as is their handling. All of that has to be managed
 *   in your software components. \n \n
 *
 *
 * @janus handles substates in a very simplistic way. Any substate request
 *   made via @ref request_minos_run_substate is acknowledged and the next time
 *   @ref process_minos_states is called, the transition to the requested substate is
 *   done. \n \n
 *
 *
 * If your task needs to know the substate of the system, you can call
 *   @ref get_minos_run_substate at any time. This function behaves the same way as
 *   it's state counterpart, @ref get_minos_state. \n \n
 *
 *
 * There is no release function for substates, as there is no hierarchy implemented
 *   by @janus for them. @janus will transition to the new substate requested by
 *   @ref request_minos_run_substate, no questions asked. As far as @janus is concerned,
 *   all substate transitions are legal. It is up to you to implement a substates
 *   mechanism if you need one. @janus will only centralize state and substate
 *   behaviours. \n \n
 *
 *
 * I would like to emphasize that substates are available only in @ref MINOS_RUN state.
 *   Calling @ref request_minos_run_substate from any other state will cause it to
 *   return @ref MINOS_ERROR. Also, keep in mind that any transition away from
 *   @ref MINOS_RUN would reset the substate to <c><b>0</b></c> and it will
 *   <c><b>not</b></c> be restored when @ref MINOS_RUN state is requested again. You
 *   are allowed, however, to change the substate in the @janus callback that informs
 *   you that a transition to @ref MINOS_RUN is happening. \n
 *
 * As I said before, the implementation of substates is entirely at your discretion.
 *   Even the data type for substates can be configured by you, using the
 *   @ref minos_run_substate_t data type, which is defined as @ref minos_defdata_t by
 *   default. \n
 *
 * Here are some examples, a visual representation of the documentation about substates:
 *
 * @anchor janus_run_substates_diagram
 * @msc "Example transitions for Janus substates"
 *
 *   width = "1050";
 *
 *   minos [label=""],
 *   user_components [label=""],
 *   janus [label=""];
 *
 *   user_components  box user_components  [label="User Components"],
 *   minos box minos  [label="MinOS", url="@ref minos.c"],
 *   janus box janus [label="Janus", url="@ref janus.c"];
 *
 *   |||;
 *   ...;
 *   |||;
 *
 *   janus note janus [label         = "Set state MINOS_STANDBY",
 *                     textbgcolour  = "#c0c0c0"];
 *
 *   |||;
 *   ...;
 *   |||;
 *
 *   minos => user_components [label = "run task"];
 *
 *   user_components => janus [label = "request_minos_run_substate(10)",
 *                             url   = "@ref request_minos_run_substate"];
 *
 *   janus >> user_components [label = "MINOS_ERROR",
 *                             url   = "@ref MINOS_ERROR"];
 *
 *   |||;
 *
 *   user_components => janus [label = "request_minos_state(MINOS_RUN)",
 *                             url   = "@ref request_minos_state"];
 *
 *   janus >> user_components [label = "MINOS_SUCCESS",
 *                             url   = "@ref MINOS_SUCCESS"];
 *
 *   |||;
 *
 *   user_components => janus [label = "request_minos_run_substate(10)",
 *                             url   = "@ref request_minos_run_substate"];
 *
 *   janus >> user_components [label = "MINOS_ERROR",
 *                             url   = "@ref MINOS_ERROR"];
 *
 *   user_components >> minos [label = "task finishes execution"];
 *
 *   |||;
 *   ...;
 *   |||;
 *
 *   minos => janus [label = "process_minos_states()",
 *                   url   = "@ref process_minos_states"];
 *
 *   janus note janus [label         = "Set state MINOS_RUN",
 *                     textbgcolour  = "#c0c0c0"];
 *
 *   janus >> minos;
 *
 *   |||;
 *   ...;
 *   |||;
 *
 *   minos => user_components [label = "run task"];
 *
 *   user_components => janus [label = "request_minos_run_substate(10)",
 *                             url   = "@ref request_minos_run_substate"];
 *
 *   janus >> user_components [label = "MINOS_SUCCESS",
 *                             url   = "@ref MINOS_SUCCESS"];
 *
 *   user_components >> minos [label = "task finishes execution"];
 *
 *   |||;
 *   ...;
 *   |||;
 *
 *   minos => janus [label = "process_minos_states()",
 *                   url   = "@ref process_minos_states"];
 *
 *   janus note janus [label         = "Set substate 10",
 *                     textbgcolour  = "#c0c0c0"];
 *
 *   janus >> minos;
 *
 *   |||;
 *   ...;
 *   |||;
 *
 *   minos => user_components [label = "run task"];
 *
 *   user_components => janus [label = "release_minos_state(MINOS_RUN)",
 *                             url   = "@ref release_minos_state"];
 *
 *   janus >> user_components [label = "MINOS_SUCCESS",
 *                             url   = "@ref MINOS_SUCCESS"];
 *
 *   user_components >> minos [label = "task finishes execution"];
 *
 *   |||;
 *   ...;
 *   |||;
 *
 *   minos => janus [label = "process_minos_states()",
 *                   url   = "@ref process_minos_states"];
 *
 *   janus note janus [label         = "\nSet state MINOS_STANDBY \n
 *                                      Set substate to 0\n",
 *                     textbgcolour  = "#c0c0c0"];
 *
 *   janus >> minos;
 *
 *   |||;
 *   ...;
 *   |||;
 *
 *   minos => user_components [label = "run task"];
 *
 *   user_components => janus [label = "request_minos_state(MINOS_RUN)",
 *                             url   = "@ref request_minos_state"];
 *
 *   janus >> user_components [label = "MINOS_SUCCESS",
 *                             url   = "@ref MINOS_SUCCESS"];
 *
 *   user_components >> minos [label = "task finishes execution"];
 *
 *   |||;
 *   ...;
 *   |||;
 *
 *   minos => janus [label = "process_minos_states()",
 *                   url   = "@ref process_minos_states"];
 *
 *   janus note janus [label         = "Request for MINOS_RUN state acknowledged",
 *                     textbgcolour  = "#c0c0c0"];
 *
 *   janus note janus [label         = "\nSet state MINOS_RUN \n
 *                                      Substate is kept at 0\n",
 *                     textbgcolour  = "#c0c0c0"];
 *
 *   janus >> minos;
 *
 * @endmsc
 *
 *
 *
 * \n \n @section janus_callbacks Callbacks for State Transitions
 *
 * @janus offers a callback feature to inform any user SW components about a state or
 *   substate change. The callback function has to have a prototype as defined by
 *   @ref janus_callbacks. The callback can be registered using the
 *   @ref set_janus_callback function. I would recommend you register them in your
 *   component initialization function, but you can register it at any time. \n \n
 *
 *
 * @minos guarantees that no task is running while the callbacks functions are called,
 *   so you don't have to worry about critical sections. \n \n
 *
 *
 * Here is an example of how the callbacks work:
 *
 * @anchor janus_callback_example
 * @msc "Example of Janus callbacks"
 *
 *   width = "1050";
 *
 *   minos [label=""],
 *   user_components [label=""],
 *   janus [label=""];
 *
 *   user_components  box user_components  [label="User Components"],
 *   minos box minos  [label="MinOS", url="@ref minos.c"],
 *   janus box janus [label="Janus", url="@ref janus.c"];
 *
 *   |||;
 *   ...;
 *   |||;
 *
 *   janus note janus [label         = "Set state MINOS_STANDBY",
 *                     textbgcolour  = "#c0c0c0"];
 *
 *   |||;
 *   ...;
 *   |||;
 *
 *   minos => user_components [label = "run task"];
 *
 *   user_components => janus [label = "request_minos_state(MINOS_RUN)",
 *                         url   = "@ref request_minos_state"];
 *
 *   janus >> user_components;
 *
 *   user_components >> minos [label = "task finishes execution"];
 *
 *   |||;
 *   ...;
 *   |||;
 *
 *   minos => janus [label = "process_minos_states()",
 *                   url   = "@ref process_minos_states"];
 *
 *   janus note janus [label         = "Request for MINOS_RUN state acknowledged",
 *                     textbgcolour  = "#c0c0c0"];
 *
 *   janus => user_components [label = "Call all callbacks"];
 *   user_components >> janus;
 *
 *   janus note janus [label         = "Set state MINOS_RUN",
 *                     textbgcolour  = "#c0c0c0"];
 *
 *   janus >> minos;
 *
 *   |||;
 *   ...;
 *   |||;
 *
 *   minos => user_components [label = "run task"];
 *
 *   user_components => janus [label = "request_minos_run_substate(10)",
 *                         url   = "@ref request_minos_run_substate"];
 *
 *   janus >> user_components;
 *
 *   user_components >> minos [label = "task finishes execution"];
 *
 *   |||;
 *   ...;
 *   |||;
 *
 *   minos => janus [label = "process_minos_states()",
 *                   url   = "@ref process_minos_states"];
 *
 *   janus note janus [label         = "Request for substate 10 acknowledged",
 *                     textbgcolour  = "#c0c0c0"];
 *
 *   janus => user_components [label = "Call all callbacks"];
 *   user_components >> janus;
 *
 *   janus note janus [label         = "Set substate 10",
 *                     textbgcolour  = "#c0c0c0"];
 *
 *   janus >> minos;
 *
 *   |||;
 *   ...;
 *   |||;
 *
 * @endmsc
 *
 * As you can see in the diagram, the callbacks are called <b>before</b> the new state
 *   is reached. The members of the structure @ref minos_state_transition_t that
 *   inform the user SW components about a state transition are called
 *   <c><b>current_state</b></c> and <c><b>next_state</b></c>. The members of the
 *   structure @ref minos_state_transition_t that inform the user SW components about a
 *   substate transition are called <c><b>current_substate</b></c> and
 *   <c><b>next_substate</b></c>. If you call the @ref get_minos_state function in the
 *   callback, you will get the old state! \n
 *
 * However, the new state is set before @janus returns to @ref minos(), so the very next
 *   task that is executed will see the new state if it calls @ref get_minos_state. \n \n
 *
 *
 * But, even if the next state is not actually set until all the callbacks finish
 *   execution, when the next state is going to be @ref MINOS_RUN you are allowed to
 *   request a substate from the @janus callback. \n
 *
 * Consequently, you are not allowed to request a substate from the callback when the
 *   next state is <b>not</b> going to be @ref MINOS_RUN.
 *
 * @anchor janus_callback_substates_example
 * @msc "Example of Janus callbacks (2)"
 *
 *   width = "1050";
 *
 *   minos [label=""],
 *   user_components [label=""],
 *   janus [label=""];
 *
 *   user_components  box user_components  [label="User Components"],
 *   minos box minos  [label="MinOS", url="@ref minos.c"],
 *   janus box janus [label="Janus", url="@ref janus.c"];
 *
 *   |||;
 *   ...;
 *   |||;
 *
 *   janus note janus [label         = "Set state MINOS_STANDBY",
 *                     textbgcolour  = "#c0c0c0"];
 *
 *   |||;
 *   ...;
 *   |||;
 *
 *   minos => user_components [label = "run task"];
 *
 *   user_components => janus [label = "request_minos_state(MINOS_RUN)",
 *                         url   = "@ref request_minos_state"];
 *
 *   janus >> user_components;
 *
 *   user_components >> minos [label = "task finishes execution"];
 *
 *   |||;
 *   ...;
 *   |||;
 *
 *   minos => janus [label = "process_minos_states()",
 *                   url   = "@ref process_minos_states"];
 *
 *   janus note janus [label         = "Request for MINOS_RUN state acknowledged",
 *                     textbgcolour  = "#c0c0c0"];
 *
 *   janus => user_components [label = "Call all callbacks"];
 *
 *   user_components => janus [label = "request_minos_run_substate(10)",
 *                             url   = "@ref request_minos_run_substate"];
 *
 *   janus >> user_components [label = "MINOS_SUCCESS",
 *                             url   = "@ref MINOS_SUCCESS"];
 *
 *   user_components >> janus;
 *
 *   janus note janus [label         = "Set state MINOS_RUN",
 *                     textbgcolour  = "#c0c0c0"];
 *
 *   janus >> minos;
 *
 *   |||;
 *   ...;
 *   |||;
 *
 *   minos => janus [label = "process_minos_states()",
 *                   url   = "@ref process_minos_states"];
 *
 *   janus note janus [label         = "Request for substate 10 acknowledged",
 *                     textbgcolour  = "#c0c0c0"];
 *
 *   janus => user_components [label = "Call all callbacks"];
 *   user_components >> janus;
 *
 *   janus note janus [label         = "Set substate 10",
 *                     textbgcolour  = "#c0c0c0"];
 *
 *   janus >> minos;
 *
 *   |||;
 *   ...;
 *   |||;
 *
 *   minos => janus [label = "process_minos_states()",
 *                   url   = "@ref process_minos_states"];
 *
 *   janus note janus [label         = "Request for substate 10 acknowledged",
 *                     textbgcolour  = "#c0c0c0"];
 *
 *   janus => user_components [label = "Call all callbacks"];
 *   user_components >> janus;
 *
 *   janus note janus [label         = "Set substate 10",
 *                     textbgcolour  = "#c0c0c0"];
 *
 *   janus >> minos;
 *
 *   |||;
 *   ...;
 *   |||;
 *
 *   minos => user_components [label = "run task"];
 *
 *   user_components => janus [label = "release_minos_state(MINOS_RUN)",
 *                             url   = "@ref release_minos_state"];
 *
 *   janus >> user_components [label = "MINOS_SUCCESS",
 *                             url   = "@ref MINOS_SUCCESS"];
 *
 *   user_components >> minos [label = "task finishes execution"];
 *
 *   |||;
 *   ...;
 *   |||;
 *
 *   minos => janus [label = "process_minos_states()",
 *                   url   = "@ref process_minos_states"];
 *
 *   janus note janus [label         = "\nRelease for MINOS_RUN state acknowledged\n
 *                                      Preparing for MINOS_STANDBY state\n",
 *                     textbgcolour  = "#c0c0c0"];
 *
 *   janus => user_components [label = "Call all callbacks"];
 *
 *   user_components => janus [label = "request_minos_run_substate(20)",
 *                             url   = "@ref request_minos_run_substate"];
 *
 *   janus >> user_components [label = "MINOS_ERROR",
 *                             url   = "@ref MINOS_ERROR"];
 *
 *   user_components >> janus;
 *
 *   janus note janus [label         = "\nSet state MINOS_STANDBY \n
 *                                      Set substate to 0\n",
 *                     textbgcolour  = "#c0c0c0"];
 *
 *   janus >> minos;
 *
 *   |||;
 *   ...;
 *   |||;
 *
 * @endmsc
 *
 *
 *
 * \n \n @section janus_the_end The End
 *
 * This concludes the documentation for @janus. \n
 *
 * Now it's time for a bit of trivia. \n
 *
 * This name comes from Roman Mythology. Janus was "the god of beginnings, gates,
 *   transitions, time, duality, doorways, passages, and endings". it is often depicted
 *   as having two faces, one that looks in the past and one that looks to the future.
 *
 * @image html "wikipedia_janus.jpeg"
 *
 * It's the perfect metaphor for a component that handles states and transitions. \n
 *
 * You can find more about this mythological being on the
 *   <a href="https://en.wikipedia.org/wiki/Janus">Wikipedia page for Janus</a>.
 *
 *
 */
