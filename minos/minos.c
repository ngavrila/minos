/** @cond license
 * MinOS
 * Copyright (C) 2020 Nicu Gavrila (https://www.linkedin.com/in/nicu-gavrila/)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 * @endcond
 */


/**
 * @brief A compiler switch for @ref user_init_vector .
 *
 * @details In file @ref minos_integration.h the @ref user_init_vector is defined. If
 *   we allow that definition to remain unguarded it will generate compiler warnings
 *   about it not being used in the other files that include the @ref minos_integration.h
 *   header. \n
 *
 * This macro is defined only while we include @ref minos_integration.h in this file
 *   and then we undefine it. @ref user_init_vector only has meaning for this file,
 *   hence this trick.
 */
#define USER_INIT_VECTOR_DEF
#include "minos_internal.h"
#undef USER_INIT_VECTOR_DEF


/**
 * @brief A list of functions that have to be ran first thing after entering the
 *   @ref minos() function and before the big switch.
 *
 * @details These functions should be exclusively @minos component initialization
 *   functions.
 */
static void (*minos_preinit_vector[])(void) =
{
	janus,
        MINOS_NULL
};


/**
 * @brief A list of initialization functions of each @minos component, except the ones
 *   that are listed in @ref minos_preinit_vector.
 */
static void (*minos_init_vector[])(void) =
{
	karajan,
        chronos,
        MINOS_NULL
};


void minos()
{
	minos_defdata_t i;

	for (i = 0; minos_preinit_vector[i] != MINOS_NULL; i++)
		minos_preinit_vector[i]();


	while (5 < 7)
	{
		switch (get_minos_state())
		{
			case MINOS_HW_INIT:
				MINOS_HW_INIT_FUNCTION();

				break;

			case MINOS_OS_INIT:
				for (i = 0; minos_init_vector[i] != MINOS_NULL; i++)
					minos_init_vector[i]();

				break;

			case MINOS_USER_INIT:
				for (i = 0; user_init_vector[i] != MINOS_NULL; i++)
					user_init_vector[i]();

				break;

			case MINOS_SLEEP:
				MINOS_SLEEP_FUNCTION();

				break;

			case MINOS_STANDBY:
				if (karajan_schedule() == MINOS_NO)
					MINOS_HALT_INSTRUCTION();

				break;

			case MINOS_RUN:
				if (karajan_schedule() == MINOS_NO)
					MINOS_HALT_INSTRUCTION();

				break;

			case MINOS_CRITICAL_ERROR:
				break;

			default:
				break;
		}

		if (MINOS_START_TIME == minos_empty_function)
			chronos_time_interrupt(1);

		while (process_minos_states() == MINOS_PENDING);
	}
}

/**
 * @file
 *
 *
 *
 * \n \n @section minos_introduction Introduction
 *
 * @minos is a Real Time Operating System designed with speed and ease of
 *   use in mind. @minos offeres a scheduler that supports non-preemptive
 *   tasks, provides a simple mechanism to handle system states and offers a system wide
 *   time counter. \n
 *
 * If you don't need a preemptive system, you just need a fast RTOS that can execute
 *   tasks and provide you with a simple way of handling system states, this is the
 *   one. \n
 *
 * @minos includes only the most common features used in an embedded
 *   system and I did my best to keep fancy stuff out. This RTOS is meant to be simple,
 *   efficient and easy to use (short list of API functions and short
 *   documentation). \n \n
 *
 *
 *
 * \n \n @section minos_components Components
 *
 * As you probably noticed already, instead of technical names, the MinOS components are
 *   named after some human or mythical being. I find this handier than using strange
 *   terms and acronyms. \n
 *
 * There aren't many components in MinOS. That's because it is a small OS and implements
 *   only a non-preemptive scheduler. All components implemented are here only because
 *   are needed in order to have this scheduler and to offer the most commonly used
 *   features in an embedded system. \n \n
 *
 * Here is a list with the components of @minos and their role:
 *
 * <table>
 *   <caption>The components of MinOS</caption>
 *
 *   <tr><th> Component  <th> Description
 *   <tr><td> Chronos    <td> Component that implements the system time counter \n \n
 *                              Its name comes from the
 *                              <a href="https://en.wikipedia.org/wiki/Chronos">
 *                              personification of time</a>.
 *   <tr><td> Janus      <td> This is the state machine implemented in @minos. It handles
 *                              the system states and substates. \n \n
 *                              It is named after the
 *                              <a href="https://en.wikipedia.org/wiki/Janus">
 *                              Roman god of beginnings, gates, transitions, time,
 *                              duality, doorways, passages, and endings</a>
 *                              (a bit of a mouthful).
 *   <tr><td> Karajan    <td> The task scheduler of @minos. \n \n
 *                              It's named after a famous 20<sup>th</sup> century
 *                              <a href="https://en.wikipedia.org/wiki/Herbert_von_Karajan">
 *                              Austrian conductor</a>.
 * </table>
 *
 *
 * Here is a nice and simple diagram of @minos:
 *
 *
 * @dot
 *   digraph minos_system_diagram
 *   {
 *     compound=true;
 *     node [shape=Mrecord style="filled, bold" fontname="times bold"];
 *     rankdir="LR";
 *
 *     subgraph cluster_minos {
 *       fontsize=25;
 *       fontname="times bold";
 *       label = "MinOS";
 *       color=blue;
 *
 *       Chronos;
 *       Janus;
 *       Karajan;
 *     }
 *   }
 * @enddot
 *
 *
 *
 * \n \n @section minos_normal_operation_loops What does MinOS do?
 *
 * The @ref minos.c file itself contains, for the most part, just a <c><b>switch</b></c>
 *   statement in an infinite loop. It initializes @janus, which implements all the logic
 *   required by the system state machine, and then it simply checks the system state as
 *   it is reported by @janus and acts according to it every loop. \n
 *
 * So, before entering the infinite loop it runs the initialization function for @janus.
 *   That ensures that the system is in a known state and starting executing code
 *   dependent (both in flow control and logic) makes sense. \n
 *
 * After @janus is initialized, the infinite loop is started and the <c><b>switch</b></c>
 *   statement is executed once every loop. Here is what it does for each state:
 *
 *   * MINOS_HW_INIT
 *
 *     * As a state created to initialize and parts of the MCU hardware that may
 *       require initialization, @ref MINOS_HW_INIT_FUNCTION is called.
 *
 *   * MINOS_OS_INIT
 *
 *     * All MinOS components are initialized here (except @janus, which was initialized
 *       before).
 *
 *   * MINOS_USER_INIT
 *
 *     * In this state the functions listed in @ref user_init_vector are called. They
 *       should initialize your software components and get them ready for the
 *       system to enter the normal operation.
 *
 *   * MINOS_SLEEP
 *
 *     * The MCU is supposed to be in sleep mode, which is a low power state. All MinOS
 *       does here is to call @ref MINOS_SLEEP_FUNCTION and wait for that function to
 *       request another system state from @janus.
 *
 *   * MINOS_STANDBY
 *
 *     * In this state @minos is fully functioning. The scheduler is called normally
 *       and everything operates as implemented. The handling of this state is
 *       identical to @ref MINOS_RUN, but it may be usefull for you if you want some
 *       sort of reduced functionality state.
 *
 *   * MINOS_RUN
 *
 *     * In this state the OS@minos operates as implemented. The handling of this state is
 *       identical to @ref MINOS_STANDBY, but this is the state where your system should
 *       operate at full functionality. In this state you also have the sub-state
 *       feature available, but more about that in the documentation for @janus.
 *
 *   * MINOS_CRITICAL_ERROR
 *
 *     * Nothing is executed here, it would be useless anyway. As soon as
 *       @janus sets this state, it will also lock the system in an
 *       infinite loop (yes, another one, but this one does absolutely nothing)
 *       implemented in function @ref enter_os_critical_error_state. The expectation
 *       is that you configured a watchdog timer and that the system will be reset by
 *       it. And maybe things will work better after that...
 *
 *
 * And that's about it with the <c><b>switch</b></c>. \n
 *
 * The only two functions that are called on every loop, regardless of the system
 *   state, are:
 *
 *   * @ref process_minos_states
 *
 *   * @ref chronos_time_interrupt with parameter <c><b>1</b></c>, if you
 *     assign @ref minos_empty_function to the @ref MINOS_START_TIME macro.
 *
 *
 *
 * \n \n @section minos_component_interactions Interactions Between MinOS Components
 *
 * Let's have a look at how the components of @minos interact with eachother. The
 *   diagram below describes the flow during normal operation and it focuses on @minos
 *   components only, no other interaction is shown.
 *
 * @msc "Normal MinOS operation"
 *
 *   width = "1050";
 *
 *   minos [label=""],
 *   janus [label=""],
 *   chronos [label=""],
 *   karajan [label=""];
 *
 *   minos    box minos    [label="minos()", url="@ref minos()"],
 *   chronos  box chronos  [label="Chronos", url="@ref chronos.c"],
 *   janus    box janus    [label="Janus"  , url="@ref janus.c"],
 *   karajan  box karajan  [label="Karajan", url="@ref karajan.c"];
 *
 *   |||;
 *
 *   minos note minos   [label = "\nIf current state is\n
 *                                MINOS_STANDBY\n or \n
 *                                MINOS_RUN\n",
 *                       textbgcolour  = "#c0c0c0"];
 *
 *     minos => karajan   [label = "karajan_schedule()",
 *                         url   = "@ref karajan_schedule"];
 *
 *       karajan => chronos [label = "get_minos_time()",
 *                           url   = "@ref get_minos_time"];
 *       chronos >> karajan;
 *
 *       karajan => karajan [label = "Scheduling activities"];
 *
 *     karajan >> minos;
 *
 *   minos note minos   [label = "End if",
 *                       textbgcolour  = "#c0c0c0"];
 *   |||;
 *
 *   minos => janus     [label = "process_minos_states()",
 *                       url   = "@ref process_minos_states"];
 *     janus => janus     [label = "State machine"];
 *     janus note janus   [label = "If state transition required",
 *                         textbgcolour  = "#c0c0c0"];
 *       janus => karajan   [label = "karajan_leaves_janus_state()",
 *                           url   = "@ref karajan_leaves_janus_state"];
 *         karajan => karajan [label = "Process state transition"];
 *       karajan >> janus;
 *     janus note janus   [label = "End if",
 *                       textbgcolour  = "#c0c0c0"];
 *   janus >> minos;
 *
 *   |||;
 *
 * @endmsc
 *
 * You may be surprised that there isn't too much going on in @minos during normal
 *   operation. Well, that's the idea, to leave more CPU time to the user software.
 *
 * \n \n @section minos_the_end The End
 *
 * This is a high level view of @minos. \n \n
 *
 *
 * Who was Minos, again? \n
 *
 * Minos comes from Greek mythology. He was the first King of Crete, son of Zeus and
 *   Europa. "Minos" is often interpreted as the Cretan word for "King". \n
 *
 * @image html "wikipedia_minos.jpg"
 *
 * After his death, he became the judge of the dead in the underworld. \n
 *
 * More about Minos can be found on his
 *   <a href="https://en.wikipedia.org/wiki/Minos">Wikipedia page</a>.
 *
 *
 */
