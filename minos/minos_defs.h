/** @cond license
 * MinOS
 * Copyright (C) 2020 Nicu Gavrila (https://www.linkedin.com/in/nicu-gavrila/)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 * @endcond
 */

#ifndef MINOS_DEFS_H
#define MINOS_DEFS_H

typedef unsigned char        minos_u8;    /**< @brief Unsigned integer, 8-bit */
typedef   signed char        minos_s8;    /**< @brief   Signed integer, 8-bit */

typedef unsigned short       minos_u16;   /**< @brief Unsigned integer, 16-bit */
typedef   signed short       minos_s16;   /**< @brief   Signed integer, 16-bit */

typedef unsigned long        minos_u32;   /**< @brief Unsigned integer, 32-bit */
typedef   signed long        minos_s32;   /**< @brief   Signed integer, 32-bit */

typedef unsigned long long   minos_u64;   /**< @brief Unsigned integer, 64-bit */
typedef   signed long long   minos_s64;   /**< @brief   Signed integer, 64-bit */

#include "minos_cfg.h"


#define MINOS_NULL 0

#define MINOS_ON 1
#define MINOS_OFF 0

#define MINOS_YES 1
#define MINOS_NO 0


/**
 * @brief An enumeration of all possible return values for @minos @ref minos.h "API"
 *   functions.
 *
 * @details Any return value less than 0 represents an error.
 */
enum MINOS_RETURN_VALUES
{
	MINOS_PENDING   =  1,   /**< @minos started to execute something but returned
	                               before finishing; this function should be called
	                               again to complete its task */

	MINOS_SUCCESS   =  0,   /**< Success */

	/* error values */
	MINOS_ERROR     = -1,   /**< Generic error */
};


/**
 * @brief An enumeration of all @minos states.
 *
 * @details For more information about system states and state transitions, see @janus.
 */
enum minos_state_t
{
	MINOS_HW_INIT         =  0,   /**< The default state of the system after power up.
                                             Microcontroller specific initialization
                                             routines are executed. After executing the
                                             required operations, the next state will be
                                             @ref MINOS_OS_INIT. */
	MINOS_OS_INIT         =  1,   /**< @minos specific initialization routines are
                                             executed, after which the @minos switches to
                                             the @ref MINOS_USER_INIT state. */
	MINOS_USER_INIT       =  2,   /**< The initialization function of each user
	                                     software component is called and the @minos
	                                     moves to the @ref MINOS_RUN state. */
	MINOS_SLEEP           =  3,   /**< The system is in low power mode, but some
                                             peripherals may still be running. @minos will
                                             only run the @ref MINOS_SLEEP_FUNCTION in
                                             this state. */
	MINOS_STANDBY         =  4,   /**< @minos is in standby mode, scheduler runs.
	                                     There is no difference between this state
	                                     and @ref MINOS_RUN as far as @minos is
	                                     concerned (except the fact that in this
	                                     state sub-states are not available). The
	                                     difference (if there is one) is only for
	                                     the user components. */
	MINOS_RUN             =  5,   /**< This is the normal operation state of the
	                                     system, the scheduler runs and sub-states
	                                     are available.  */
	MINOS_CRITICAL_ERROR  =  6,   /**< The system has encountered a critical and
	                                     unrecoverable error and is now stopped in
	                                     an infinite loop waiting to be reset. */
};


/**
 * @brief This structure is used by @janus callbacks.
 *
 * @details Given as a parameter, they will inform the software components about the
 *   transition that is currently taking place. \n
 *
 * Check the janus_callbacks section in the documentation for @janus for more details on
 *   callbacks.
 */
struct minos_state_transition_t
{
	enum minos_state_t    current_state;     /**< @brief The state to be left after
	                                              all callbacks are called */

	enum minos_state_t    next_state;        /**< @brief The state to be entered
	                                              after all callbacks are called */

	minos_run_substate_t  current_substate;  /**< @brief The substate to be left
	                                              after all callbacks are called */

	minos_run_substate_t  next_substate;     /**< @brief The substate to be entered
	                                              after all callbacks are called */
};


/**
 * @brief A @janus callback should have this prototype.
 *
 * @details Check the janus_callbacks section in the documentation for @janus for more
 *   details on callbacks.
 */
typedef void (*janus_callback_t)(struct minos_state_transition_t transition);


#endif // MINOS_DEFS_H



/**
 * @file
 *
 * On this page you can find the various data types and constants defined in @minos.
 *   All these are used by @minos and you should use them, too, in conjuncture with
 *   any @minos @ref minos.h "API" function. \n
 *
 * Some of the data types are configurable, therefore defined and documented in the
 *   @ref minos_cfg.h "Configuration" page \n
 *
 * Some definitions here are so self explanatory that any documentation I would add would
 *   result in a pleonasm.
 *
 */
