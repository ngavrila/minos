/** @cond license
 * MinOS
 * Copyright (C) 2020 Nicu Gavrila (https://www.linkedin.com/in/nicu-gavrila/)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 * @endcond
 */

#include "minos_internal.h"


/**
 * @brief Maximum number of activations for a task at any given point.
 *
 * @details This limit applies only to one-shot tasks, since periodic tasks will have
 *   only the values <c><b>0</b></c> or <c><b>1</b></c> for their
 *   @ref task_entry::activations "activations" parameter.
 */
#define MAX_ACTIVATIONS    0xFF


/**
 * @brief Structure that defines all parameters of a task.
 */
struct task_entry
{
	void (*task)(void);              /**< @brief The function that will be called
	                                        when @karajan runs a task.
	                                      @details This cannot be modified. If you
	                                        want to modify it, simply delete this task
	                                        using @ref delete_minos_task and create
	                                        a new one using @ref create_minos_task. */

	minos_handle_t prio;             /**< @brief The priority number of the task.
	                                      @details The lower the number, the higher
	                                        the priority. This parameter is set once
	                                        when you create the task by calling
	                                        @ref create_minos_task and cannot be
	                                        modified. */

	minos_tick_t period;             /**< @brief The period with which the task is to
	                                        be executed in <c><b>ticks</b></c>.
	                                      @details A period of <c><b>0</b></c>
	                                        (which is the default when creating a
	                                        task) means that the task is
	                                        @ref karajan_one_shot_tasks "one-shot".
	                                        Can be modified using
	                                        @ref change_minos_task_period.
	                                        Changing the period to a value different
	                                        from <c><b>0</b></c> will change the task
	                                        to @ref karajan_periodic_tasks "periodic".
	                                        You can change it back to
	                                        @ref karajan_one_shot_tasks "one-shot"
	                                        at any point by changing the period
	                                        back to <c><b>0</b></c>. \n
	                                        When you change the type of the task,
	                                        it will be automatically
	                                        @ref karajan_task_activate_stop_suspend
	                                        "stopped". */

	minos_tick_t delay;              /**< @brief The delay with which the task
	                                        will start it's execution in
	                                        <c><b>ticks</b></c>.
	                                      @details Can be modified using
	                                        @ref change_minos_task_delay. */

	minos_tick_t runpoint;           /**< @brief The next point in time when
	                                        the task has to be executed if it's
	                                        @ref karajan_task_activate_stop_suspend
	                                        "active" in <c><b>ticks</b></c>.
	                                      @details It is not accessible outside
	                                        @karajan. */

	minos_u8 activations;            /**< @brief How many times the task has to run.
	                                      @details A number greater than
	                                        <c><b>0</b></c> means the task is
	                                        @ref karajan_task_activate_stop_suspend
	                                        "active".
	                                        The values it takes are <c><b>0</b></c>
	                                        and <c><b>1</b></c> for
	                                        @ref karajan_periodic_tasks "periodic"
	                                        tasks and <c><b>0</b></c> to
	                                        @ref MAX_ACTIVATIONS for
	                                        @ref karajan_one_shot_tasks "one-shot"
	                                        tasks. It is not accessible outside
	                                        @karajan, but it can be set to
	                                        <c><b>1</b></c> (for
	                                        @ref karajan_periodic_tasks "periodic"
	                                        tasks) or incremented (for
	                                        @ref karajan_one_shot_tasks "one-shot"
	                                        tasks) by calling @ref activate_minos_task
	                                        and set to <c><b>0</b></c> by calling
	                                        @ref stop_minos_task. */

	minos_u8 runpoint_overflow : 1;  /**< @brief Marks if the next runpoint is in the
	                                        future, but the runpoint value is less
	                                        than the current time value.
	                                      @details Useful when the system time counter
	                                        overflows. It is not accessible outside
	                                        @karajan. */

	minos_u8 suspended : 1;          /**< @brief Stores the information if a task is
	                                        @ref karajan_task_activate_stop_suspend
	                                        "suspended" or not.
	                                      @details It is not accessible outside
	                                        @karajan, but it can be set to
	                                        <c><b>1</b></c> by calling
	                                        @ref suspend_minos_task
	                                        and set to <c><b>0</b></c> by calling
	                                        @ref activate_minos_task. */
};


/**
 * @brief An array that keeps all the information about all the tasks in the system. \n
 *
 * @details It's size macro @ref MAX_MINOS_TASKS is configurable in file @ref minos_cfg.h
 *   and it should be greater or equal than the maximum number of tasks you intend to use
 *   at one time in the system.
 */
struct task_entry task_array[MAX_MINOS_TASKS];


/**
 * @brief An array which stores the available free handles, which can be used when
 *   creating new tasks.
 *
 * @details It stores the indexes from @ref task_array that are free.
 */
minos_handle_t   free_task_handles[MAX_MINOS_TASKS];


/**
 * @brief The indexes of the tasks from @ref task_array sorted by their priority number.
 */
minos_handle_t   task_prio_order[MAX_MINOS_TASKS];


/**
 * @brief The current number of tasks registered with @karajan.
 */
minos_defdata_t  number_of_tasks;


/**
 * @brief A variable that is updated with the current system time counter value every
 *   time @ref karajan_schedule is called.
 *
 * @details This is done in order to ensure that the same value for time is used during
 *   the entire scheduling cycle.
 */
minos_tick_t current_schedule_time = 0;


/**
 * @brief A variable that stores the system time counter value used for the previous
 *   scheduling cycle.
 *
 * @details This value is used to detect any system time counter overflows.
 */
minos_tick_t last_schedule_time = 0;


/**
 * @brief Stores the handle of the last task started by @karajan.
 *
 * @details Used by @karajan to detect if an operation like calculating the next runpoint
 *   for the task has been done while the task was running. \n
 *
 * @karajan will not recalculate the runpoint in this case.
 */
minos_handle_t last_started_task;


/**
 * @brief A flag that signals that the runpoint of the last task started by @karajan
 *   has been calculated during its execution.
 *
 * @details This variable is set by function @ref activate_minos_task when activating
 *   and calculating the next runpoint of the task that has its handle stored in
 *   @ref last_started_task. \n
 *
 * @karajan will not recalculate the runpoint of that task if the task is actually
 *   currently running, nor will it decrement its
 *   @ref task_entry::activations "activations" parameter.
 */
minos_defdata_t next_runpoint_calculated;

/**
 * @brief A helper function that sets the next runpoint of the task after it has been
 *   executed by the scheduler.
 *
 * @details For more information about the runpoint calculation, check the
 *   @ref karajan_runpoint_calculation section.
 */
void setup_next_task_runpoint(minos_handle_t handle);


/**
 * @brief This function initializes @karajan, the scheduler of @minos.
 */
void karajan(void)
{
	minos_u8 i;

	for (i = 0; i < MAX_MINOS_TASKS; i++)
	{
		task_array[i].task = minos_empty_function;
		free_task_handles[i] = i;
	}

	number_of_tasks = 0;
}


/**
 * @brief This function is called by @janus to inform @karajan about a state change.
 *
 * @details @karajan is only interested in the old state, because the new state can be
 *   deduced from that. \n
 *
 * @karajan only acts on transitions from @ref MINOS_USER_INIT to
 *   @ref MINOS_RUN (this is the only valid transition from @ref MINOS_USER_INIT) and
 *   from @ref MINOS_SLEEP to @ref MINOS_STANDBY (the only valid transition from
 *   @ref MINOS_SLEEP). \n
 *
 * This function resynchronizes all tasks so that they all have a common point from
 *   which their period/delay applies. This feature allows you to control the CPU
 *   load over time and avoid high peaks. For more details about this topic, plus a
 *   graphical representation, see @ref karajan_task_delay section.
 *
 * @param old_state  The state @minos is about to exit from.
 */
void karajan_leaves_janus_state(enum minos_state_t old_state)
{
	minos_handle_t tmp;
	minos_handle_t i;

	current_schedule_time = get_minos_time();
	last_schedule_time = current_schedule_time;

	if ((old_state == MINOS_USER_INIT) || (old_state == MINOS_SLEEP))
	{
		for (i = 0; i < number_of_tasks; i++)
		{
			tmp = task_prio_order[i];
			task_array[tmp].runpoint = current_schedule_time;
			task_array[tmp].runpoint += task_array[tmp].delay;

			if (task_array[tmp].runpoint < current_schedule_time)
			{
				task_array[tmp].runpoint_overflow = 1;
			}
		}

	}
}


void setup_next_task_runpoint(minos_handle_t handle)
{
	MINOS_INTERRUPT_DISABLE();

	if (task_array[handle].activations != 0)
	{
		if (task_array[handle].period == 0)
		{
			if (task_array[handle].suspended == 0)
			{
				task_array[handle].activations--;
			}

			if (task_array[handle].activations != 0)
			{
				task_array[handle].runpoint += task_array[handle].delay;
			}
		}
		else
		{
			task_array[handle].runpoint += task_array[handle].period;
		}

		if (task_array[handle].runpoint < current_schedule_time)
		{
			task_array[handle].runpoint_overflow = 1;
		}
	}

	MINOS_INTERRUPT_RESTORE();
}


/**
 * @brief The implementation of the scheduler itself.
 *
 * @details Each time it is called it tries to run a task. It searches through the list
 *   of registered tasks, in order of their priority, and as soon as it finds one active
 *   and ready for execution, it runs it and returns @ref MINOS_YES. If all tasks have
 *   been checked and none is ready for execution, it returns @ref MINOS_NO. This is
 *   useful information for @ref minos(), since it knows that the system may take a
 *   break (see @ref MINOS_HALT_INSTRUCTION for more details).
 *
 * @return @ref MINOS_PENDING if a task was executed, otherwise @ref MINOS_SUCCESS
 */
minos_return_t karajan_schedule(void)
{
	minos_handle_t i;
	minos_handle_t tmp;

	minos_return_t ret_val = MINOS_SUCCESS;

	current_schedule_time = get_minos_time();

	MINOS_INTERRUPT_DISABLE();

	if (last_schedule_time > current_schedule_time)
	{
		for (i = 0; i < number_of_tasks; i++)
		{
			tmp = task_prio_order[i];
			task_array[tmp].runpoint_overflow = 0;
		}
	}

	last_schedule_time = current_schedule_time;

	for (i = 0; i < number_of_tasks; i++)
	{
		tmp = task_prio_order[i];
		if ((task_array[tmp].task != minos_empty_function) &&
		    (task_array[tmp].activations != 0)   &&
		    (task_array[tmp].runpoint_overflow == 0))
		{
			if (task_array[tmp].runpoint <= current_schedule_time)
			{
				last_started_task = tmp;
				next_runpoint_calculated = 0;

				if (task_array[tmp].suspended == 0)
				{
					MINOS_INTERRUPT_RESTORE();
					task_array[tmp].task();
					MINOS_INTERRUPT_DISABLE();

					if (next_runpoint_calculated == 0)
					{
						setup_next_task_runpoint(tmp);
					}

					ret_val = MINOS_PENDING;

					break;
				}
				else
				{
					if (task_array[tmp].runpoint < current_schedule_time)
					{
						if (next_runpoint_calculated == 0)
						{
							setup_next_task_runpoint(tmp);
						}
					}
				}
			}
		}

		MINOS_INTERRUPT_RESTORE();
		// Allow interrupts here
		MINOS_INTERRUPT_DISABLE();
	}

	MINOS_INTERRUPT_RESTORE();

	return ret_val;
}


minos_return_t create_minos_task(void (*task)(void), minos_defdata_t prio)
{
	minos_handle_t   handle;
	minos_handle_t   i;
	minos_handle_t   tmp;

	if ((task != MINOS_NULL) && (number_of_tasks < MAX_MINOS_TASKS ))
	{
		MINOS_INTERRUPT_DISABLE();

		handle = free_task_handles[MAX_MINOS_TASKS - 1 - number_of_tasks];

		task_array[handle].task              = task;
		task_array[handle].prio              = prio;
		task_array[handle].period            = 0;
		task_array[handle].delay             = 0;
		task_array[handle].runpoint          = 0;
		task_array[handle].activations       = 0;
		task_array[handle].runpoint_overflow = 0;
		task_array[handle].suspended         = 0;

		free_task_handles[MAX_MINOS_TASKS - 1 - number_of_tasks] = 0xFF;

		for (i = number_of_tasks; i > 0; i--)
		{
			tmp = task_prio_order[i - 1];
			if (task_array[tmp].prio <= prio)
			{
				break;
			}

			task_prio_order[i] = task_prio_order[i - 1];
		}
		task_prio_order[i] = handle;

		number_of_tasks++;

		MINOS_INTERRUPT_RESTORE();

		return handle;
	}

	return MINOS_ERROR;
}


minos_return_t delete_minos_task(minos_handle_t handle)
{
	minos_defdata_t found;
	minos_handle_t i;

	minos_return_t ret_val = MINOS_ERROR;

	MINOS_INTERRUPT_DISABLE();

	if ((handle < MAX_MINOS_TASKS) && (task_array[handle].task != minos_empty_function))
	{

		free_task_handles[MAX_MINOS_TASKS - number_of_tasks] = handle;
		task_array[handle].task = minos_empty_function;

		found = MINOS_NO;
		for (i = 0; i < (number_of_tasks - 1); i++)
		{
			if (task_prio_order[i] == handle)
			{
				found = MINOS_YES;
			}
			if (found != MINOS_NO)
			{
				task_prio_order[i] = task_prio_order[i + 1];
			}
		}

		number_of_tasks--;

		ret_val = MINOS_SUCCESS;
	}

	MINOS_INTERRUPT_RESTORE();

	return ret_val;
}


minos_return_t change_minos_task_period(minos_handle_t handle, minos_tick_t period)
{
	minos_return_t ret_val = MINOS_ERROR;

	MINOS_INTERRUPT_DISABLE();

	if ((handle < MAX_MINOS_TASKS) && (task_array[handle].task != minos_empty_function))
	{
		if (task_array[handle].period == 0)
		{
			if (period != 0)
			{
				task_array[handle].activations = 0;
			}
		}
		else
		{
			if (period == 0)
			{
				task_array[handle].activations = 0;
				task_array[handle].suspended = 0;
			}
		}

		task_array[handle].period = period;

		ret_val = MINOS_SUCCESS;
	}

	MINOS_INTERRUPT_RESTORE();

	return ret_val;
}


minos_return_t change_minos_task_delay(minos_handle_t handle, minos_tick_t delay)
{
	minos_return_t ret_val = MINOS_ERROR;

	MINOS_INTERRUPT_DISABLE();

	if ((handle < MAX_MINOS_TASKS) && (task_array[handle].task != minos_empty_function))
	{
		task_array[handle].delay = delay;

		ret_val = MINOS_SUCCESS;
	}

	MINOS_INTERRUPT_RESTORE();

	return ret_val;
}


minos_return_t activate_minos_task(minos_handle_t handle)
{
	minos_tick_t current_time;

	minos_return_t ret_val = MINOS_ERROR;

	MINOS_INTERRUPT_DISABLE();

	if ((handle < MAX_MINOS_TASKS) && (task_array[handle].task != minos_empty_function))
	{
		ret_val = MINOS_SUCCESS;

		if (task_array[handle].suspended == 0)
		{
			if (task_array[handle].activations == 0)
			{
				current_time = get_minos_time();
				task_array[handle].runpoint = current_time;
				task_array[handle].runpoint += task_array[handle].delay;

				if (task_array[handle].runpoint < current_time)
				{
					task_array[handle].runpoint_overflow = 1;
				}

				if (last_started_task == handle)
				{
					next_runpoint_calculated = 1;
				}
			}

			if (task_array[handle].period == 0)
			{
				if (task_array[handle].activations < MAX_ACTIVATIONS)
				{
					task_array[handle].activations++;
				}
				else
				{
					ret_val = MINOS_ERROR;
				}
			}
			else
			{
				if (task_array[handle].activations == 0)
				{
					task_array[handle].activations = 1;
				}
			}
		}
		else
		{
			task_array[handle].suspended = 0;

			if (task_array[handle].runpoint < get_minos_time())
			{
				task_array[handle].runpoint += task_array[handle].period;
			}
		}
	}

	MINOS_INTERRUPT_RESTORE();

	return ret_val;
}


minos_return_t stop_minos_task(minos_handle_t handle)
{
	minos_return_t ret_val = MINOS_ERROR;

	MINOS_INTERRUPT_DISABLE();

	if ((handle < MAX_MINOS_TASKS) && (task_array[handle].task != minos_empty_function))
	{
		task_array[handle].activations = 0;
		task_array[handle].suspended = 0;

		ret_val = MINOS_SUCCESS;
	}

	MINOS_INTERRUPT_RESTORE();

	return ret_val;
}


minos_return_t suspend_minos_task(minos_handle_t handle)
{
	minos_return_t ret_val = MINOS_ERROR;

	MINOS_INTERRUPT_DISABLE();

	if ((handle < MAX_MINOS_TASKS) && (task_array[handle].task != minos_empty_function))
	{
		if ((task_array[handle].activations != 0) &&
		    (task_array[handle].period != 0))
		{
			task_array[handle].suspended = 1;

			ret_val = MINOS_SUCCESS;
		}
	}

	MINOS_INTERRUPT_RESTORE();

	return ret_val;
}




/**
 * @file
 *
 * @karajan is the @minos component that implements the task scheduler.
 *   This functionality is the core of any operating system. If there are any components
 *   that can be removed from @minos, this one si definitely not one of them. \n \n
 *
 *
 *
 * \n \n @section karajan_task_parameters The Tasks and Their Parameters
 *
 * So, @karajan is a task scheduler. What is a task? Simple, a task is
 *   a function plus some parameters. The function has to have the following prototype:
 *   <c><b>void task(void)</b></c>. All parameters of a task can be found in the
 *   documentation for the @ref task_entry structure:
 *
 * @anchor task_parameter_table
 * <table>
 *   <caption id="task_parameters_table">Parameters of the Tasks</caption>
 *
 *   <tr><th> State                  <th>Description
 *   <tr><td> task                   <td> @copydoc task_entry::task
 *   <tr><td> prio                   <td> @copydoc task_entry::prio
 *   <tr><td> period                 <td> @copydoc task_entry::period
 *   <tr><td> delay                  <td> @copydoc task_entry::delay
 *   <tr><td> runpoint               <td> @copydoc task_entry::runpoint
 *   <tr><td> activations            <td> @copydoc task_entry::activations
 *   <tr><td> runpoint_overflow      <td> @copydoc task_entry::runpoint_overflow
 *   <tr><td> suspended              <td> @copydoc task_entry::suspended
 * </table>
 *
 * @karajan relies heavily on the system time to operate accurately. The system time is
 *   kept by the @minos component called @chronos. You may want to have a look at its
 *   @ref chronos.c "documentation". \n
 *
 * You may also want to take a look at the documentation for @ref minos_tick_t data type.
 *   You will find useful information about what a <c><b>tick</b></c> is and how you can
 *   decide its duration.
 *
 *
 *
 * \n \n @section karajan_task_creation How to Create and Delete Tasks
 *
 * Creating a task in @minos is simple. You call @ref create_minos_task and you're done.
 *   You will have to supply a function and a priority, both of which cannot be changed
 *   later. You will have to decide on the priorities of all tasks in your system.
 *   One would usually order the tasks in order of importance and then assign them
 *   priority numbers. There is no restriction on the actual priority numbers as far as
 *   @minos is concerned, they don't have to be consecutive or unique. Assign them as you
 *   see fit. Just keep in mind, the lower the priority number, the higher the priority.
 *   Just like an order number, the lower the number, the quicker your order delivery. \n
 *
 * A good design practice would be to assign non consecutive numbers, so that if later
 *   on you decide to add a new task to the system and its priority is in the middle
 *   you don't have to update all lower priorities. \n
 *
 * For example, let's take the following tasks and assigned priorities:
 *
 * | Task function | Priority number |
 * | :------------ | :-------------: |
 * | task_1        | 1               |
 * | task_2        | 2               |
 * | task_3        | 3               |
 * | task_4        | 4               |
 *
 * If you want to add <c><b>task_1dot5</b></c> to the system, which from the priority
 *   number point of view would fit between <c><b>task_1</b></c> and <c><b>task_2</b></c>,
 *   you would have to modify the priorities of tasks <c><b>task_2</b></c>,
 *   <c><b>task_3</b></c> and <c><b>task_4</b></c> to fit it. That can be avoided by
 *   assigning priorities like this:
 *
 * | Task function | Priority number |
 * | :------------ | :-------------: |
 * | task_1        | 10              |
 * | task_2        | 20              |
 * | task_3        | 30              |
 * | task_4        | 40              |
 *
 * Now you would just add <c><b>task_1dot5</b></c> with a priority of <c><b>15</b></c>
 *   and you're done! \n
 *
 * One more thing, the priority number data type in MinOS can be configured by changing
 *   the definition of @ref minos_defdata_t in @ref minos_cfg.h. \n \n
 *
 *
 * @ref create_minos_task will return a @ref minos_handle_t "task handle", if it succeeds.
 *   This @ref minos_handle_t "task handle" must be stored and used whenever you want to
 *   change the properties or status of the newly created task. If it fails and the new
 *   task is not created, it will return @ref MINOS_ERROR. \n \n
 *
 *
 * Here is a coding example of creating a task:
 *
 * @code
 *
 * #define TASK_PRIORITY  10
 *
 * minos_handle_t task_handle;
 *
 * void task_function(void)
 * {
 *         // do some important work here
 * }
 *
 *
 * void component(void)
 * {
 *         // stuff
 *
 *         task_handle = create_minos_task(task_function, TASK_PRIORITY);
 *
 *         if (task_handle == MINOS_ERROR)
 *         {
 *                 // task creation failed
 *         }
 *         else
 *         {
 *                 // task creation succeeded
 *         }
 *
 *         // some more stuff
 * }
 *
 * @endcode
 *
 * If you want to delete a task, although I'm not sure why you would do this in an
 *   embedded system, you can use the @ref delete_minos_task function. You will need to
 *   suply the @ref minos_handle_t "task handle" of the task you want to delete as a
 *   parameter. If the deletion succeeds, @ref delete_minos_task will return
 *   @ref MINOS_SUCCESS, if it fails it will return @ref MINOS_ERROR.
 *
 *
 *
 * \n \n @section karajan_task_types Types of Tasks
 *
 * There are two types of tasks supported by @karajan:
 *
 *   * one-shot (it's @ref task_entry::period "period" parameter is <c><b>0</b></c>)
 *
 *     * Call @ref activate_minos_task once, it will run once (after the
 *        @ref task_entry::delay "delay" expires), call it twice, and the task will run
 *        twice (both times after @ref task_entry::delay "delay" expires).
 *
 *     * The maximum times the task can be activated is hardcoded by @ref MAX_ACTIVATIONS.
 *
 *     * This is the default task type when creating a tasl using @ref create_minos_task.
 *
 *   * periodic (it's task_entry::period "period" parameter is <c><b>>0</b></c>)
 *
 *     * Call @ref activate_minos_task and it will start running every
 *       @ref task_entry::period "period"
 *       ticks after an initial delay of @ref task_entry::delay "delay".
 *
 * \n
 *
 *
 * \n @subsection karajan_one_shot_tasks One-Shot Tasks
 *
 * This is the default type of task that is created when calling @ref create_minos_task.
 *   As mentioned above, an <c><b>one-shot</b></c> task is a task that is executed as
 *   many times as you call the @ref activate_minos_task function for it. Its
 *   task_entry::period "period" parameter is set to <c><b>>0</b></c>, but you can change
 *   its task_entry::delay "delay" parameter to control when it is going to be executed
 *   by @karajan.
 *
 * Here is an scheduling example of an one-shot task with a delay of 3 ticks:
 *
 * @anchor karajan_one_shot_task_example
 * @msc "One-shot tasks - scheduling example"
 *
 *   width = "1050";
 *
 *   chronos [label=""],
 *   user_cmp [label=""],
 *   o [label=""],
 *   karajan [label=""];
 *
 *   chronos    box chronos    [label = "Chronos",       url = "@ref chronos.c"],
 *   user_cmp   box user_cmp   [label = "User component"],
 *   o          box o          [label = "One_shot_task"],
 *   karajan    box karajan    [label = "Karajan",       url = "@ref karajan.c"];
 *
 *   ...;
 *
 *   chronos note karajan [label = "The system is in state MINOS_USER_INIT, so the
 *                                  User component is creating and configuring a task",
 *                         url   = "@ref MINOS_USER_INIT",
 *                         textbgcolour  = "#c0c0c0"];
 *
 *   ...;
 *
 *   user_cmp => karajan  [label = "create_minos_task(One_shot_task, prio)",
 *                         url   = "@ref create_minos_task"];
 *   karajan >> user_cmp  [label = "task_handle",
 *                         url   = "@ref minos_handle_t"];
 *
 *   |||;
 *
 *   user_cmp => karajan  [label = "change_minos_task_delay(task_handle, 3)",
 *                         url   = "@ref change_minos_task_delay"];
 *   karajan >> user_cmp  [label = "MINOS_SUCCESS",
 *                         url   = "@ref MINOS_SUCCESS"];
 *
 *   |||;
 *
 *   user_cmp => karajan  [label = "activate_minos_task(task_handle)",
 *                         url   = "@ref activate_minos_task"];
 *   karajan >> user_cmp  [label = "MINOS_SUCCESS",
 *                         url   = "@ref MINOS_SUCCESS"];
 *
 *   ...;
 *
 *   chronos note karajan [label = "The system goes to state MINOS_RUN, so the scheduler
 *                                  starts running tasks",
 *                         url   = "@ref MINOS_RUN",
 *                         textbgcolour  = "#c0c0c0"];
 *
 *   ...;
 *
 *   chronos box chronos [label =  "0"];
 *   chronos box chronos [label =  "1"];
 *   chronos box chronos [label =  "2"];
 *   chronos box chronos [label =  "3"],---,karajan => o [label = "Run one-shot task"];
 *   chronos box chronos [label =  "4"];
 *   chronos box chronos [label =  "5"];
 *   chronos box chronos [label =  "6"];
 *   chronos box chronos [label =  "7"];
 *   chronos box chronos [label =  "8"];
 *   chronos box chronos [label =  "9"];
 *   chronos box chronos [label = "10"];
 *
 *                       user_cmp => karajan [label = "activate_minos_task(task_handle)",
 *                                            url = "@ref activate_minos_task"];
 *                       user_cmp => karajan [label = "activate_minos_task(task_handle)",
 *                                            url = "@ref activate_minos_task"];
 *                       user_cmp => karajan [label = "activate_minos_task(task_handle)",
 *                                            url = "@ref activate_minos_task"];
 *
 *   chronos box chronos [label = "11"];
 *   chronos box chronos [label = "12"];
 *   chronos box chronos [label = "13"],---,karajan => o [label = "Run one-shot task"];
 *   chronos box chronos [label = "14"];
 *   chronos box chronos [label = "15"];
 *   chronos box chronos [label = "16"],---,karajan => o [label = "Run one-shot task"];
 *   chronos box chronos [label = "17"];
 *   chronos box chronos [label = "18"];
 *   chronos box chronos [label = "19"],---,karajan => o [label = "Run one-shot task"];
 *   chronos box chronos [label = "20"];
 *   chronos box chronos [label = "21"];
 *
 *   ...;
 *
 * @endmsc
 *
 *
 * One special feature of this type of tasks is that you can configure another delay
 *   every time this function runs (the initialization stage is identical to the
 *   @ref karajan_one_shot_task_example "One-shot tasks - scheduling example" diagram):
 *
 * @anchor karajan_one_shot_different_delay_task_example
 * @msc "One-shot task with different delay - scheduling example"
 *
 *   width = "1050";
 *
 *   chronos [label=""],
 *   user_cmp [label=""],
 *   o [label=""],
 *   karajan [label=""];
 *
 *   chronos    box chronos    [label = "Chronos",       url = "@ref chronos.c"],
 *   user_cmp   box user_cmp   [label = "User component"],
 *   o          box o          [label = "One_shot_task"],
 *   karajan    box karajan    [label = "Karajan",       url = "@ref karajan.c"];
 *
 *   ...;
 *   ...;
 *
 *   chronos note karajan [label = "The system goes to state MINOS_RUN, so the scheduler
 *                                  starts running tasks",
 *                         url   = "@ref MINOS_RUN",
 *                         textbgcolour  = "#c0c0c0"];
 *
 *   ...;
 *
 *   chronos box chronos [label =  "0"];
 *   chronos box chronos [label =  "1"];
 *   chronos box chronos [label =  "2"];
 *   chronos box chronos [label =  "3"],---,karajan => o [label = "Run one-shot task"];
 *                       o => karajan [label = "change_minos_task_delay(task_handle, 7)",
 *                                     url   = "@ref change_minos_task_delay"];
 *                       o => karajan [label = "activate_minos_task(task_handle)",
 *                                     url   = "@ref activate_minos_task"];
 *   chronos box chronos [label =  "4"];
 *   chronos box chronos [label =  "5"];
 *   chronos box chronos [label =  "6"];
 *   chronos box chronos [label =  "7"];
 *   chronos box chronos [label =  "8"];
 *   chronos box chronos [label =  "9"];
 *   chronos box chronos [label = "10"],---,karajan => o [label = "Run one-shot task"];
 *                       o => karajan [label = "change_minos_task_delay(task_handle, 5)",
 *                                     url   = "@ref change_minos_task_delay"];
 *                       o => karajan [label = "activate_minos_task(task_handle)",
 *                                     url   = "@ref activate_minos_task"];
 *   chronos box chronos [label = "11"];
 *   chronos box chronos [label = "12"];
 *   chronos box chronos [label = "13"];
 *   chronos box chronos [label = "14"];
 *   chronos box chronos [label = "15"],---,karajan => o [label = "Run one-shot task"];
 *   chronos box chronos [label = "16"];
 *   chronos box chronos [label = "17"];
 *
 *   ...;
 *
 * @endmsc
 *
 * @karajan decides the new runpoint of every task right after it has been executed if
 *   the number of activations require the task to be executed again. This means that you
 *   can change the delay while the task is actually running. If the number of current
 *   activations is <c><b>0</b></c>, then the runpoint is calculated when you call
 *   @ref activate_minos_task. \n
 *
 * If you change the delay before the task is scheduled to run (and the task is active)
 *   the new delay will take effect only after it is executed. \n \n
 *
 *
 * Just keep in mind, the number of times an one-shot task is executed by @karajan is
 *   equal to the number of times you call @ref activate_minos_task! \n \n
 *
 *
 * Here is an example of changing the delay while the task is already scheduled (the
 *   initialization stage is identical to the one at @ref karajan_one_shot_task_example
 *   "One-shot tasks - scheduling example"):
 *
 *
 * @anchor karajan_one_shot_changing_delay_task_example
 * @msc "One-shot task, changing delay - scheduling example"
 *
 *   width = "1050";
 *
 *   chronos [label=""],
 *   user_cmp [label=""],
 *   o [label=""],
 *   karajan [label=""];
 *
 *   chronos    box chronos    [label = "Chronos",       url = "@ref chronos.c"],
 *   user_cmp   box user_cmp   [label = "User component"],
 *   o          box o          [label = "One_shot_task"],
 *   karajan    box karajan    [label = "Karajan",       url = "@ref karajan.c"];
 *
 *   ...;
 *   ...;
 *
 *   chronos note karajan [label = "The system goes to state MINOS_RUN, so the scheduler
 *                                  starts running tasks",
 *                         url   = "@ref MINOS_RUN",
 *                         textbgcolour  = "#c0c0c0"];
 *
 *   ...;
 *
 *   chronos box chronos [label =  "0"];
 *   chronos box chronos [label =  "1"];
 *   chronos box chronos [label =  "2"];
 *   chronos box chronos [label =  "3"],---,karajan => o [label = "Run one-shot task"];
 *   chronos box chronos [label =  "4"];
 *   chronos box chronos [label =  "5"];
 *                user_cmp => karajan [label = "change_minos_task_delay(task_handle, 5)",
 *                                     url   = "@ref change_minos_task_delay"];
 *                user_cmp => karajan [label = "activate_minos_task(task_handle)",
 *                                     url   = "@ref activate_minos_task"];
 *   chronos box chronos [label =  "6"];
 *   chronos box chronos [label =  "7"];
 *                user_cmp => karajan [label = "change_minos_task_delay(task_handle, 10)",
 *                                     url   = "@ref change_minos_task_delay"];
 *                user_cmp => karajan [label = "activate_minos_task(task_handle)",
 *                                     url   = "@ref activate_minos_task"];
 *   chronos box chronos [label =  "8"];
 *   chronos box chronos [label =  "9"];
 *   chronos box chronos [label = "10"],---,karajan => o [label = "Run one-shot task"];
 *   chronos box chronos [label = "11"];
 *   chronos box chronos [label = "12"];
 *   chronos box chronos [label = "13"];
 *   chronos box chronos [label = "14"];
 *   chronos box chronos [label = "15"];
 *   chronos box chronos [label = "16"];
 *   chronos box chronos [label = "17"];
 *   chronos box chronos [label = "18"];
 *   chronos box chronos [label = "19"];
 *   chronos box chronos [label = "20"],---,karajan => o [label = "Run one-shot task"];
 *   chronos box chronos [label = "21"];
 *
 *   ...;
 *
 * @endmsc
 *
 *
 * \n @subsection karajan_periodic_tasks Periodic Tasks
 *
 * Now let's see how periodic tasks behave. The important parameters for a periodic task
 *   are the @ref task_entry::delay "delay" and @ref task_entry::period "period".
 *   Usually you would set them at initialization time and they won't need to change,
 *   but it's your system, you do what you want! \n
 *
 * The periodic task is different from the one-shot tasks in the way that once activated,
 *   it will continue to run until you stop or suspend it. Therefore you would activate
 *   it just once. You won't get an error if you try to activate an already active
 *   periodic task, nor will it impact the execution of that task.
 *
 * The @ref task_entry::delay "delay" parameter is only used once, before the task is
 *   executed first. After that, the runpoint is calculated only based on the
 *   @ref task_entry::period "period" parameter. \n
 *
 * Here is a scheduling example of a periodic task with a delay of 5 ticks and
 *   a period of 10:
 *
 * @anchor karajan_periodic_task_example
 * @msc "Periodic tasks - scheduling example"
 *
 *   width = "1050";
 *
 *   chronos [label=""],
 *   user_cmp [label=""],
 *   p [label=""],
 *   karajan [label=""];
 *
 *   chronos    box chronos    [label = "Chronos",       url = "@ref chronos.c"],
 *   user_cmp   box user_cmp   [label = "User component"],
 *   p          box p          [label = "Periodic_task"],
 *   karajan    box karajan    [label = "Karajan",       url = "@ref karajan.c"];
 *
 *   ...;
 *
 *   chronos note karajan [label = "The system is in state MINOS_USER_INIT, so the
 *                                  User component is creating and configuring a task",
 *                         url   = "@ref MINOS_USER_INIT",
 *                         textbgcolour  = "#c0c0c0"];
 *
 *   ...;
 *
 *   user_cmp => karajan  [label = "create_minos_task(Periodic_task, prio)",
 *                         url   = "@ref create_minos_task"];
 *   karajan >> user_cmp  [label = "task_handle",
 *                         url   = "@ref minos_handle_t"];
 *
 *   |||;
 *
 *   user_cmp => karajan  [label = "change_minos_task_delay(task_handle, 5)",
 *                         url   = "@ref change_minos_task_delay"];
 *   karajan >> user_cmp  [label = "MINOS_SUCCESS",
 *                         url   = "@ref MINOS_SUCCESS"];
 *
 *   |||;
 *
 *   user_cmp => karajan  [label = "change_minos_task_period(task_handle, 10)",
 *                         url   = "@ref change_minos_task_delay"];
 *   karajan >> user_cmp  [label = "MINOS_SUCCESS",
 *                         url   = "@ref MINOS_SUCCESS"];
 *
 *   |||;
 *
 *   user_cmp => karajan  [label = "activate_minos_task(task_handle)",
 *                         url   = "@ref activate_minos_task"];
 *   karajan >> user_cmp  [label = "MINOS_SUCCESS",
 *                         url   = "@ref MINOS_SUCCESS"];
 *
 *   ...;
 *
 *   chronos note karajan [label = "The system goes to state MINOS_RUN, so the scheduler
 *                                  starts running tasks",
 *                         url   = "@ref MINOS_RUN",
 *                         textbgcolour  = "#c0c0c0"];
 *
 *   ...;
 *
 *   chronos box chronos [label =  "0"];
 *   chronos box chronos [label =  "1"];
 *   chronos box chronos [label =  "2"];
 *   chronos box chronos [label =  "3"];
 *   chronos box chronos [label =  "4"];
 *   chronos box chronos [label =  "5"],---,karajan => p [label = "Run periodic task"];
 *   chronos box chronos [label =  "6"];
 *   chronos box chronos [label =  "7"];
 *   chronos box chronos [label =  "8"];
 *   chronos box chronos [label =  "9"];
 *   chronos box chronos [label = "10"];
 *   chronos box chronos [label = "11"];
 *   chronos box chronos [label = "12"];
 *   chronos box chronos [label = "13"];
 *   chronos box chronos [label = "14"];
 *   chronos box chronos [label = "15"],---,karajan => p [label = "Run periodic task"];
 *   chronos box chronos [label = "16"];
 *   chronos box chronos [label = "17"];
 *   chronos box chronos [label = "18"];
 *   chronos box chronos [label = "19"];
 *   chronos box chronos [label = "20"];
 *   chronos box chronos [label = "21"];
 *   chronos box chronos [label = "22"];
 *   chronos box chronos [label = "23"];
 *   chronos box chronos [label = "24"];
 *   chronos box chronos [label = "25"],---,karajan => p [label = "Run periodic task"];
 *   chronos box chronos [label = "26"];
 *
 *
 *   ...;
 *
 * @endmsc
 *
 *
 * \n @subsection karajan_runpoint_calculation Runpoint Calculation
 *
 * Some information was presented before on how the next runpoint is calculated, but
 *   let's take a closer look at it. \n \n
 *
 *
 * The runpoint can be calculated at two different points. The calculation may differ
 *   for periodic and one-shot tasks:
 *
 *   * When the task is activated, if it is currently stopped.
 *     * For both types of tasks, at activation the next runpoint is calculated as
 *       the current time as it is reported by @ref get_minos_time, plus the value of the
 *       @ref task_entry::delay "delay" parameter of the task.
 *     * Please note that the next runpoint is calculated at activation <b>only</b> if the
 *       task is stopped. If you activate a task twice, the runpoint will be calculated
 *       only at the first activation. For the second runpoint calculation see the next
 *       bullet point.
 *
 *   * Just after the task is executed, if the task needs to be executed again (either
 *     it's periodic, or it's one-shot but it has been activated more times that it has
 *     been executed so far).
 *     * Here we have different methods for different types of tasks
 *       * For periodic tasks, the new runpoint is calculated as the old runpoint plus
 *         the value of its @ref task_entry::period "period" parameter.
 *       * For one-shot tasks, the new runpoint is calculated as the old runpoint plus
 *         the value of its @ref task_entry::delay "delay" parameter.
 *
 * Once the runpoint has been calculated it cannot be modified. Any changes you make to
 *   the @ref task_entry::delay "delay" and/or @ref task_entry::period "period"
 *   parameters will take effect only for subsequent runpoint calculations.
 *
 *
 *
 * \n @subsection karajan_task_delay The Delay Task Parameter
 *
 * An important note for the @ref task_entry::delay "delay" parameter. This is useful
 *   to avoid CPU load peaks. Let's assume you have two periodic tasks, completely
 *   unrelated, one with a period of 2, the other with a period of 4. If you don't
 *   configure any delays (@ref task_entry::delay "delay" <c><b>= 0</b></c>), this is how
 *   their execution would look like:
 *
 * <table>
 *   <caption>Periodic tasks, without delays</caption>
 *
 *   <tr><th> Task    <th colspan="13"> Timeline and execution points
 *   <tr><th> Ticks   <td>0<td>1<td>2<td>3<td>4<td>5<td>6<td>7<td>8<td>9<td>10<td>11<td>12
 *   <tr><td> task_4  <td>X<td> <td> <td> <td>X<td> <td> <td> <td>X<td> <td>  <td>  <td>X
 *   <tr><td> task_2  <td>X<td> <td>X<td> <td>X<td> <td>X<td> <td>X<td> <td>X <td>  <td>X
 * </table>
 *
 * As you can see, there are quite high CPU loads at time indexes: 0, 4, 8 and 12. We can
 *   fix that by delaying one of the tasks. That task will respect it's period, but it
 *   would start with a delay. Essentially, it would be out of phase with the other
 *   one. \n
 *
 * Use this feature whenever you can to make better use of the processing resource at
 *   your disposal. \n
 *
 * Here is the previous example, but this time <c><b>task_4</b></c> has a
 *   @ref task_entry::delay "delay" of <c><b>1</b></c>:
 *
 * <table>
 *   <caption>Periodic tasks, with delays</caption>
 *
 *   <tr><th> Task    <th colspan="13"> Timeline and execution points
 *   <tr><th> Ticks   <td>0<td>1<td>2<td>3<td>4<td>5<td>6<td>7<td>8<td>9<td>10<td>11<td>12
 *   <tr><td> task_4  <td> <td>X<td> <td> <td> <td>X<td> <td> <td> <td>X<td>  <td>  <td>
 *   <tr><td> task_2  <td>X<td> <td>X<td> <td>X<td> <td>X<td> <td>X<td> <td>X <td>  <td>X
 * </table>
 *
 * The CPU is now better used, and the tasks will have better timings, because they
 *   don't have to fight for some processor time.
 *
 *
 *
 * \n \n @section karajan_task_activate_stop_suspend How to Activate, Suspend and Stop Tasks
 *
 * Each task has it's own state, with no connection to the system states implemented in
 *   @janus other than the fact that @karajan only runs in states @ref MINOS_RUN and
 *   @ref MINOS_STANDBY. \n
 *
 * Here is a state diagram, complete with the functions you can call to move one task
 *   from one state to another (if it's up to the user code):
 *
 * @dot
 *   digraph task_state_machine
 *   {
 *     compound=true;
 *     node [shape=Mrecord style="bold" fontname="times bold"];
 *     rankdir="LR";
 *     newrank=true;
 *     nodesep="0.6";
 *
 *     init [shape=point height=0.2 width=0.2 pos="0,0!"];
 *
 *     subgraph cluster_task_exists {
 *       fontsize=25;
 *       fontname="times bold";
 *
 *
 *       stopped    [label = "Stopped"];
 *
 *
 *       subgraph cluster_task_active {
 *         fontsize=25;
 *         fontname="times bold";
 *
 *         suspended  [label = "Suspended"];
 *         active     [label = "Active"];
 *         running    [label = "Running"];
 *
 *
 *         suspended  -> active    [label     = "activate_minos_task()"
 *                                  fontcolor = blue
 *                                  URL       = "@ref activate_minos_task"];
 *         active     -> suspended [label     = "suspend_minos_task()"
 *                                  fontcolor = blue
 *                                  URL       = "@ref suspend_minos_task"];
 *
 *         running    -> active    [label = "Task execution ends"];
 *         active     -> running   [label = "Karajan decides when"];
 *
 *         running    -> suspended [label     = "suspend_minos_task()" constraint=false
 *                                  fontcolor = blue
 *                                  URL       = "@ref suspend_minos_task"];
 *       }
 *
 *
 *       stopped -> active [dir       = back
 *                          label     = "stop_minos_task()"
 *                          lhead     = "cluster_task_active"
 *                          fontcolor = blue
 *                          URL       = "@ref stop_minos_task"];
 *       stopped -> active [label     = "activate_minos_task()"
 *                          fontcolor = blue
 *                          URL       = "@ref activate_minos_task"];
 *
 *     }
 *
 *     init     -> stopped [label     = "create_minos_task()"
 *                          fontcolor = blue
 *                          URL       = "@ref create_minos_task"];
 *     stopped  -> init    [label     = "delete_minos_task()"
 *                          ltail     = "cluster_task_exists"
 *                          fontcolor = blue
 *                          URL       = "@ref delete_minos_task"];
 *   }
 * @enddot
 *
 * <table>
 *   <caption id="task_states_table">States Table</caption>
 *
 *   <tr><th>State                   <th>Description
 *   <tr><td> non-existent  <td> <p> Represented by the black dot, it simply means that
 *                                   the task does not exist, so it actually has no
 *                                   state. It either hasn't been created yet or it has
 *                                   been deleted. \n
 *                                   A task is created with @ref create_minos_task and it
 *                                   will be created in state <c><b>Stopped</b></c>. \n
 *                                   A task is deleted with @ref delete_minos_task. A
 *                                   task can be deleted regardless of its current
 *                                   state \n
 *                               </p>
 *   <tr><td> Stopped       <td> <p> This is the state of a task when newly created.
 *                                   @karajan is aware of it and you can use the handle
 *                                   you got to change the parameters of the task. \n
 *                                   However, the task will not run in this
 *                                   state. You will have to activate it by calling
 *                                   @ref activate_minos_task. \n
 *                                   You can also stop a task, regardless of its
 *                                   current state, by calling @ref stop_minos_task.
 *                               </p>
 *   <tr><td> Active        <td> <p> A task in this state is considered by
 *                                   @karajan good to go. The scheduler can decide at any
 *                                   moment, based on its parameters, to execute this
 *                                   task. \n
 *                                   The transition to and from the <c><b>Running</b></c>
 *                                   state is handled by @karajan, as mentioned above. \n
 *                                   You can suspend an active task by calling
 *                                   @ref suspend_minos_task. \n
 *                                   You can stop it by calling @ref stop_minos_task. \n
 *                                   Or delete it, calling @ref delete_minos_task.
 *                               </p>
 *   <tr><td> Running       <td> <p> A running task is a task that is currently being
 *                                   executed. A task can be executed only if it was
 *                                   previously in the <c><b>Active</b></c> state. \n
 *                                   Transitions to and from this state are controlled
 *                                   by @karajan. \n
 *                                   You have a bit of control by specifying to which
 *                                   state the task goes to after execution. \n
 *                                   If you call @ref stop_minos_task while it's
 *                                   executing, it will go to <c><b>Stopped</b></c>. \n
 *                                   If you call @ref suspend_minos_task during it's
 *                                   execution, it will go to <c><b>Suspended</b></c>. \n
 *                                   If you call @ref delete_minos_task during it's
 *                                   execution, it will go into oblivion. \n
 *                                   If you don't call any function, it will return to
 *                                   <c><b>Active</b></c> state.
 *                               </p>
 *   <tr><td> Suspended     <td> <p> A suspended task is a task that is treated just like
 *                                   an active one by the scheduler, except it won't
 *                                   run. Only its next runpoint will be calculated. \n
 *                                   That means that @karajan keeps
 *                                   track of all the counters and timings, it just won't
 *                                   execute the task when the time comes. It considers
 *                                   it was executed and moves forward. \n
 *                                   This state is useful when you want to temporary
 *                                   withhold a task from executing, but you don't want
 *                                   it to lose synchronization with the rest of the
 *                                   system (see the @ref karajan_task_delay section
 *                                   for more details). \n
 *                                   Only a @ref karajan_periodic_tasks "periodic" task
 *                                   can be suspended. \n
 *                                   A suspended task can be reactivated by calling
 *                                   @ref activate_minos_task. \n
 *                                   It can also be stopped by calling
 *                                   @ref stop_minos_task. \n
 *                                   Or deleted, calling @ref delete_minos_task.
 *                               </p>
 *
 * </table>
 *
 *
 *
 * \n \n @section karajan_the_end The End
 *
 * This concludes the documentation for @ref karajan.c "Karajan". \n
 *
 * Now it's time for a bit of trivia. \n
 *
 * <a href="https://en.wikipedia.org/wiki/Herbert_von_Karajan">Herbert von Karajan</a>
 *   was a famous Austrian conductor, generally regarded as one of the greatest
 *   conductors of the 20<sup>th</sup> century.
 *
 * @image html "wikipedia_karajan.jpg"
 *
 * Why choose the name of a conductor for a scheduler? In this instance, because
 *   Herbert von Karajan was known to have an exceptional sense of tempo, going as far as
 *   having himself tested against a computer to prove it! He said that rythmic
 *   inaccuracies are one thing that might make him lose temper. \n
 *   <i>I can accept a wrong note from an orchestra but when everything is getting
 *   faster or slower, that I cannot accept.</i>\n
 *
 * If you make use of all the features described on this page when you design your
 *   SW system and the tasks, you'll help the scheduler of @minos keep a very good tempo,
 *   one that won't annoy Herbert von Karajan. \n \n
 *
 *
 * You can find more about Herbert von Karajan on his
 *   <a href="https://en.wikipedia.org/wiki/Herbert_von_Karajan">Wikipedia page</a>.
 *
 *
 *
 */
