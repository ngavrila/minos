/** @cond license
 * MinOS
 * Copyright (C) 2020 Nicu Gavrila (https://www.linkedin.com/in/nicu-gavrila/)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 * @endcond
 */

#ifndef MINOS_INTEGRATION_H
#define MINOS_INTEGRATION_H

#include "minos_defs.h"


void dummy_start_timer_signal(void);
/**
 * @brief This macro will be called by @minos (@chronos) to start the system timer.
 *
 * @details Assign the appropiate function that starts the timer. After the call to this
 *   function, @minos will expect that the function @ref chronos_time_interrupt will be
 *   called regulary to update the system time counter. \n
 *
 * If you provide @ref minos_empty_function instead of a function, @minos will run without
 *   timing information, but the duration of one @ref minos_tick_t "tick" will be
 *   completely random! \n
 *
 * Because reliable timing is an important part of a real-time system, this may be the
 *   most important function you have to supply @minos with. \n
 *
 * You have a template example @ref mainpage_code_example_timer "here".
 */
#define MINOS_START_TIME      dummy_start_timer_signal

/**
 * @brief Assign to this macro a function that initializes the MCU HW.
 *
 * @details Use this if you want @minos to initialize the MCU HW system. You can also
 *   call that function yourself before calling @ref minos(). If you don't need to
 *   initialize the MCU HW in any way or you do it before starting @minos, just assign
 *   @ref minos_empty_function to this macro. \n
 *
 * @minos operation won't be impacted if @ref minos_empty_function is assigned here,
 *   but make sure your MCU is fully operational. For example, 8-bit PIC microcontrollers
 *   don't usually need such a function implemented, but 32-bit ARM microcontrollers
 *   do. \n \n
 *
 *
 * Below is an example of initializing the HW on a STM32 microcontroller. The function
 *   is called <c><b>system_init</b></c> and it would replace @ref minos_empty_function
 *   in the definition of macro @ref MINOS_HW_INIT_FUNCTION.
 *
 * @code
 * void system_init(void)
 * {
 *         // HSE config:
 *         // HSEON = 0
 *         // HSEBYP = 0
 *         // HSEON = 1
 *
 *         RCC->CR.HSEON = 0;
 *         RCC->CR.HSEBYP = 0;
 *         RCC->CR.HSEON = 1;
 *
 *         // PLL config:
 *         // disable PLL
 *         // PLLSource = HSE
 *         // PLLM = 8
 *         // PLLN = 336
 *         // PLLP = PLLP_DIV2
 *         // PLLQ = 7
 *         // enable PLL
 *
 *
 *         RCC->CR.PLLON = 0;
 *
 *         RCC->PLLCFGR.PLLSRC = RCC_PLLCFGR_PLLSRC_HSE;
 *         RCC->PLLCFGR.PLLM = 8;
 *         RCC->PLLCFGR.PLLN = 336;
 *         RCC->PLLCFGR.PLLP = 0;
 *         RCC->PLLCFGR.PLLQ = 7;
 *
 *         RCC->CR.PLLON = 1;
 *
 *         // System Clock Source config:
 *         // flash latency: ACR_BYTE0_ADDRESS = 5
 *         // ACR_DCEN = 1
 *         // ACR_ICEN = 1
 *         // ACR_PRFTEN = 1
 *         // sysclocksource: RCC_CFGR_SW = PLL
 *         // RCC_CFGR_HPRE = sysclk_div1
 *         // RCC_CFGR_PPRE1 = hclk_div4
 *         // RCC_CFGR_PPRE2 = hclk_div2
 *
 *         FLASH->ACR.LATENCY = 5;
 *         FLASH->ACR.DCEN = 1;
 *         FLASH->ACR.ICEN = 1;
 *         FLASH->ACR.PRFTEN = 1;
 *
 *         RCC->CFGR.HPRE = 0;
 *         RCC->CFGR.PPRE1 = 5;
 *         RCC->CFGR.PPRE2 = 4;
 *         RCC->CFGR.SW = RCC_CFGR_SW_PLL;
 * }
 *
 * @endcode
 */
#define MINOS_HW_INIT_FUNCTION  minos_empty_function

/**
 * @brief @minos macro to disable interrupts.
 *
 * @details Assign the appropiate function that implements this function in your system.
 *   Please note that @minos may nest calls to @ref MINOS_INTERRUPT_DISABLE and
 *   @ref MINOS_INTERRUPT_RESTORE (that means it can call twice or more times
 *   @ref MINOS_INTERRUPT_DISABLE before calling @ref MINOS_INTERRUPT_RESTORE the same
 *   number of times). You have to make sure the function you assign here supports
 *   that. \n
 *
 * If any @minos API (including @ref chronos_time_interrupt!) is called from interrupt
 *   context you must assign the interrupt disable function to this macro. Otherwise
 *   @minos may experience glitches during operation (like tasks executed more or less
 *   times than requested by the software system, which is a bad thing). \n
 *
 * If you don't use interrupts, you can assign @ref minos_empty_function to it. \n
 *
 * Usually the HW header of your microcontroller have a macro like this, sometimes called
 *   <c><b>DI</b></c> and its counterpart to re-enable interrupts, <c><b>EI</b></c>.
 *   You would have to implement a function around it to support nested calls:
 *
 * @code
 *
 * uint8 intvect_disableCounter = 0;
 *
 * void interrupts(void)
 * {
 *         intvect_disableCounter = 0;
 * }
 *
 * void disable_interrupts(void)
 * {
 *         DI()
 *         intvect_disableCounter++;
 * }
 *
 * void enable_interrupts(void)
 * {
 *         intvect_disableCounter--;
 *         if (intvect_disableCounter == 0)
 *                 EI()
 * }
 *
 * @endcode
 *
 * You should add the initialization function for this small component (called
 *   <c><b>interrupts</b></c> in the example above) as high as possible in the
 *   @ref user_init_vector list as possible, if not the first.
 */
#define MINOS_INTERRUPT_DISABLE    minos_empty_function

/**
 * @brief @minos macros to re-enable interrupts.
 *
 * @details It is mandatory that you assign the appropriate function here if you assigned
 *   the one that disables interrupts to @ref MINOS_INTERRUPT_DISABLE. \n
 *
 * For more details about the interrupt disable/enable (including a code example), check
 *   the documentation of the previous macro, @ref MINOS_INTERRUPT_DISABLE.
 */
#define MINOS_INTERRUPT_RESTORE    minos_empty_function


void dummy_halt_instruction(void);
/**
 * @brief @minos macros to halt the CPU.
 *
 * @details @minos will call this macro when there's nothing left to execute. This means
 *   that no currently active task has to run at this point in time. Only a call to
 *   @ref chronos_time_interrupt would advance the time, which tells @minos that it may
 *   be the moment to run a task. \n
 *
 * If you choose to implement this function, make sure that the execution is halted
 *   and resumed inside it and this operation is transparent to @minos. You must
 *   guarantee that no calls to @ref chronos_time_interrupt are missed due to the HALT
 *   instruction, or task executions might be delayed. \n
 *
 * @minos runs just fine without this function. The only reason this feature is here is
 *   because it can give the CPU a break when there's nothing to do, instead of
 *   busy-waiting. This will lower the power consumption, which is an useful thing,
 *   especially in battery powered applications. \n
 *
 * Check the documentation of @janus for more details about system states. \n
 *
 * You must check the documentation of your MCU to see how to implement this function.
 *   An example may be added here in future versions.
 */
#define MINOS_HALT_INSTRUCTION     dummy_halt_instruction

/**
 * @brief @minos macro that is called when the current state is @ref MINOS_SLEEP.
 *
 * @details If you plan to use the @ref MINOS_SLEEP state, you must implement a function
 *   that handles it. The system must go to sleep while inside this function and the
 *   function returns when the system wakes up. \n
 *
 * @minos operates correctly without it, but be aware that if @minos ever goes into the
 *   @ref MINOS_SLEEP, this is the only function that gets executed, no matter what.
 *   One way of making sure that the system doesn't go to sleep is to set
 *   @ref LOWEST_JANUS_STATE to, for example, @ref MINOS_RUN. \n
 *
 * You must check the documentation of your MCU to see how to implement this function.
 *   An example may be added here in future versions.
 */
#define MINOS_SLEEP_FUNCTION    minos_empty_function


#ifdef USER_INIT_VECTOR_DEF

void test_system(void);
/**
 * @brief @ref user_init_vector is a list of your component initialization functions.
 *
 * @details Add all your initialization functions here in the same order you wish them
 *   executed. @ref MINOS_NULL must be the last entry in this list. \n
 *
 * A code example is provided here, with function <c><b>test_system</b></c>. You should
 *   replace this function with the ones for your components. Another example can be
 *   found in the @ref mainpage_code_example_integration_h on the @ref index "Main Page".
 */
static void (*user_init_vector[])(void) =
{
	test_system,
	MINOS_NULL
};

#endif // USER_INIT_VECTOR_DEF


#endif  // MINOS_INTEGRATION_H

/**
 * @file
 *
 * This page provides information about various functions that you may have to implement
 *   so that @minos can use. If some of the functions are missing, @minos operation
 *   would be affected (as it's the case with @ref MINOS_START_TIME), others would
 *   have no impact on the operation of @minos, but may restrict the way you use some of
 *   the @minos features (for example @ref MINOS_SLEEP_FUNCTION). Read the documentation
 *   on each macro and variable defined here. When you implement the functions required
 *   here, go to the @ref minos_integration.h file and assign them to the corresponding
 *   macros. @ref user_init_vector is also defined in @ref minos_integration.h and
 *   documented here. \n
 *
 * Needless to say, you should implement your functions in your own file(s), not in
 *   @ref minos_integration.h. \n
 *
 * Some of the macros may have functions assigned to them if the test system implemented
 *   in this repo uses those functionalities. The test system is here to test various
 *   features of @minos. You have to replace its functions with the ones you implement.
 *   The functions you may see here also provide an example on how to add your own. \n
 *
 * Any macro you don't implement a function for, must be assigned
 *   @ref minos_empty_function. \n \n
 *
 *
 * Any function you see in this page is implemented somewhere else, so the
 *   documentation for them may not exist here.
 */
