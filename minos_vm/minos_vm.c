/** @cond license
 * MinOS
 * Copyright (C) 2020 Nicu Gavrila (https://www.linkedin.com/in/nicu-gavrila/)
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 * @endcond
 */

#include <pthread.h>
#include <semaphore.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

#include "minos_vm.h"

#include "../test_system/test_system.h"

#include "../minos/minos.h"


#define TIMER_SIGNAL SIGRTMIN


#define  S_TIME_INTERVAL   0
#define US_TIME_INTERVAL   1000

#define TICKS_PER_INTERVAL 1

pthread_t task_thread_id;

pthread_mutex_t isr_lock_mutex;

sem_t halt_instruction_simulation_semaphore;

int halt_instruction_executed = 0;


void resume_halt_instruction_simulation(void);


void signal_handler(int sig, siginfo_t *sig_info, void *uc)
{
	DI();
	if (sig == TIMER_SIGNAL)
	{
		resume_halt_instruction_simulation();

		chronos_time_interrupt((sig_info->si_overrun + 1) * TICKS_PER_INTERVAL);

		if (sig_info->si_overrun != 0)
		{
			log_data(LOG_WAR, RES_NONE, "Timer signal handler overflow: %d",
			         sig_info->si_overrun);
		}
	}
	EI();
}


void* task_thread(void *ptr);
void start_timer_signal(void);




int main()
{
	if (pthread_mutex_init(&isr_lock_mutex, NULL) != 0)
	{
		printf("\n mutex init has failed\n");
		return -3;
	}


	if (sem_init(&halt_instruction_simulation_semaphore, 0, 2) != 0)
	{
		printf("halt_instruction_simulation_semaphore init has failed!\n");
		return -2;
	}

	if (pthread_create(&task_thread_id, NULL, task_thread, NULL))
	{
		printf("Error starting task_thread, aborting!\n");
		return -1;
	}

	/// start_timer_signal();

	while (5 < 7)
		sleep(2);

	return 0;
}



void* task_thread(void *ptr)
{
	minos();

	fprintf(stderr, "ERROR: Function \"minos\" returned unexpectedly!\n");

	return NULL;
}

void start_timer_signal(void)
{
	timer_t timerid;
	struct sigevent sev;
	struct itimerspec its;
	struct sigaction sa;


	/** Establish handler for timer signal */

	sa.sa_flags = SA_SIGINFO;
	sa.sa_sigaction = signal_handler;
	sigemptyset(&sa.sa_mask);
	sigaction(TIMER_SIGNAL, &sa, NULL);


	/** Create the timer and set it's event */

	sev.sigev_notify = SIGEV_SIGNAL;
	sev.sigev_signo = TIMER_SIGNAL;
	sev.sigev_value.sival_ptr = &timerid;
	timer_create(CLOCK_REALTIME, &sev, &timerid);


	/** Start the timer */

	its.it_value.tv_sec = S_TIME_INTERVAL;
	its.it_value.tv_nsec = US_TIME_INTERVAL * 1000;
	its.it_interval.tv_sec = its.it_value.tv_sec;
	its.it_interval.tv_nsec = its.it_value.tv_nsec;

	timer_settime(timerid, 0, &its, NULL);
}

void halt_instruction_simulation(void)
{
	sem_wait(&halt_instruction_simulation_semaphore);
}

void resume_halt_instruction_simulation(void)
{
	int sem_val;

	if (sem_getvalue(&halt_instruction_simulation_semaphore, &sem_val) != 0)
	{
		sem_post(&halt_instruction_simulation_semaphore);
		printf("Warning: getting the value of halt_instruction_simulation_semaphore failed! \n");
	}
	else
	{
		if (sem_val < 2)
		{
			sem_post(&halt_instruction_simulation_semaphore);
		}
	}
}

void DI(void)
{
	pthread_mutex_lock(&isr_lock_mutex);
}

void EI(void)
{
	pthread_mutex_unlock(&isr_lock_mutex);
}
